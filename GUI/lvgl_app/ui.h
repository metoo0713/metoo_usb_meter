// SquareLine LVGL GENERATED FILE
// EDITOR VERSION: SquareLine Studio 1.1.0
// LVGL VERSION: 8.2
// PROJECT: Metoo_USB_Meter

#ifndef _METOO_USB_METER_UI_H
#define _METOO_USB_METER_UI_H

#ifdef __cplusplus
extern "C" {
#endif

#if defined __has_include
#if __has_include("lvgl.h")
#include "lvgl.h"
#elif __has_include("lvgl/lvgl.h")
#include "lvgl/lvgl.h"
#else
#include "lvgl.h"
#endif
#else
#include "lvgl.h"
#endif

void msgin_Animation(lv_obj_t * TargetObject, int delay);
void msgout_Animation(lv_obj_t * TargetObject, int delay);
void ui_event_Screen1(lv_event_t * e);
extern lv_obj_t * ui_Screen1;
extern lv_obj_t * ui_Label_LOGO;
extern lv_obj_t * ui_Label_name;
extern lv_obj_t * ui_Screen2;
extern lv_obj_t * ui_Bar1;
extern lv_obj_t * ui_Bar2;
extern lv_obj_t * ui_Label_mAh_S2;
extern lv_obj_t * ui_Label_V_S2;
extern lv_obj_t * ui_Label_A_S2;
extern lv_obj_t * ui_Label_W_S2;
extern lv_obj_t * ui_Label_MAX_S2;
extern lv_obj_t * ui_Label_AVG_S2;
void ui_event_KEY_Label1_S2(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label1_S2;
void ui_event_KEY_Label2_S2(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label2_S2;
void ui_event_KEY_Label3_S2(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label3_S2;
extern lv_obj_t * ui_Screen3;
extern lv_obj_t * ui_Chart_VA_S3;
extern lv_obj_t * ui_Label_Chart_V_S3;
extern lv_obj_t * ui_Label_Chart_A_S3;
extern lv_obj_t * ui_Label_V_S3;
extern lv_obj_t * ui_Label_A_S3;
extern lv_obj_t * ui_Label_W_S3;
extern lv_obj_t * ui_Label_T_S3;
void ui_event_KEY_Label1_S3(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label1_S3;
void ui_event_KEY_Label2_S3(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label2_S3;
void ui_event_KEY_Label3_S3(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label3_S3;
extern lv_obj_t * ui_Screen4;
extern lv_obj_t * ui_TAG_Label_S4;
extern lv_obj_t * ui_Label1_S4;
void ui_event_KEY_Label1_S4(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label1_S4;
void ui_event_KEY_Label2_S4(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label2_S4;
void ui_event_KEY_Label3_S4(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label3_S4;
extern lv_obj_t * ui_Panel_MSG_S4;
extern lv_obj_t * ui_MSG_Label1_WARN_S4;
void ui_event_KEY_Label1_S4_MSG(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label1_S4_MSG;
extern lv_obj_t * ui_KEY_Label2_S4_MSG;
void ui_event_KEY_Label3_S4_MSG(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label3_S4_MSG;
extern lv_obj_t * ui_Screen5;
extern lv_obj_t * ui_TAG_Label_S5;
extern lv_obj_t * ui_Label_SET_V;
extern lv_obj_t * ui_Label_OUT_V;
extern lv_obj_t * ui_Label1_S5;
void ui_event_KEY_Label1_S5(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label1_S5;
void ui_event_KEY_Label2_S5(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label2_S5;
void ui_event_KEY_Label3_S5(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label3_S5;
extern lv_obj_t * ui_Label_DN_S5;
extern lv_obj_t * ui_Label_DP_S5;
extern lv_obj_t * ui_Panel_MSG_S5;
extern lv_obj_t * ui_MSG_Label1_WARN_S5;
extern lv_obj_t * ui_MSG_Label1_S5;
extern lv_obj_t * ui_MSG_Label2_S5;
extern lv_obj_t * ui_MSG_Label3_S5;
void ui_event_KEY_Label1_S5_MSG(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label1_S5_MSG;
void ui_event_KEY_Label2_S5_MSG(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label2_S5_MSG;
void ui_event_KEY_Label3_S5_MSG(lv_event_t * e);
extern lv_obj_t * ui_KEY_Label3_S5_MSG;

void Metoo_Screen_D(lv_event_t * e);
void Metoo_Restart_REC(lv_event_t * e);
void Metoo_Screen_U(lv_event_t * e);
void Metoo_Screen_D(lv_event_t * e);
void Metoo_Change_Chart(lv_event_t * e);
void Metoo_Screen_U(lv_event_t * e);
void Metoo_Screen_D(lv_event_t * e);
void Metoo_s4_msg_show(lv_event_t * e);
void Metoo_Screen_U(lv_event_t * e);
void Metoo_S4_MSG_yes(lv_event_t * e);
void Metoo_S4_MSG_no(lv_event_t * e);
void Metoo_Screen_D(lv_event_t * e);
void Metoo_s5_setDown(lv_event_t * e);
void Metoo_s5_msg_show(lv_event_t * e);
void Metoo_s5_mode_change(lv_event_t * e);
void Metoo_Screen_U(lv_event_t * e);
void Metoo_s5_setUP(lv_event_t * e);
void Metoo_S5_MSG_yes(lv_event_t * e);
void Metoo_S5_MSG_set(lv_event_t * e);
void Metoo_S5_MSG_no(lv_event_t * e);



LV_FONT_DECLARE(ui_font_QUUB_58);
LV_FONT_DECLARE(ui_font_SARASA_30_key_bpp2);
LV_FONT_DECLARE(ui_font_SARASA_40_bpp2);
LV_FONT_DECLARE(ui_font_SARASA_18);


void ui_init(void);

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif
