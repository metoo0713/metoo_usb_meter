#ifndef METOO_LVGL_H_
#define METOO_LVGL_H_

#include "ui.h"
/* ui生成后需要修改部分
ui_helpers.c        替换



ui.c
LV_EVENT_CLICKED 替换为 LV_EVENT_SHORT_CLICKED
*/

#define CHART_TIME 3                               // 图标持续时间
#define CHART_TICK 30                              // 图表更新间隔 20ms
#define CHART_NUM (CHART_TIME * 1000 / CHART_TICK) // 图标点数

typedef struct
{
    uint8_t s5_msg_id;     // 页面5消息框设置ID,0xff表示在启动中
    uint8_t s5_id;         // 诱骗模式，0=QC,1=PD
    uint8_t s4_test_state; // 检测进行中

} MetooLVGL_Data_t;

void Metoo_lvgl_init(void);
void Metoo_lvgl_update(lv_timer_t *t);
void Metoo_S5_MSG(bool state);
void Metoo_S4_MSG(bool state); // 完成关闭消息框
void Metoo_S4_set(void);
void Metoo_S4_loop(void);
#endif /*METOO_LVGL_H_*/
