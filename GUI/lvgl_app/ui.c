// SquareLine LVGL GENERATED FILE
// EDITOR VERSION: SquareLine Studio 1.1.0
// LVGL VERSION: 8.2
// PROJECT: Metoo_USB_Meter

#include "ui.h"
#include "ui_helpers.h"

///////////////////// VARIABLES ////////////////////
void msgin_Animation(lv_obj_t * TargetObject, int delay);
void msgout_Animation(lv_obj_t * TargetObject, int delay);
void ui_event_Screen1(lv_event_t * e);
lv_obj_t * ui_Screen1;
lv_obj_t * ui_Label_LOGO;
lv_obj_t * ui_Label_name;
lv_obj_t * ui_Screen2;
lv_obj_t * ui_Bar1;
lv_obj_t * ui_Bar2;
lv_obj_t * ui_Label_mAh_S2;
lv_obj_t * ui_Label_V_S2;
lv_obj_t * ui_Label_A_S2;
lv_obj_t * ui_Label_W_S2;
lv_obj_t * ui_Label_MAX_S2;
lv_obj_t * ui_Label_AVG_S2;
void ui_event_KEY_Label1_S2(lv_event_t * e);
lv_obj_t * ui_KEY_Label1_S2;
void ui_event_KEY_Label2_S2(lv_event_t * e);
lv_obj_t * ui_KEY_Label2_S2;
void ui_event_KEY_Label3_S2(lv_event_t * e);
lv_obj_t * ui_KEY_Label3_S2;
lv_obj_t * ui_Screen3;
lv_obj_t * ui_Chart_VA_S3;
lv_obj_t * ui_Label_Chart_V_S3;
lv_obj_t * ui_Label_Chart_A_S3;
lv_obj_t * ui_Label_V_S3;
lv_obj_t * ui_Label_A_S3;
lv_obj_t * ui_Label_W_S3;
lv_obj_t * ui_Label_T_S3;
void ui_event_KEY_Label1_S3(lv_event_t * e);
lv_obj_t * ui_KEY_Label1_S3;
void ui_event_KEY_Label2_S3(lv_event_t * e);
lv_obj_t * ui_KEY_Label2_S3;
void ui_event_KEY_Label3_S3(lv_event_t * e);
lv_obj_t * ui_KEY_Label3_S3;
lv_obj_t * ui_Screen4;
lv_obj_t * ui_TAG_Label_S4;
lv_obj_t * ui_Label1_S4;
void ui_event_KEY_Label1_S4(lv_event_t * e);
lv_obj_t * ui_KEY_Label1_S4;
void ui_event_KEY_Label2_S4(lv_event_t * e);
lv_obj_t * ui_KEY_Label2_S4;
void ui_event_KEY_Label3_S4(lv_event_t * e);
lv_obj_t * ui_KEY_Label3_S4;
lv_obj_t * ui_Panel_MSG_S4;
lv_obj_t * ui_MSG_Label1_WARN_S4;
void ui_event_KEY_Label1_S4_MSG(lv_event_t * e);
lv_obj_t * ui_KEY_Label1_S4_MSG;
lv_obj_t * ui_KEY_Label2_S4_MSG;
void ui_event_KEY_Label3_S4_MSG(lv_event_t * e);
lv_obj_t * ui_KEY_Label3_S4_MSG;
lv_obj_t * ui_Screen5;
lv_obj_t * ui_TAG_Label_S5;
lv_obj_t * ui_Label_SET_V;
lv_obj_t * ui_Label_OUT_V;
lv_obj_t * ui_Label1_S5;
void ui_event_KEY_Label1_S5(lv_event_t * e);
lv_obj_t * ui_KEY_Label1_S5;
void ui_event_KEY_Label2_S5(lv_event_t * e);
lv_obj_t * ui_KEY_Label2_S5;
void ui_event_KEY_Label3_S5(lv_event_t * e);
lv_obj_t * ui_KEY_Label3_S5;
lv_obj_t * ui_Label_DN_S5;
lv_obj_t * ui_Label_DP_S5;
lv_obj_t * ui_Panel_MSG_S5;
lv_obj_t * ui_MSG_Label1_WARN_S5;
lv_obj_t * ui_MSG_Label1_S5;
lv_obj_t * ui_MSG_Label2_S5;
lv_obj_t * ui_MSG_Label3_S5;
void ui_event_KEY_Label1_S5_MSG(lv_event_t * e);
lv_obj_t * ui_KEY_Label1_S5_MSG;
void ui_event_KEY_Label2_S5_MSG(lv_event_t * e);
lv_obj_t * ui_KEY_Label2_S5_MSG;
void ui_event_KEY_Label3_S5_MSG(lv_event_t * e);
lv_obj_t * ui_KEY_Label3_S5_MSG;

///////////////////// TEST LVGL SETTINGS ////////////////////
#if LV_COLOR_DEPTH != 16
    #error "LV_COLOR_DEPTH should be 16bit to match SquareLine Studio's settings"
#endif
#if LV_COLOR_16_SWAP !=0
    #error "LV_COLOR_16_SWAP should be 0 to match SquareLine Studio's settings"
#endif

///////////////////// ANIMATIONS ////////////////////
void msgin_Animation(lv_obj_t * TargetObject, int delay)
{
    lv_anim_t PropertyAnimation_0;
    lv_anim_init(&PropertyAnimation_0);
    lv_anim_set_time(&PropertyAnimation_0, 1000);
    lv_anim_set_user_data(&PropertyAnimation_0, TargetObject);
    lv_anim_set_custom_exec_cb(&PropertyAnimation_0, _ui_anim_callback_set_y);
    lv_anim_set_values(&PropertyAnimation_0, -120, 0);
    lv_anim_set_path_cb(&PropertyAnimation_0, lv_anim_path_overshoot);
    lv_anim_set_delay(&PropertyAnimation_0, delay + 0);
    lv_anim_set_playback_time(&PropertyAnimation_0, 0);
    lv_anim_set_playback_delay(&PropertyAnimation_0, 0);
    lv_anim_set_repeat_count(&PropertyAnimation_0, 0);
    lv_anim_set_repeat_delay(&PropertyAnimation_0, 0);
    lv_anim_set_early_apply(&PropertyAnimation_0, false);
    lv_anim_start(&PropertyAnimation_0);

}
void msgout_Animation(lv_obj_t * TargetObject, int delay)
{
    lv_anim_t PropertyAnimation_0;
    lv_anim_init(&PropertyAnimation_0);
    lv_anim_set_time(&PropertyAnimation_0, 1000);
    lv_anim_set_user_data(&PropertyAnimation_0, TargetObject);
    lv_anim_set_custom_exec_cb(&PropertyAnimation_0, _ui_anim_callback_set_y);
    lv_anim_set_values(&PropertyAnimation_0, 0, -120);
    lv_anim_set_path_cb(&PropertyAnimation_0, lv_anim_path_overshoot);
    lv_anim_set_delay(&PropertyAnimation_0, delay + 0);
    lv_anim_set_playback_time(&PropertyAnimation_0, 0);
    lv_anim_set_playback_delay(&PropertyAnimation_0, 0);
    lv_anim_set_repeat_count(&PropertyAnimation_0, 0);
    lv_anim_set_repeat_delay(&PropertyAnimation_0, 0);
    lv_anim_set_early_apply(&PropertyAnimation_0, false);
    lv_anim_start(&PropertyAnimation_0);

}

///////////////////// FUNCTIONS ////////////////////
void ui_event_Screen1(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_SCREEN_LOADED) {
        _ui_screen_change(ui_Screen2, LV_SCR_LOAD_ANIM_FADE_ON, 1000, 500);
    }
}
void ui_event_KEY_Label1_S2(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        _ui_screen_change(ui_Screen5, LV_SCR_LOAD_ANIM_MOVE_RIGHT, 500, 0);
        Metoo_Screen_D(e);
    }
}
void ui_event_KEY_Label2_S2(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        Metoo_Restart_REC(e);
    }
    if(event_code == LV_EVENT_SHORT_CLICKED) {
        _ui_flag_modify(ui_Label_mAh_S2, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_TOGGLE);
        _ui_flag_modify(ui_Label_AVG_S2, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_TOGGLE);
        _ui_flag_modify(ui_Label_MAX_S2, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_TOGGLE);
    }
}
void ui_event_KEY_Label3_S2(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        _ui_screen_change(ui_Screen3, LV_SCR_LOAD_ANIM_MOVE_LEFT, 500, 0);
        Metoo_Screen_U(e);
    }
}
void ui_event_KEY_Label1_S3(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        _ui_screen_change(ui_Screen2, LV_SCR_LOAD_ANIM_MOVE_RIGHT, 500, 0);
        Metoo_Screen_D(e);
    }
}
void ui_event_KEY_Label2_S3(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_SHORT_CLICKED) {
        Metoo_Change_Chart(e);
    }
}
void ui_event_KEY_Label3_S3(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        _ui_screen_change(ui_Screen4, LV_SCR_LOAD_ANIM_MOVE_LEFT, 500, 0);
        Metoo_Screen_U(e);
    }
}
void ui_event_KEY_Label1_S4(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        _ui_screen_change(ui_Screen3, LV_SCR_LOAD_ANIM_MOVE_RIGHT, 500, 0);
        Metoo_Screen_D(e);
    }
}
void ui_event_KEY_Label2_S4(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        msgin_Animation(ui_Panel_MSG_S4, 0);
        Metoo_s4_msg_show(e);
    }
}
void ui_event_KEY_Label3_S4(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        _ui_screen_change(ui_Screen5, LV_SCR_LOAD_ANIM_MOVE_LEFT, 500, 0);
        Metoo_Screen_U(e);
    }
}
void ui_event_KEY_Label1_S4_MSG(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        Metoo_S4_MSG_yes(e);
    }
}
void ui_event_KEY_Label3_S4_MSG(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_SHORT_CLICKED) {
        Metoo_S4_MSG_no(e);
        msgout_Animation(ui_Panel_MSG_S4, 0);
    }
}
void ui_event_KEY_Label1_S5(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        _ui_screen_change(ui_Screen4, LV_SCR_LOAD_ANIM_MOVE_RIGHT, 500, 0);
        Metoo_Screen_D(e);
    }
    if(event_code == LV_EVENT_SHORT_CLICKED) {
        Metoo_s5_setDown(e);
    }
}
void ui_event_KEY_Label2_S5(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        Metoo_s5_msg_show(e);
        msgin_Animation(ui_Panel_MSG_S5, 0);
    }
    if(event_code == LV_EVENT_SHORT_CLICKED) {
        Metoo_s5_mode_change(e);
    }
}
void ui_event_KEY_Label3_S5(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        _ui_screen_change(ui_Screen2, LV_SCR_LOAD_ANIM_MOVE_LEFT, 500, 0);
        Metoo_Screen_U(e);
    }
    if(event_code == LV_EVENT_SHORT_CLICKED) {
        Metoo_s5_setUP(e);
    }
}
void ui_event_KEY_Label1_S5_MSG(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_LONG_PRESSED) {
        Metoo_S5_MSG_yes(e);
    }
}
void ui_event_KEY_Label2_S5_MSG(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_SHORT_CLICKED) {
        Metoo_S5_MSG_set(e);
    }
}
void ui_event_KEY_Label3_S5_MSG(lv_event_t * e)
{
    lv_event_code_t event_code = lv_event_get_code(e);
    lv_obj_t * target = lv_event_get_target(e);
    if(event_code == LV_EVENT_SHORT_CLICKED) {
        Metoo_S5_MSG_no(e);
        msgout_Animation(ui_Panel_MSG_S5, 0);
    }
}

///////////////////// SCREENS ////////////////////
void ui_Screen1_screen_init(void)
{
    ui_Screen1 = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_Screen1, LV_OBJ_FLAG_SCROLLABLE);      /// Flags

    ui_Label_LOGO = lv_label_create(ui_Screen1);
    lv_obj_set_width(ui_Label_LOGO, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label_LOGO, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label_LOGO, lv_pct(0));
    lv_obj_set_y(ui_Label_LOGO, lv_pct(-10));
    lv_obj_set_align(ui_Label_LOGO, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_LOGO, "Metoo");
    lv_obj_set_style_text_font(ui_Label_LOGO, &ui_font_QUUB_58, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_name = lv_label_create(ui_Screen1);
    lv_obj_set_width(ui_Label_name, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label_name, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label_name, lv_pct(0));
    lv_obj_set_y(ui_Label_name, lv_pct(18));
    lv_obj_set_align(ui_Label_name, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_name, "USB Meter");
    lv_obj_set_style_text_font(ui_Label_name, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_add_event_cb(ui_Screen1, ui_event_Screen1, LV_EVENT_ALL, NULL);

}
void ui_Screen2_screen_init(void)
{
    ui_Screen2 = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_Screen2, LV_OBJ_FLAG_SCROLLABLE);      /// Flags

    ui_Bar1 = lv_bar_create(ui_Screen2);
    lv_bar_set_range(ui_Bar1, 0, 255);
    lv_obj_set_width(ui_Bar1, lv_pct(100));
    lv_obj_set_height(ui_Bar1, lv_pct(3));
    lv_obj_set_x(ui_Bar1, 0);
    lv_obj_set_y(ui_Bar1, -65);
    lv_obj_set_align(ui_Bar1, LV_ALIGN_CENTER);
    lv_obj_set_style_bg_color(ui_Bar1, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Bar1, 0, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_set_style_bg_color(ui_Bar1, lv_color_hex(0x00FF00), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Bar1, 255, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_grad_color(ui_Bar1, lv_color_hex(0x292831), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_main_stop(ui_Bar1, 0, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_grad_stop(ui_Bar1, 255, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_grad_dir(ui_Bar1, LV_GRAD_DIR_HOR, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    ui_Bar2 = lv_bar_create(ui_Screen2);
    lv_bar_set_range(ui_Bar2, 0, 255);
    lv_obj_set_width(ui_Bar2, lv_pct(100));
    lv_obj_set_height(ui_Bar2, lv_pct(3));
    lv_obj_set_x(ui_Bar2, 0);
    lv_obj_set_y(ui_Bar2, 66);
    lv_obj_set_align(ui_Bar2, LV_ALIGN_CENTER);
    lv_obj_set_style_bg_color(ui_Bar2, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Bar2, 0, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_set_style_bg_color(ui_Bar2, lv_color_hex(0x00FF00), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Bar2, 255, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_grad_color(ui_Bar2, lv_color_hex(0x292831), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_main_stop(ui_Bar2, 0, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_grad_stop(ui_Bar2, 255, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_grad_dir(ui_Bar2, LV_GRAD_DIR_HOR, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    ui_Label_mAh_S2 = lv_label_create(ui_Screen2);
    lv_obj_set_width(ui_Label_mAh_S2, lv_pct(50));
    lv_obj_set_height(ui_Label_mAh_S2, lv_pct(90));
    lv_obj_set_x(ui_Label_mAh_S2, lv_pct(24));
    lv_obj_set_y(ui_Label_mAh_S2, lv_pct(-2));
    lv_obj_set_align(ui_Label_mAh_S2, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_mAh_S2,
                      "200000.00mAh\n200000.00 Wh\n00:00:00:000\n+3.3V  -3.3V\n温度     30℃\n模式   QC3.0");
    lv_obj_set_style_text_color(ui_Label_mAh_S2, lv_color_hex(0xFFC107), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_mAh_S2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Label_mAh_S2, LV_TEXT_ALIGN_RIGHT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_mAh_S2, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_V_S2 = lv_label_create(ui_Screen2);
    lv_obj_set_width(ui_Label_V_S2, lv_pct(52));
    lv_obj_set_height(ui_Label_V_S2, lv_pct(25));
    lv_obj_set_x(ui_Label_V_S2, lv_pct(-25));
    lv_obj_set_y(ui_Label_V_S2, lv_pct(-33));
    lv_obj_set_align(ui_Label_V_S2, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_V_S2, "20.00V");
    lv_obj_set_style_text_color(ui_Label_V_S2, lv_color_hex(0xF44336), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_V_S2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Label_V_S2, LV_TEXT_ALIGN_RIGHT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_V_S2, &ui_font_SARASA_40_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_A_S2 = lv_label_create(ui_Screen2);
    lv_obj_set_width(ui_Label_A_S2, lv_pct(52));
    lv_obj_set_height(ui_Label_A_S2, lv_pct(25));
    lv_obj_set_x(ui_Label_A_S2, lv_pct(-25));
    lv_obj_set_y(ui_Label_A_S2, lv_pct(1));
    lv_obj_set_align(ui_Label_A_S2, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_A_S2, "8.888A");
    lv_obj_set_style_text_color(ui_Label_A_S2, lv_color_hex(0x2196F3), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_A_S2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Label_A_S2, LV_TEXT_ALIGN_RIGHT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_A_S2, &ui_font_SARASA_40_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_W_S2 = lv_label_create(ui_Screen2);
    lv_obj_set_width(ui_Label_W_S2, lv_pct(52));
    lv_obj_set_height(ui_Label_W_S2, lv_pct(25));
    lv_obj_set_x(ui_Label_W_S2, lv_pct(-25));
    lv_obj_set_y(ui_Label_W_S2, lv_pct(35));
    lv_obj_set_align(ui_Label_W_S2, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_W_S2, "88.88W");
    lv_obj_set_style_text_color(ui_Label_W_S2, lv_color_hex(0x8BC34A), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_W_S2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Label_W_S2, LV_TEXT_ALIGN_RIGHT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_W_S2, &ui_font_SARASA_40_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_MAX_S2 = lv_label_create(ui_Screen2);
    lv_obj_set_width(ui_Label_MAX_S2, lv_pct(45));
    lv_obj_set_height(ui_Label_MAX_S2, lv_pct(55));
    lv_obj_set_x(ui_Label_MAX_S2, 78);
    lv_obj_set_y(ui_Label_MAX_S2, -25);
    lv_obj_set_align(ui_Label_MAX_S2, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_MAX_S2, "最│20.000 V\n大│8.8880 A\n值│160.00 W");
    lv_obj_add_flag(ui_Label_MAX_S2, LV_OBJ_FLAG_HIDDEN);     /// Flags
    lv_obj_set_style_text_color(ui_Label_MAX_S2, lv_color_hex(0xFFC107), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_MAX_S2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Label_MAX_S2, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_MAX_S2, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_AVG_S2 = lv_label_create(ui_Screen2);
    lv_obj_set_width(ui_Label_AVG_S2, lv_pct(45));
    lv_obj_set_height(ui_Label_AVG_S2, lv_pct(55));
    lv_obj_set_x(ui_Label_AVG_S2, 78);
    lv_obj_set_y(ui_Label_AVG_S2, 39);
    lv_obj_set_align(ui_Label_AVG_S2, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_AVG_S2, "平│20.000 V\n均│8.8880 A\n值│160.00 W");
    lv_obj_add_flag(ui_Label_AVG_S2, LV_OBJ_FLAG_HIDDEN);     /// Flags
    lv_obj_set_style_text_color(ui_Label_AVG_S2, lv_color_hex(0xDB66EF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_AVG_S2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_letter_space(ui_Label_AVG_S2, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_line_space(ui_Label_AVG_S2, -1, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Label_AVG_S2, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_AVG_S2, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_KEY_Label1_S2 = lv_label_create(ui_Screen2);
    lv_obj_set_width(ui_KEY_Label1_S2, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label1_S2, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label1_S2, 185);
    lv_obj_set_y(ui_KEY_Label1_S2, 0);
    lv_label_set_text(ui_KEY_Label1_S2, "●");
    lv_obj_add_flag(ui_KEY_Label1_S2, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label1_S2, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S2, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S2, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label2_S2 = lv_label_create(ui_Screen2);
    lv_obj_set_width(ui_KEY_Label2_S2, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label2_S2, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label2_S2, 205);
    lv_obj_set_y(ui_KEY_Label2_S2, 0);
    lv_label_set_text(ui_KEY_Label2_S2, "◒");
    lv_obj_add_flag(ui_KEY_Label2_S2, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label2_S2, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S2, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S2, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label3_S2 = lv_label_create(ui_Screen2);
    lv_obj_set_width(ui_KEY_Label3_S2, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label3_S2, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label3_S2, 225);
    lv_obj_set_y(ui_KEY_Label3_S2, 0);
    lv_label_set_text(ui_KEY_Label3_S2, "●");
    lv_obj_add_flag(ui_KEY_Label3_S2, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label3_S2, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S2, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S2, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    lv_obj_add_event_cb(ui_KEY_Label1_S2, ui_event_KEY_Label1_S2, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label2_S2, ui_event_KEY_Label2_S2, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label3_S2, ui_event_KEY_Label3_S2, LV_EVENT_ALL, NULL);

}
void ui_Screen3_screen_init(void)
{
    ui_Screen3 = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_Screen3, LV_OBJ_FLAG_SCROLLABLE);      /// Flags

    ui_Chart_VA_S3 = lv_chart_create(ui_Screen3);
    lv_obj_set_width(ui_Chart_VA_S3, lv_pct(98));
    lv_obj_set_height(ui_Chart_VA_S3, lv_pct(80));
    lv_obj_set_x(ui_Chart_VA_S3, lv_pct(0));
    lv_obj_set_y(ui_Chart_VA_S3, lv_pct(-8));
    lv_obj_set_align(ui_Chart_VA_S3, LV_ALIGN_CENTER);
    lv_obj_set_style_pad_left(ui_Chart_VA_S3, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_pad_right(ui_Chart_VA_S3, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_pad_top(ui_Chart_VA_S3, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_pad_bottom(ui_Chart_VA_S3, 0, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_Chart_V_S3 = lv_label_create(ui_Chart_VA_S3);
    lv_obj_set_width(ui_Label_Chart_V_S3, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label_Chart_V_S3, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label_Chart_V_S3, lv_pct(-46));
    lv_obj_set_y(ui_Label_Chart_V_S3, lv_pct(0));
    lv_obj_set_align(ui_Label_Chart_V_S3, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_Chart_V_S3, "20\n\n\n0");
    lv_obj_set_style_text_color(ui_Label_Chart_V_S3, lv_color_hex(0xF44336), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_Chart_V_S3, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_Chart_V_S3, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_blend_mode(ui_Label_Chart_V_S3, LV_BLEND_MODE_NORMAL, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_Label_Chart_V_S3, 150, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_Chart_A_S3 = lv_label_create(ui_Chart_VA_S3);
    lv_obj_set_width(ui_Label_Chart_A_S3, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label_Chart_A_S3, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label_Chart_A_S3, lv_pct(46));
    lv_obj_set_y(ui_Label_Chart_A_S3, lv_pct(0));
    lv_obj_set_align(ui_Label_Chart_A_S3, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_Chart_A_S3, "8\n\n\n0");
    lv_obj_set_style_text_color(ui_Label_Chart_A_S3, lv_color_hex(0x2196F3), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_Chart_A_S3, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_Chart_A_S3, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_Label_Chart_A_S3, 150, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_V_S3 = lv_label_create(ui_Screen3);
    lv_obj_set_width(ui_Label_V_S3, lv_pct(25));
    lv_obj_set_height(ui_Label_V_S3, lv_pct(15));
    lv_obj_set_x(ui_Label_V_S3, lv_pct(-38));
    lv_obj_set_y(ui_Label_V_S3, lv_pct(42));
    lv_obj_set_align(ui_Label_V_S3, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_V_S3, "20.00V");
    lv_obj_set_style_text_color(ui_Label_V_S3, lv_color_hex(0xF44336), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_V_S3, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Label_V_S3, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_V_S3, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_A_S3 = lv_label_create(ui_Screen3);
    lv_obj_set_width(ui_Label_A_S3, lv_pct(25));
    lv_obj_set_height(ui_Label_A_S3, lv_pct(15));
    lv_obj_set_x(ui_Label_A_S3, lv_pct(-13));
    lv_obj_set_y(ui_Label_A_S3, lv_pct(42));
    lv_obj_set_align(ui_Label_A_S3, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_A_S3, "8.888A");
    lv_obj_set_style_text_color(ui_Label_A_S3, lv_color_hex(0x2196F3), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_A_S3, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Label_A_S3, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_A_S3, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_W_S3 = lv_label_create(ui_Screen3);
    lv_obj_set_width(ui_Label_W_S3, lv_pct(25));
    lv_obj_set_height(ui_Label_W_S3, lv_pct(15));
    lv_obj_set_x(ui_Label_W_S3, lv_pct(13));
    lv_obj_set_y(ui_Label_W_S3, lv_pct(42));
    lv_obj_set_align(ui_Label_W_S3, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_W_S3, "88.88W");
    lv_obj_set_style_text_color(ui_Label_W_S3, lv_color_hex(0x8BC34A), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_W_S3, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Label_W_S3, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_W_S3, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_T_S3 = lv_label_create(ui_Screen3);
    lv_obj_set_width(ui_Label_T_S3, lv_pct(25));
    lv_obj_set_height(ui_Label_T_S3, lv_pct(15));
    lv_obj_set_x(ui_Label_T_S3, lv_pct(38));
    lv_obj_set_y(ui_Label_T_S3, lv_pct(42));
    lv_obj_set_align(ui_Label_T_S3, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_T_S3, "30℃");
    lv_obj_set_style_text_color(ui_Label_T_S3, lv_color_hex(0xFFC107), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_T_S3, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Label_T_S3, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_T_S3, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_KEY_Label1_S3 = lv_label_create(ui_Screen3);
    lv_obj_set_width(ui_KEY_Label1_S3, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label1_S3, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label1_S3, 185);
    lv_obj_set_y(ui_KEY_Label1_S3, 0);
    lv_label_set_text(ui_KEY_Label1_S3, "●");
    lv_obj_add_flag(ui_KEY_Label1_S3, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label1_S3, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S3, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S3, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label2_S3 = lv_label_create(ui_Screen3);
    lv_obj_set_width(ui_KEY_Label2_S3, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label2_S3, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label2_S3, 205);
    lv_obj_set_y(ui_KEY_Label2_S3, 0);
    lv_label_set_text(ui_KEY_Label2_S3, "◒");
    lv_obj_add_flag(ui_KEY_Label2_S3, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label2_S3, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S3, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S3, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label3_S3 = lv_label_create(ui_Screen3);
    lv_obj_set_width(ui_KEY_Label3_S3, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label3_S3, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label3_S3, 225);
    lv_obj_set_y(ui_KEY_Label3_S3, 0);
    lv_label_set_text(ui_KEY_Label3_S3, "●");
    lv_obj_add_flag(ui_KEY_Label3_S3, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label3_S3, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S3, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S3, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    lv_obj_add_event_cb(ui_KEY_Label1_S3, ui_event_KEY_Label1_S3, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label2_S3, ui_event_KEY_Label2_S3, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label3_S3, ui_event_KEY_Label3_S3, LV_EVENT_ALL, NULL);

}
void ui_Screen4_screen_init(void)
{
    ui_Screen4 = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_Screen4, LV_OBJ_FLAG_SCROLLABLE);      /// Flags

    ui_TAG_Label_S4 = lv_label_create(ui_Screen4);
    lv_obj_set_width(ui_TAG_Label_S4, 238);
    lv_obj_set_height(ui_TAG_Label_S4, 20);
    lv_obj_set_x(ui_TAG_Label_S4, lv_pct(0));
    lv_obj_set_y(ui_TAG_Label_S4, lv_pct(-42));
    lv_obj_set_align(ui_TAG_Label_S4, LV_ALIGN_CENTER);
    lv_label_set_text(ui_TAG_Label_S4, "协议检测");
    lv_obj_set_style_text_align(ui_TAG_Label_S4, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_TAG_Label_S4, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_color(ui_TAG_Label_S4, lv_color_hex(0xDB66EF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_opa(ui_TAG_Label_S4, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_width(ui_TAG_Label_S4, 1, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_pad(ui_TAG_Label_S4, 1, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label1_S4 = lv_label_create(ui_Screen4);
    lv_obj_set_width(ui_Label1_S4, lv_pct(98));
    lv_obj_set_height(ui_Label1_S4, lv_pct(85));
    lv_obj_set_x(ui_Label1_S4, lv_pct(0));
    lv_obj_set_y(ui_Label1_S4, lv_pct(9));
    lv_obj_set_align(ui_Label1_S4, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label1_S4, "");

    ui_KEY_Label1_S4 = lv_label_create(ui_Screen4);
    lv_obj_set_width(ui_KEY_Label1_S4, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label1_S4, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label1_S4, 185);
    lv_obj_set_y(ui_KEY_Label1_S4, 0);
    lv_label_set_text(ui_KEY_Label1_S4, "●");
    lv_obj_add_flag(ui_KEY_Label1_S4, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label1_S4, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S4, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S4, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label2_S4 = lv_label_create(ui_Screen4);
    lv_obj_set_width(ui_KEY_Label2_S4, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label2_S4, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label2_S4, 205);
    lv_obj_set_y(ui_KEY_Label2_S4, 0);
    lv_label_set_text(ui_KEY_Label2_S4, "●");
    lv_obj_add_flag(ui_KEY_Label2_S4, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label2_S4, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S4, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S4, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label3_S4 = lv_label_create(ui_Screen4);
    lv_obj_set_width(ui_KEY_Label3_S4, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label3_S4, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label3_S4, 225);
    lv_obj_set_y(ui_KEY_Label3_S4, 0);
    lv_label_set_text(ui_KEY_Label3_S4, "●");
    lv_obj_add_flag(ui_KEY_Label3_S4, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label3_S4, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S4, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S4, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_Panel_MSG_S4 = lv_obj_create(ui_Screen4);
    lv_obj_set_width(ui_Panel_MSG_S4, lv_pct(65));
    lv_obj_set_height(ui_Panel_MSG_S4, lv_pct(75));
    lv_obj_set_x(ui_Panel_MSG_S4, 0);
    lv_obj_set_y(ui_Panel_MSG_S4, -120);
    lv_obj_set_align(ui_Panel_MSG_S4, LV_ALIGN_TOP_RIGHT);
    lv_obj_clear_flag(ui_Panel_MSG_S4, LV_OBJ_FLAG_SCROLLABLE);      /// Flags
    lv_obj_set_style_border_width(ui_Panel_MSG_S4, 0, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_MSG_Label1_WARN_S4 = lv_label_create(ui_Panel_MSG_S4);
    lv_obj_set_width(ui_MSG_Label1_WARN_S4, lv_pct(95));
    lv_obj_set_height(ui_MSG_Label1_WARN_S4, lv_pct(95));
    lv_obj_set_x(ui_MSG_Label1_WARN_S4, 0);
    lv_obj_set_y(ui_MSG_Label1_WARN_S4, 9);
    lv_obj_set_align(ui_MSG_Label1_WARN_S4, LV_ALIGN_CENTER);
    lv_label_set_text(ui_MSG_Label1_WARN_S4, "检测过程中输出接口请勿连接任何设备!");
    lv_obj_set_style_text_color(ui_MSG_Label1_WARN_S4, lv_color_hex(0xF44336), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_MSG_Label1_WARN_S4, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_MSG_Label1_WARN_S4, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_MSG_Label1_WARN_S4, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_KEY_Label1_S4_MSG = lv_label_create(ui_Panel_MSG_S4);
    lv_obj_set_width(ui_KEY_Label1_S4_MSG, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label1_S4_MSG, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label1_S4_MSG, -27);
    lv_obj_set_y(ui_KEY_Label1_S4_MSG, -13);
    lv_obj_set_align(ui_KEY_Label1_S4_MSG, LV_ALIGN_TOP_RIGHT);
    lv_label_set_text(ui_KEY_Label1_S4_MSG, "●");
    lv_obj_add_flag(ui_KEY_Label1_S4_MSG, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_color(ui_KEY_Label1_S4_MSG, lv_color_hex(0x1CFF00), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_KEY_Label1_S4_MSG, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_KEY_Label1_S4_MSG, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S4_MSG, 150, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S4_MSG, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label2_S4_MSG = lv_label_create(ui_Panel_MSG_S4);
    lv_obj_set_width(ui_KEY_Label2_S4_MSG, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label2_S4_MSG, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label2_S4_MSG, -7);
    lv_obj_set_y(ui_KEY_Label2_S4_MSG, -13);
    lv_obj_set_align(ui_KEY_Label2_S4_MSG, LV_ALIGN_TOP_RIGHT);
    lv_label_set_text(ui_KEY_Label2_S4_MSG, "◌");
    lv_obj_add_flag(ui_KEY_Label2_S4_MSG, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label2_S4_MSG, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S4_MSG, 150, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S4_MSG, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label3_S4_MSG = lv_label_create(ui_Panel_MSG_S4);
    lv_obj_set_width(ui_KEY_Label3_S4_MSG, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label3_S4_MSG, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label3_S4_MSG, 13);
    lv_obj_set_y(ui_KEY_Label3_S4_MSG, -13);
    lv_obj_set_align(ui_KEY_Label3_S4_MSG, LV_ALIGN_TOP_RIGHT);
    lv_label_set_text(ui_KEY_Label3_S4_MSG, "○");
    lv_obj_add_flag(ui_KEY_Label3_S4_MSG, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_color(ui_KEY_Label3_S4_MSG, lv_color_hex(0xFF0000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_KEY_Label3_S4_MSG, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_KEY_Label3_S4_MSG, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S4_MSG, 150, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S4_MSG, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    lv_obj_add_event_cb(ui_KEY_Label1_S4, ui_event_KEY_Label1_S4, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label2_S4, ui_event_KEY_Label2_S4, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label3_S4, ui_event_KEY_Label3_S4, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label1_S4_MSG, ui_event_KEY_Label1_S4_MSG, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label3_S4_MSG, ui_event_KEY_Label3_S4_MSG, LV_EVENT_ALL, NULL);

}
void ui_Screen5_screen_init(void)
{
    ui_Screen5 = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_Screen5, LV_OBJ_FLAG_SCROLLABLE);      /// Flags

    ui_TAG_Label_S5 = lv_label_create(ui_Screen5);
    lv_obj_set_width(ui_TAG_Label_S5, 238);
    lv_obj_set_height(ui_TAG_Label_S5, 20);
    lv_obj_set_x(ui_TAG_Label_S5, lv_pct(0));
    lv_obj_set_y(ui_TAG_Label_S5, lv_pct(-42));
    lv_obj_set_align(ui_TAG_Label_S5, LV_ALIGN_CENTER);
    lv_label_set_text(ui_TAG_Label_S5, "QC诱骗");
    lv_obj_set_style_text_align(ui_TAG_Label_S5, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_TAG_Label_S5, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_color(ui_TAG_Label_S5, lv_color_hex(0xFFC107), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_opa(ui_TAG_Label_S5, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_width(ui_TAG_Label_S5, 1, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_pad(ui_TAG_Label_S5, 1, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_SET_V = lv_label_create(ui_Screen5);
    lv_obj_set_width(ui_Label_SET_V, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label_SET_V, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label_SET_V, lv_pct(0));
    lv_obj_set_y(ui_Label_SET_V, lv_pct(-19));
    lv_obj_set_align(ui_Label_SET_V, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_SET_V, "20.00V");
    lv_obj_set_style_text_font(ui_Label_SET_V, &ui_font_SARASA_40_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_OUT_V = lv_label_create(ui_Screen5);
    lv_obj_set_width(ui_Label_OUT_V, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label_OUT_V, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label_OUT_V, lv_pct(0));
    lv_obj_set_y(ui_Label_OUT_V, lv_pct(13));
    lv_obj_set_align(ui_Label_OUT_V, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_OUT_V, "20.00V");
    lv_obj_set_style_text_color(ui_Label_OUT_V, lv_color_hex(0xF44336), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label_OUT_V, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label_OUT_V, &ui_font_SARASA_40_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label1_S5 = lv_label_create(ui_Screen5);
    lv_obj_set_width(ui_Label1_S5, lv_pct(100));
    lv_obj_set_height(ui_Label1_S5, lv_pct(75));
    lv_obj_set_x(ui_Label1_S5, lv_pct(0));
    lv_obj_set_y(ui_Label1_S5, lv_pct(8));
    lv_obj_set_align(ui_Label1_S5, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label1_S5, "设定:       \n\n输出:\n\nD+:          D-:");
    lv_obj_set_style_text_color(ui_Label1_S5, lv_color_hex(0x009688), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label1_S5, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label1_S5, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_KEY_Label1_S5 = lv_label_create(ui_Screen5);
    lv_obj_set_width(ui_KEY_Label1_S5, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label1_S5, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label1_S5, 184);
    lv_obj_set_y(ui_KEY_Label1_S5, 1);
    lv_label_set_text(ui_KEY_Label1_S5, "●");
    lv_obj_add_flag(ui_KEY_Label1_S5, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label1_S5, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S5, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S5, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label2_S5 = lv_label_create(ui_Screen5);
    lv_obj_set_width(ui_KEY_Label2_S5, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label2_S5, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label2_S5, 205);
    lv_obj_set_y(ui_KEY_Label2_S5, 0);
    lv_label_set_text(ui_KEY_Label2_S5, "◒");
    lv_obj_add_flag(ui_KEY_Label2_S5, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label2_S5, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S5, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S5, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label3_S5 = lv_label_create(ui_Screen5);
    lv_obj_set_width(ui_KEY_Label3_S5, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label3_S5, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label3_S5, 225);
    lv_obj_set_y(ui_KEY_Label3_S5, 0);
    lv_label_set_text(ui_KEY_Label3_S5, "●");
    lv_obj_add_flag(ui_KEY_Label3_S5, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label3_S5, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S5, 88, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S5, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_Label_DN_S5 = lv_label_create(ui_Screen5);
    lv_obj_set_width(ui_Label_DN_S5, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label_DN_S5, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label_DN_S5, lv_pct(14));
    lv_obj_set_y(ui_Label_DN_S5, lv_pct(41));
    lv_obj_set_align(ui_Label_DN_S5, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_DN_S5, "3.3 V");
    lv_obj_set_style_text_font(ui_Label_DN_S5, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label_DP_S5 = lv_label_create(ui_Screen5);
    lv_obj_set_width(ui_Label_DP_S5, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label_DP_S5, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label_DP_S5, lv_pct(-30));
    lv_obj_set_y(ui_Label_DP_S5, lv_pct(41));
    lv_obj_set_align(ui_Label_DP_S5, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Label_DP_S5, "3.3 V");
    lv_obj_set_style_text_font(ui_Label_DP_S5, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Panel_MSG_S5 = lv_obj_create(ui_Screen5);
    lv_obj_set_width(ui_Panel_MSG_S5, lv_pct(75));
    lv_obj_set_height(ui_Panel_MSG_S5, lv_pct(80));
    lv_obj_set_x(ui_Panel_MSG_S5, 0);
    lv_obj_set_y(ui_Panel_MSG_S5, -120);
    lv_obj_set_align(ui_Panel_MSG_S5, LV_ALIGN_TOP_RIGHT);
    lv_obj_clear_flag(ui_Panel_MSG_S5, LV_OBJ_FLAG_SCROLLABLE);      /// Flags
    lv_obj_set_style_border_width(ui_Panel_MSG_S5, 0, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_MSG_Label1_WARN_S5 = lv_label_create(ui_Panel_MSG_S5);
    lv_obj_set_width(ui_MSG_Label1_WARN_S5, lv_pct(100));
    lv_obj_set_height(ui_MSG_Label1_WARN_S5, lv_pct(100));
    lv_obj_set_x(ui_MSG_Label1_WARN_S5, 0);
    lv_obj_set_y(ui_MSG_Label1_WARN_S5, 9);
    lv_obj_set_align(ui_MSG_Label1_WARN_S5, LV_ALIGN_CENTER);
    lv_label_set_text(ui_MSG_Label1_WARN_S5,
                      "诱骗过程中输出接口会直接输出电压,非专业人士请勿操作!");
    lv_obj_set_style_text_color(ui_MSG_Label1_WARN_S5, lv_color_hex(0xF44336), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_MSG_Label1_WARN_S5, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_MSG_Label1_WARN_S5, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_MSG_Label1_WARN_S5, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_MSG_Label1_S5 = lv_label_create(ui_Panel_MSG_S5);
    lv_obj_set_width(ui_MSG_Label1_S5, lv_pct(90));
    lv_obj_set_height(ui_MSG_Label1_S5, lv_pct(25));
    lv_obj_set_x(ui_MSG_Label1_S5, lv_pct(0));
    lv_obj_set_y(ui_MSG_Label1_S5, lv_pct(-33));
    lv_obj_set_align(ui_MSG_Label1_S5, LV_ALIGN_CENTER);
    lv_label_set_text(ui_MSG_Label1_S5, "启动QC诱骗?");
    lv_obj_add_flag(ui_MSG_Label1_S5, LV_OBJ_FLAG_HIDDEN);     /// Flags
    lv_obj_set_style_text_align(ui_MSG_Label1_S5, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_MSG_Label1_S5, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_MSG_Label2_S5 = lv_label_create(ui_Panel_MSG_S5);
    lv_obj_set_width(ui_MSG_Label2_S5, lv_pct(90));
    lv_obj_set_height(ui_MSG_Label2_S5, lv_pct(25));
    lv_obj_set_align(ui_MSG_Label2_S5, LV_ALIGN_CENTER);
    lv_label_set_text(ui_MSG_Label2_S5, "5V 9V 12V 20V");
    lv_obj_add_flag(ui_MSG_Label2_S5, LV_OBJ_FLAG_HIDDEN);     /// Flags
    lv_obj_set_style_text_align(ui_MSG_Label2_S5, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_MSG_Label2_S5, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_color(ui_MSG_Label2_S5, lv_color_hex(0xFFC107), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_opa(ui_MSG_Label2_S5, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_width(ui_MSG_Label2_S5, 1, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_pad(ui_MSG_Label2_S5, 1, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_MSG_Label3_S5 = lv_label_create(ui_Panel_MSG_S5);
    lv_obj_set_width(ui_MSG_Label3_S5, lv_pct(90));
    lv_obj_set_height(ui_MSG_Label3_S5, lv_pct(25));
    lv_obj_set_x(ui_MSG_Label3_S5, lv_pct(0));
    lv_obj_set_y(ui_MSG_Label3_S5, lv_pct(33));
    lv_obj_set_align(ui_MSG_Label3_S5, LV_ALIGN_CENTER);
    lv_label_set_text(ui_MSG_Label3_S5, "3.6V~20V");
    lv_obj_add_flag(ui_MSG_Label3_S5, LV_OBJ_FLAG_HIDDEN);     /// Flags
    lv_obj_set_style_text_align(ui_MSG_Label3_S5, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_MSG_Label3_S5, &ui_font_SARASA_18, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_color(ui_MSG_Label3_S5, lv_color_hex(0x292831), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_opa(ui_MSG_Label3_S5, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_width(ui_MSG_Label3_S5, 1, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_outline_pad(ui_MSG_Label3_S5, 1, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_KEY_Label1_S5_MSG = lv_label_create(ui_Panel_MSG_S5);
    lv_obj_set_width(ui_KEY_Label1_S5_MSG, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label1_S5_MSG, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label1_S5_MSG, -27);
    lv_obj_set_y(ui_KEY_Label1_S5_MSG, -13);
    lv_obj_set_align(ui_KEY_Label1_S5_MSG, LV_ALIGN_TOP_RIGHT);
    lv_label_set_text(ui_KEY_Label1_S5_MSG, "●");
    lv_obj_add_flag(ui_KEY_Label1_S5_MSG, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label1_S5_MSG, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S5_MSG, 150, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label1_S5_MSG, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label2_S5_MSG = lv_label_create(ui_Panel_MSG_S5);
    lv_obj_set_width(ui_KEY_Label2_S5_MSG, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label2_S5_MSG, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label2_S5_MSG, -7);
    lv_obj_set_y(ui_KEY_Label2_S5_MSG, -13);
    lv_obj_set_align(ui_KEY_Label2_S5_MSG, LV_ALIGN_TOP_RIGHT);
    lv_label_set_text(ui_KEY_Label2_S5_MSG, "○");
    lv_obj_add_flag(ui_KEY_Label2_S5_MSG, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label2_S5_MSG, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S5_MSG, 150, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label2_S5_MSG, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    ui_KEY_Label3_S5_MSG = lv_label_create(ui_Panel_MSG_S5);
    lv_obj_set_width(ui_KEY_Label3_S5_MSG, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_KEY_Label3_S5_MSG, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_KEY_Label3_S5_MSG, 13);
    lv_obj_set_y(ui_KEY_Label3_S5_MSG, -13);
    lv_obj_set_align(ui_KEY_Label3_S5_MSG, LV_ALIGN_TOP_RIGHT);
    lv_label_set_text(ui_KEY_Label3_S5_MSG, "○");
    lv_obj_add_flag(ui_KEY_Label3_S5_MSG, LV_OBJ_FLAG_CLICKABLE);     /// Flags
    lv_obj_set_style_text_font(ui_KEY_Label3_S5_MSG, &ui_font_SARASA_30_key_bpp2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S5_MSG, 150, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_opa(ui_KEY_Label3_S5_MSG, 200, LV_PART_MAIN | LV_STATE_PRESSED);

    lv_obj_add_event_cb(ui_KEY_Label1_S5, ui_event_KEY_Label1_S5, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label2_S5, ui_event_KEY_Label2_S5, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label3_S5, ui_event_KEY_Label3_S5, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label1_S5_MSG, ui_event_KEY_Label1_S5_MSG, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label2_S5_MSG, ui_event_KEY_Label2_S5_MSG, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_KEY_Label3_S5_MSG, ui_event_KEY_Label3_S5_MSG, LV_EVENT_ALL, NULL);

}

void ui_init(void)
{
    lv_disp_t * dispp = lv_disp_get_default();
    lv_theme_t * theme = lv_theme_default_init(dispp, lv_palette_main(LV_PALETTE_BLUE), lv_palette_main(LV_PALETTE_RED),
                                               true, LV_FONT_DEFAULT);
    lv_disp_set_theme(dispp, theme);
    ui_Screen1_screen_init();
    ui_Screen2_screen_init();
    ui_Screen3_screen_init();
    ui_Screen4_screen_init();
    ui_Screen5_screen_init();
    lv_disp_load_scr(ui_Screen1);
}
