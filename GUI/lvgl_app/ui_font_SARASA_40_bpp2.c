/*******************************************************************************
 * Size: 40 px
 * Bpp: 2
 * Opts: --bpp 2 --size 40 --font D:\lvgl\Metoo_USB_Meter\assets\sarasa-term-sc-regular.ttf -o D:\lvgl\Metoo_USB_Meter\assets\ui_font_SARASA_40_bpp2.c --format lvgl -r 0x20 --symbols 0123456789.AVW▲▶▼◀ --no-compress --no-prefilter
 ******************************************************************************/

#ifdef LV_LVGL_H_INCLUDE_SIMPLE
#include "lvgl.h"
#else
#include "lvgl/lvgl.h"
#endif

#ifndef UI_FONT_SARASA_40_BPP2
#define UI_FONT_SARASA_40_BPP2 1
#endif

#if UI_FONT_SARASA_40_BPP2

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t glyph_bitmap[] = {
    /* U+0020 " " */

    /* U+002E "." */
    0x0, 0x0, 0xf, 0xf0, 0x3f, 0xfc, 0x3f, 0xfc,
    0x3f, 0xfc, 0x3f, 0xfc, 0xb, 0xe0,

    /* U+0030 "0" */
    0x0, 0x2f, 0xf8, 0x0, 0x2, 0xff, 0xff, 0x80,
    0xf, 0xff, 0xff, 0xf0, 0x2f, 0xe0, 0xb, 0xf8,
    0x3f, 0x40, 0x1, 0xfc, 0x7f, 0x0, 0x0, 0xfd,
    0xbe, 0x0, 0x0, 0xbe, 0xbd, 0x0, 0x0, 0x7e,
    0xfd, 0x0, 0x0, 0x7f, 0xfd, 0x0, 0x0, 0xbf,
    0xfd, 0x0, 0x2, 0xff, 0xfd, 0x0, 0x1f, 0xff,
    0xfd, 0x0, 0x7f, 0xff, 0xfd, 0x2, 0xfe, 0x7f,
    0xfd, 0xf, 0xf4, 0x7f, 0xfd, 0x7f, 0xc0, 0x7f,
    0xff, 0xfe, 0x0, 0x7f, 0xff, 0xf4, 0x0, 0x7f,
    0xff, 0xc0, 0x0, 0x7f, 0xfe, 0x0, 0x0, 0x7f,
    0xfd, 0x0, 0x0, 0x7f, 0xbd, 0x0, 0x0, 0x7e,
    0xbe, 0x0, 0x0, 0xbe, 0xbf, 0x0, 0x0, 0xfd,
    0x3f, 0x40, 0x1, 0xfc, 0x3f, 0xd0, 0x7, 0xf8,
    0xf, 0xff, 0xff, 0xf0, 0x3, 0xff, 0xff, 0xc0,
    0x0, 0x6f, 0xf9, 0x0,

    /* U+0031 "1" */
    0x0, 0x3, 0xfc, 0x0, 0x7f, 0xf0, 0xf, 0xff,
    0xc1, 0xff, 0xff, 0x2f, 0xf5, 0xfd, 0xfe, 0x7,
    0xf2, 0xd0, 0x1f, 0xc0, 0x0, 0x7f, 0x0, 0x1,
    0xfc, 0x0, 0x7, 0xf0, 0x0, 0x1f, 0xc0, 0x0,
    0x7f, 0x0, 0x1, 0xfc, 0x0, 0x7, 0xf0, 0x0,
    0x1f, 0xc0, 0x0, 0x7f, 0x0, 0x1, 0xfc, 0x0,
    0x7, 0xf0, 0x0, 0x1f, 0xc0, 0x0, 0x7f, 0x0,
    0x1, 0xfc, 0x0, 0x7, 0xf0, 0x0, 0x1f, 0xc0,
    0x0, 0x7f, 0x0, 0x1, 0xfc, 0x0, 0x7, 0xf0,
    0x0, 0x1f, 0xc0, 0x0, 0x7f, 0x0, 0x1, 0xfc,

    /* U+0032 "2" */
    0x0, 0x6f, 0xf9, 0x0, 0x3, 0xff, 0xff, 0xc0,
    0xf, 0xff, 0xff, 0xf0, 0x2f, 0xd0, 0x7, 0xf8,
    0x3f, 0x40, 0x1, 0xfc, 0x7f, 0x0, 0x0, 0xfd,
    0x7e, 0x0, 0x0, 0xbd, 0x0, 0x0, 0x0, 0xbe,
    0x0, 0x0, 0x0, 0xbe, 0x0, 0x0, 0x0, 0xbd,
    0x0, 0x0, 0x0, 0xfc, 0x0, 0x0, 0x2, 0xfc,
    0x0, 0x0, 0x7, 0xf4, 0x0, 0x0, 0x1f, 0xe0,
    0x0, 0x0, 0xbf, 0x80, 0x0, 0x3, 0xfe, 0x0,
    0x0, 0x1f, 0xf4, 0x0, 0x0, 0x7f, 0xc0, 0x0,
    0x0, 0xff, 0x0, 0x0, 0x3, 0xfc, 0x0, 0x0,
    0xb, 0xf0, 0x0, 0x0, 0xf, 0xd0, 0x0, 0x0,
    0x2f, 0xc0, 0x0, 0x0, 0x3f, 0x40, 0x0, 0x0,
    0x3f, 0x0, 0x0, 0x0, 0x7f, 0x0, 0x0, 0x0,
    0x7f, 0xff, 0xff, 0xfd, 0x7f, 0xff, 0xff, 0xfd,
    0x7f, 0xff, 0xff, 0xfd,

    /* U+0033 "3" */
    0x0, 0x6f, 0xf9, 0x0, 0x0, 0xbf, 0xff, 0xf0,
    0x0, 0xbf, 0xff, 0xff, 0x0, 0x7f, 0x80, 0x1f,
    0xe0, 0x3f, 0x80, 0x1, 0xfc, 0xf, 0xc0, 0x0,
    0x3f, 0x43, 0xf0, 0x0, 0xb, 0xd0, 0x0, 0x0,
    0x2, 0xf4, 0x0, 0x0, 0x0, 0xfc, 0x0, 0x0,
    0x0, 0x7f, 0x0, 0x0, 0x0, 0x3f, 0x40, 0x0,
    0x0, 0xbf, 0x80, 0x0, 0x3f, 0xff, 0x80, 0x0,
    0xf, 0xff, 0x40, 0x0, 0x3, 0xff, 0xf4, 0x0,
    0x0, 0x1, 0xbf, 0x80, 0x0, 0x0, 0x7, 0xf8,
    0x0, 0x0, 0x0, 0x7f, 0x0, 0x0, 0x0, 0xb,
    0xe0, 0x0, 0x0, 0x1, 0xfc, 0x0, 0x0, 0x0,
    0x3f, 0x15, 0x0, 0x0, 0xf, 0xcf, 0xd0, 0x0,
    0x3, 0xf2, 0xf8, 0x0, 0x2, 0xf8, 0x7f, 0x0,
    0x0, 0xfd, 0xf, 0xf4, 0x1, 0xff, 0x0, 0xff,
    0xff, 0xff, 0x0, 0xf, 0xff, 0xff, 0x40, 0x0,
    0x6f, 0xf9, 0x0, 0x0,

    /* U+0034 "4" */
    0x0, 0x0, 0x2f, 0x80, 0x0, 0x0, 0x3f, 0x0,
    0x0, 0x0, 0xbe, 0x0, 0x0, 0x0, 0xfc, 0x0,
    0x0, 0x2, 0xf8, 0x0, 0x0, 0x3, 0xf0, 0x0,
    0x0, 0xb, 0xe0, 0x0, 0x0, 0x1f, 0xc0, 0x0,
    0x0, 0x3f, 0x80, 0x0, 0x0, 0x7f, 0x0, 0x0,
    0x0, 0xfe, 0x0, 0x0, 0x1, 0xfc, 0xa, 0x80,
    0x3, 0xf8, 0xf, 0xc0, 0x7, 0xf0, 0xf, 0xc0,
    0xf, 0xe0, 0xf, 0xc0, 0x1f, 0xc0, 0xf, 0xc0,
    0x3f, 0x80, 0xf, 0xc0, 0x7f, 0x0, 0xf, 0xc0,
    0xff, 0xff, 0xff, 0xfd, 0xff, 0xff, 0xff, 0xfd,
    0xff, 0xff, 0xff, 0xfd, 0x0, 0x0, 0xf, 0xc0,
    0x0, 0x0, 0xf, 0xc0, 0x0, 0x0, 0xf, 0xc0,
    0x0, 0x0, 0xf, 0xc0, 0x0, 0x0, 0xf, 0xc0,
    0x0, 0x0, 0xf, 0xc0, 0x0, 0x0, 0xf, 0xc0,
    0x0, 0x0, 0xf, 0xc0,

    /* U+0035 "5" */
    0x3f, 0xff, 0xff, 0xf8, 0x3f, 0xff, 0xff, 0xf8,
    0x3f, 0xff, 0xff, 0xf8, 0x3f, 0x0, 0x0, 0x0,
    0x3f, 0x0, 0x0, 0x0, 0x3f, 0x0, 0x0, 0x0,
    0x3f, 0x0, 0x0, 0x0, 0x3f, 0x0, 0x0, 0x0,
    0x3f, 0x0, 0x0, 0x0, 0x3f, 0x0, 0x0, 0x0,
    0x3f, 0x2f, 0xf9, 0x0, 0x3f, 0xff, 0xff, 0x80,
    0x3f, 0xff, 0xff, 0xe0, 0x3f, 0xe0, 0xb, 0xf4,
    0x3f, 0x80, 0x2, 0xfc, 0x3f, 0x0, 0x0, 0xfc,
    0x3f, 0x0, 0x0, 0xbd, 0x0, 0x0, 0x0, 0xbe,
    0x0, 0x0, 0x0, 0x7e, 0x0, 0x0, 0x0, 0x7f,
    0x0, 0x0, 0x0, 0x7e, 0x0, 0x0, 0x0, 0xbe,
    0x7e, 0x0, 0x0, 0xbd, 0x3f, 0x0, 0x0, 0xfc,
    0x3f, 0x80, 0x2, 0xfc, 0x1f, 0xe0, 0xb, 0xf4,
    0xb, 0xff, 0xff, 0xe0, 0x2, 0xff, 0xff, 0x80,
    0x0, 0x2f, 0xf9, 0x0,

    /* U+0036 "6" */
    0x0, 0x1, 0xfc, 0x0, 0x0, 0x3, 0xf8, 0x0,
    0x0, 0x7, 0xf0, 0x0, 0x0, 0xf, 0xe0, 0x0,
    0x0, 0x1f, 0xc0, 0x0, 0x0, 0x3f, 0x40, 0x0,
    0x0, 0x7f, 0x0, 0x0, 0x0, 0xfd, 0x0, 0x0,
    0x1, 0xfc, 0x0, 0x0, 0x3, 0xf4, 0x0, 0x0,
    0x7, 0xe0, 0x0, 0x0, 0xf, 0xef, 0xf9, 0x0,
    0x1f, 0xff, 0xff, 0x80, 0x2f, 0xff, 0xff, 0xf0,
    0x3f, 0xe0, 0xb, 0xf8, 0x7f, 0x80, 0x2, 0xfc,
    0x7f, 0x0, 0x0, 0xfd, 0xbe, 0x0, 0x0, 0xbe,
    0xbd, 0x0, 0x0, 0x7e, 0xbd, 0x0, 0x0, 0x7e,
    0xbd, 0x0, 0x0, 0x7e, 0xbd, 0x0, 0x0, 0x7e,
    0x7e, 0x0, 0x0, 0xbe, 0x3f, 0x0, 0x0, 0xfd,
    0x3f, 0x80, 0x2, 0xfc, 0x1f, 0xe0, 0xb, 0xf4,
    0xb, 0xff, 0xff, 0xf0, 0x2, 0xff, 0xff, 0x80,
    0x0, 0x2f, 0xf9, 0x0,

    /* U+0037 "7" */
    0x7f, 0xff, 0xff, 0xfd, 0x7f, 0xff, 0xff, 0xfd,
    0x7f, 0xff, 0xff, 0xfd, 0x0, 0x0, 0x0, 0xfc,
    0x0, 0x0, 0x1, 0xfc, 0x0, 0x0, 0x2, 0xf8,
    0x0, 0x0, 0x3, 0xf0, 0x0, 0x0, 0x7, 0xf0,
    0x0, 0x0, 0xb, 0xe0, 0x0, 0x0, 0xf, 0xd0,
    0x0, 0x0, 0x1f, 0xc0, 0x0, 0x0, 0x2f, 0x80,
    0x0, 0x0, 0x3f, 0x40, 0x0, 0x0, 0x3f, 0x0,
    0x0, 0x0, 0xbe, 0x0, 0x0, 0x0, 0xfd, 0x0,
    0x0, 0x0, 0xfc, 0x0, 0x0, 0x1, 0xf8, 0x0,
    0x0, 0x3, 0xf4, 0x0, 0x0, 0x3, 0xf0, 0x0,
    0x0, 0x7, 0xf0, 0x0, 0x0, 0xf, 0xd0, 0x0,
    0x0, 0xf, 0xc0, 0x0, 0x0, 0x1f, 0xc0, 0x0,
    0x0, 0x2f, 0x40, 0x0, 0x0, 0x3f, 0x0, 0x0,
    0x0, 0x7f, 0x0, 0x0, 0x0, 0xbe, 0x0, 0x0,
    0x0, 0xfc, 0x0, 0x0,

    /* U+0038 "8" */
    0x0, 0x6f, 0xf9, 0x0, 0x2, 0xff, 0xff, 0x80,
    0xf, 0xff, 0xff, 0xf0, 0x1f, 0xe0, 0xb, 0xf4,
    0x3f, 0x80, 0x2, 0xfc, 0x3f, 0x0, 0x0, 0xfc,
    0x3f, 0x0, 0x0, 0xfc, 0x3f, 0x0, 0x0, 0xfc,
    0x3f, 0x40, 0x1, 0xfc, 0x1f, 0xc0, 0x3, 0xf4,
    0xf, 0xe0, 0xb, 0xf0, 0x3, 0xf4, 0x1f, 0xc0,
    0x1, 0xfe, 0xbf, 0x40, 0x0, 0x7f, 0xfd, 0x0,
    0x0, 0x3f, 0xfc, 0x0, 0x0, 0xff, 0xff, 0x0,
    0x3, 0xfc, 0x3f, 0xc0, 0xf, 0xf0, 0xf, 0xf0,
    0x2f, 0xc0, 0x3, 0xf8, 0x3f, 0x40, 0x1, 0xfc,
    0x7e, 0x0, 0x0, 0xbd, 0xbd, 0x0, 0x0, 0x7e,
    0xbd, 0x0, 0x0, 0x7e, 0xbe, 0x0, 0x0, 0xbe,
    0x7f, 0x0, 0x0, 0xfd, 0x3f, 0xd0, 0x7, 0xfc,
    0x1f, 0xff, 0xff, 0xf0, 0x7, 0xff, 0xff, 0xd0,
    0x0, 0x6f, 0xf9, 0x0,

    /* U+0039 "9" */
    0x0, 0x6f, 0xf8, 0x0, 0x2, 0xff, 0xff, 0x80,
    0xf, 0xff, 0xff, 0xe0, 0x2f, 0xe0, 0xb, 0xf4,
    0x3f, 0x80, 0x2, 0xfc, 0x7f, 0x0, 0x0, 0xfc,
    0xbe, 0x0, 0x0, 0xbd, 0xbd, 0x0, 0x0, 0x7e,
    0xbd, 0x0, 0x0, 0x7e, 0xbd, 0x0, 0x0, 0x7e,
    0xbd, 0x0, 0x0, 0x7e, 0xbe, 0x0, 0x0, 0xbe,
    0x7f, 0x0, 0x0, 0xfd, 0x3f, 0x80, 0x2, 0xfd,
    0x1f, 0xf4, 0x1f, 0xfc, 0xb, 0xff, 0xff, 0xf8,
    0x2, 0xff, 0xff, 0xf4, 0x0, 0x1a, 0xab, 0xf0,
    0x0, 0x0, 0xf, 0xd0, 0x0, 0x0, 0x1f, 0xc0,
    0x0, 0x0, 0x3f, 0x40, 0x0, 0x0, 0x7f, 0x0,
    0x0, 0x0, 0xfd, 0x0, 0x0, 0x2, 0xfc, 0x0,
    0x0, 0x3, 0xf4, 0x0, 0x0, 0xb, 0xf0, 0x0,
    0x0, 0xf, 0xd0, 0x0, 0x0, 0x2f, 0xc0, 0x0,
    0x0, 0x3f, 0x40, 0x0,

    /* U+0041 "A" */
    0x0, 0x3, 0xfc, 0x0, 0x0, 0x0, 0x3f, 0xc0,
    0x0, 0x0, 0x7, 0xfd, 0x0, 0x0, 0x0, 0xbf,
    0xe0, 0x0, 0x0, 0xf, 0xff, 0x0, 0x0, 0x0,
    0xff, 0xf0, 0x0, 0x0, 0xf, 0xaf, 0x0, 0x0,
    0x1, 0xf5, 0xf4, 0x0, 0x0, 0x2f, 0xf, 0x80,
    0x0, 0x3, 0xf0, 0xfc, 0x0, 0x0, 0x3f, 0xf,
    0xc0, 0x0, 0x7, 0xe0, 0xbd, 0x0, 0x0, 0xbd,
    0x7, 0xe0, 0x0, 0xf, 0xc0, 0x3f, 0x0, 0x0,
    0xfc, 0x3, 0xf0, 0x0, 0xf, 0xc0, 0x3f, 0x0,
    0x1, 0xf8, 0x2, 0xf4, 0x0, 0x2f, 0x40, 0x1f,
    0x80, 0x3, 0xf0, 0x0, 0xfc, 0x0, 0x3f, 0xff,
    0xff, 0xc0, 0x7, 0xff, 0xff, 0xfd, 0x0, 0xbf,
    0xff, 0xff, 0xe0, 0xf, 0xd0, 0x0, 0x7f, 0x0,
    0xfc, 0x0, 0x3, 0xf0, 0xf, 0xc0, 0x0, 0x3f,
    0x1, 0xf8, 0x0, 0x2, 0xf4, 0x2f, 0x40, 0x0,
    0x1f, 0x83, 0xf4, 0x0, 0x1, 0xfc, 0x3f, 0x0,
    0x0, 0xf, 0xc0,

    /* U+0056 "V" */
    0x3f, 0x0, 0x0, 0xf, 0xc3, 0xf4, 0x0, 0x1,
    0xfc, 0x2f, 0x40, 0x0, 0x1f, 0x81, 0xf8, 0x0,
    0x2, 0xf4, 0xf, 0xc0, 0x0, 0x3f, 0x0, 0xfc,
    0x0, 0x3, 0xf0, 0xf, 0xd0, 0x0, 0x7f, 0x0,
    0xbe, 0x0, 0xb, 0xe0, 0x7, 0xe0, 0x0, 0xbd,
    0x0, 0x3f, 0x0, 0xf, 0xc0, 0x3, 0xf0, 0x0,
    0xfc, 0x0, 0x2f, 0x40, 0x1f, 0x80, 0x1, 0xf8,
    0x2, 0xf4, 0x0, 0xf, 0x80, 0x2f, 0x0, 0x0,
    0xfc, 0x3, 0xf0, 0x0, 0xf, 0xc0, 0x3f, 0x0,
    0x0, 0xbd, 0x7, 0xe0, 0x0, 0x7, 0xe0, 0xbd,
    0x0, 0x0, 0x3f, 0xf, 0xc0, 0x0, 0x3, 0xf0,
    0xfc, 0x0, 0x0, 0x2f, 0xf, 0x80, 0x0, 0x1,
    0xf5, 0xf4, 0x0, 0x0, 0xf, 0xaf, 0x0, 0x0,
    0x0, 0xff, 0xf0, 0x0, 0x0, 0xf, 0xff, 0x0,
    0x0, 0x0, 0xbf, 0xe0, 0x0, 0x0, 0x7, 0xfd,
    0x0, 0x0, 0x0, 0x3f, 0xc0, 0x0, 0x0, 0x3,
    0xfc, 0x0, 0x0,

    /* U+0057 "W" */
    0xbd, 0x0, 0x0, 0x7, 0xeb, 0xe0, 0x0, 0x0,
    0xbe, 0x7e, 0x0, 0x0, 0xb, 0xd7, 0xe0, 0x0,
    0x0, 0xbd, 0x3f, 0x0, 0x0, 0xf, 0xc3, 0xf0,
    0x0, 0x0, 0xfc, 0x3f, 0x0, 0x0, 0xf, 0xc3,
    0xf0, 0x0, 0x0, 0xfc, 0x2f, 0x2, 0xf8, 0xf,
    0x82, 0xf0, 0x3f, 0xc0, 0xf8, 0x1f, 0x3, 0xfc,
    0xf, 0x41, 0xf4, 0x3f, 0xc1, 0xf4, 0x1f, 0x43,
    0xfc, 0x1f, 0x40, 0xf4, 0x7f, 0xd1, 0xf0, 0xf,
    0x87, 0xfd, 0x2f, 0x0, 0xf8, 0xba, 0xe2, 0xf0,
    0xf, 0x8b, 0xae, 0x2f, 0x0, 0xbc, 0xf5, 0xf3,
    0xe0, 0xb, 0xcf, 0x5f, 0x3e, 0x0, 0x7c, 0xf0,
    0xf3, 0xd0, 0x7, 0xcf, 0xf, 0x3d, 0x0, 0x3d,
    0xf0, 0xf7, 0xc0, 0x3, 0xee, 0xb, 0xbc, 0x0,
    0x3e, 0xe0, 0xbb, 0xc0, 0x3, 0xfd, 0x7, 0xfc,
    0x0, 0x2f, 0xd0, 0x7f, 0x80, 0x2, 0xfc, 0x3,
    0xf8, 0x0, 0x1f, 0xc0, 0x3f, 0x40, 0x1, 0xfc,
    0x3, 0xf4, 0x0,

    /* U+25B2 "▲" */
    0x0, 0x0, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0,
    0xf0, 0x0, 0x0, 0x0, 0x0, 0x2f, 0x80, 0x0,
    0x0, 0x0, 0x3, 0xfc, 0x0, 0x0, 0x0, 0x0,
    0xbf, 0xe0, 0x0, 0x0, 0x0, 0x1f, 0xff, 0x40,
    0x0, 0x0, 0x3, 0xff, 0xfc, 0x0, 0x0, 0x0,
    0xbf, 0xff, 0xe0, 0x0, 0x0, 0xf, 0xff, 0xff,
    0x0, 0x0, 0x3, 0xff, 0xff, 0xfc, 0x0, 0x0,
    0x7f, 0xff, 0xff, 0xd0, 0x0, 0xf, 0xff, 0xff,
    0xff, 0x0, 0x2, 0xff, 0xff, 0xff, 0xf8, 0x0,
    0x3f, 0xff, 0xff, 0xff, 0xc0, 0xf, 0xff, 0xff,
    0xff, 0xff, 0x1, 0xff, 0xff, 0xff, 0xff, 0xf4,
    0x2f, 0xff, 0xff, 0xff, 0xff, 0x80,

    /* U+25B6 "▶" */
    0x40, 0x0, 0x0, 0x0, 0x7, 0x80, 0x0, 0x0,
    0x0, 0x7f, 0x40, 0x0, 0x0, 0x7, 0xff, 0x0,
    0x0, 0x0, 0x7f, 0xfe, 0x0, 0x0, 0x7, 0xff,
    0xfd, 0x0, 0x0, 0x7f, 0xff, 0xfc, 0x0, 0x7,
    0xff, 0xff, 0xf8, 0x0, 0x7f, 0xff, 0xff, 0xf4,
    0x7, 0xff, 0xff, 0xff, 0xe0, 0x7f, 0xff, 0xff,
    0xff, 0xc7, 0xff, 0xff, 0xff, 0xe0, 0x7f, 0xff,
    0xff, 0xf0, 0x7, 0xff, 0xff, 0xf4, 0x0, 0x7f,
    0xff, 0xf8, 0x0, 0x7, 0xff, 0xfc, 0x0, 0x0,
    0x7f, 0xfd, 0x0, 0x0, 0x7, 0xfe, 0x0, 0x0,
    0x0, 0x7f, 0x0, 0x0, 0x0, 0x7, 0x40, 0x0,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,

    /* U+25BC "▼" */
    0x3f, 0xff, 0xff, 0xff, 0xff, 0xc1, 0xff, 0xff,
    0xff, 0xff, 0xf4, 0xb, 0xff, 0xff, 0xff, 0xfe,
    0x0, 0x3f, 0xff, 0xff, 0xff, 0xc0, 0x1, 0xff,
    0xff, 0xff, 0xf4, 0x0, 0xf, 0xff, 0xff, 0xff,
    0x0, 0x0, 0x7f, 0xff, 0xff, 0xd0, 0x0, 0x2,
    0xff, 0xff, 0xf8, 0x0, 0x0, 0xf, 0xff, 0xff,
    0x0, 0x0, 0x0, 0x7f, 0xff, 0xd0, 0x0, 0x0,
    0x3, 0xff, 0xfc, 0x0, 0x0, 0x0, 0xf, 0xff,
    0x0, 0x0, 0x0, 0x0, 0xbf, 0xe0, 0x0, 0x0,
    0x0, 0x3, 0xfc, 0x0, 0x0, 0x0, 0x0, 0x1f,
    0x40, 0x0, 0x0, 0x0, 0x0, 0xf0, 0x0, 0x0,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0,

    /* U+25C0 "◀" */
    0x0, 0x0, 0x0, 0x0, 0x50, 0x0, 0x0, 0x0,
    0x2d, 0x0, 0x0, 0x0, 0x1f, 0xd0, 0x0, 0x0,
    0xf, 0xfd, 0x0, 0x0, 0xb, 0xff, 0xd0, 0x0,
    0x7, 0xff, 0xfd, 0x0, 0x3, 0xff, 0xff, 0xd0,
    0x2, 0xff, 0xff, 0xfd, 0x1, 0xff, 0xff, 0xff,
    0xd0, 0xbf, 0xff, 0xff, 0xfd, 0x3f, 0xff, 0xff,
    0xff, 0xd0, 0xbf, 0xff, 0xff, 0xfd, 0x0, 0xff,
    0xff, 0xff, 0xd0, 0x1, 0xff, 0xff, 0xfd, 0x0,
    0x2, 0xff, 0xff, 0xd0, 0x0, 0x3, 0xff, 0xfd,
    0x0, 0x0, 0x7, 0xff, 0xd0, 0x0, 0x0, 0xb,
    0xfd, 0x0, 0x0, 0x0, 0xf, 0xd0, 0x0, 0x0,
    0x0, 0x1d, 0x0, 0x0, 0x0, 0x0, 0x0
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 320, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 0, .adv_w = 320, .box_w = 8, .box_h = 7, .ofs_x = 6, .ofs_y = 0},
    {.bitmap_index = 14, .adv_w = 320, .box_w = 16, .box_h = 29, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 130, .adv_w = 320, .box_w = 11, .box_h = 29, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 210, .adv_w = 320, .box_w = 16, .box_h = 29, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 326, .adv_w = 320, .box_w = 17, .box_h = 29, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 450, .adv_w = 320, .box_w = 16, .box_h = 29, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 566, .adv_w = 320, .box_w = 16, .box_h = 29, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 682, .adv_w = 320, .box_w = 16, .box_h = 29, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 798, .adv_w = 320, .box_w = 16, .box_h = 29, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 914, .adv_w = 320, .box_w = 16, .box_h = 29, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 1030, .adv_w = 320, .box_w = 16, .box_h = 29, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 1146, .adv_w = 320, .box_w = 18, .box_h = 29, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1277, .adv_w = 320, .box_w = 18, .box_h = 29, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1408, .adv_w = 320, .box_w = 18, .box_h = 29, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1539, .adv_w = 320, .box_w = 22, .box_h = 17, .ofs_x = -1, .ofs_y = 5},
    {.bitmap_index = 1633, .adv_w = 320, .box_w = 18, .box_h = 21, .ofs_x = 1, .ofs_y = 3},
    {.bitmap_index = 1728, .adv_w = 320, .box_w = 22, .box_h = 17, .ofs_x = -1, .ofs_y = 5},
    {.bitmap_index = 1822, .adv_w = 320, .box_w = 18, .box_h = 21, .ofs_x = 1, .ofs_y = 3}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint16_t unicode_list_0[] = {
    0x0, 0xe, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15,
    0x16, 0x17, 0x18, 0x19, 0x21, 0x36, 0x37, 0x2592,
    0x2596, 0x259c, 0x25a0
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 32, .range_length = 9633, .glyph_id_start = 1,
        .unicode_list = unicode_list_0, .glyph_id_ofs_list = NULL, .list_length = 19, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY
    }
};



/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

#if LV_VERSION_CHECK(8, 0, 0)
/*Store all the custom data of the font*/
static  lv_font_fmt_txt_glyph_cache_t cache;
static const lv_font_fmt_txt_dsc_t font_dsc = {
#else
static lv_font_fmt_txt_dsc_t font_dsc = {
#endif
    .glyph_bitmap = glyph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = NULL,
    .kern_scale = 0,
    .cmap_num = 1,
    .bpp = 2,
    .kern_classes = 0,
    .bitmap_format = 0,
#if LV_VERSION_CHECK(8, 0, 0)
    .cache = &cache
#endif
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
#if LV_VERSION_CHECK(8, 0, 0)
const lv_font_t ui_font_SARASA_40_bpp2 = {
#else
lv_font_t ui_font_SARASA_40_bpp2 = {
#endif
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 29,          /*The maximum line height required by the font*/
    .base_line = 0,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
#if LV_VERSION_CHECK(7, 4, 0) || LVGL_VERSION_MAJOR >= 8
    .underline_position = -2,
    .underline_thickness = 2,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};



#endif /*#if UI_FONT_SARASA_40_BPP2*/

