// SquareLine LVGL GENERATED FILE
// EDITOR VERSION: SquareLine Studio 1.0.5
// LVGL VERSION: 8.2
// PROJECT: Metoo_USB_Meter

#include "ui.h"
#include "lvgl.h"
#include "MetooConfig.h"
#include "log.h"

void Metoo_Screen_D(lv_event_t *e)
{
	// Your code here
}

void Metoo_Restart_REC(lv_event_t *e)
{
	// Your code here
	MData.INA.mode = INA219_R_GO_START;
}

void Metoo_Screen_U(lv_event_t *e)
{
	// Your code here
}

void Metoo_Change_Chart(lv_event_t *e)
{
	// Your code here
}

void Metoo_s4_msg_show(lv_event_t *e)
{
	// Your code here
	lv_label_set_text(ui_MSG_Label1_WARN_S4, "检测过程中输出接口请勿连接任何设备!");
	lv_obj_set_style_text_color(ui_MSG_Label1_WARN_S4, lv_color_hex(0xF44336), LV_PART_MAIN | LV_STATE_DEFAULT);

	lv_label_set_text_fmt(ui_KEY_Label1_S4_MSG, "●");
	lv_obj_set_style_text_color(ui_KEY_Label1_S4_MSG, lv_color_hex(0x1CFF00), LV_PART_MAIN | LV_STATE_DEFAULT);
	lv_label_set_text_fmt(ui_KEY_Label3_S4_MSG, "○");
	lv_obj_set_style_text_color(ui_KEY_Label3_S4_MSG, lv_color_hex(0xFF0000), LV_PART_MAIN | LV_STATE_DEFAULT);
}

void Metoo_S4_MSG_yes(lv_event_t *e)
{
	// Your code here
	lv_label_set_text_fmt(ui_MSG_Label1_WARN_S4, "检测中...");
	lv_obj_set_style_text_color(ui_MSG_Label1_WARN_S4, lv_color_hex(0xFFFAFF), LV_PART_MAIN | LV_STATE_DEFAULT);

	lv_label_set_text_fmt(ui_KEY_Label1_S4_MSG, "◌");
	lv_obj_set_style_text_color(ui_KEY_Label1_S4_MSG, lv_color_hex(0xFFFAFF), LV_PART_MAIN | LV_STATE_DEFAULT);
	lv_label_set_text_fmt(ui_KEY_Label3_S4_MSG, "◌");
	lv_obj_set_style_text_color(ui_KEY_Label3_S4_MSG, lv_color_hex(0xFFFAFF), LV_PART_MAIN | LV_STATE_DEFAULT);

	MData.lvgl.s4_test_state = 50;
	lv_label_set_text_fmt(ui_Label1_S4, "");

	MData.PD->PD_info_state = PDO_START1;
}

void Metoo_S4_MSG_no(lv_event_t *e)
{
	// Your code here
}

void Metoo_s5_setDown(lv_event_t *e)
{
	// Your code here
	if (MData.lvgl.s5_id == 0)
	{
		if (MData.QC.state == QC_ON)
		{
			if (MData.QC.mode == QC_M_3_0)
				MData.QC.setV -= 0.2;
			else if (MData.QC.mode == QC_M_2_0)
			{
				if (MData.QC.setV == 20)
					MData.QC.setV = 12;
				else if (MData.QC.setV == 12)
					MData.QC.setV = 9;
				else if (MData.QC.setV == 9)
					MData.QC.setV = 5;
			}
		}
	}
	else if (MData.lvgl.s5_id == 1)
	{
		if (MData.PD->state == PD_ON)
		{
			if (MData.PD->setV == 9)
			{
				MData.PD->setV = 5;
				MData.PD->PD_V = PD_POWER_OPTION_MAX_5V;
			}
			else if (MData.PD->setV == 12)
			{
				MData.PD->setV = 9;
				MData.PD->PD_V = PD_POWER_OPTION_MAX_9V;
			}
			else if (MData.PD->setV == 15)
			{
				MData.PD->setV = 12;
				MData.PD->PD_V = PD_POWER_OPTION_MAX_12V;
			}
			else if (MData.PD->setV == 20)
			{
				MData.PD->setV = 15;
				MData.PD->PD_V = PD_POWER_OPTION_MAX_15V;
			}
			// MData.PD->state = PD_SET;
		}
		else if (MData.PD->state == PD_PPS_ON)
		{
			MData.PD->setV -= 0.2;
			// MData.PD->state = PD_PPS_SET;
		}
	}
}

void Metoo_s5_msg_show(lv_event_t *e)
{
	// Your code here

	lv_obj_clear_flag(ui_MSG_Label1_WARN_S5, LV_OBJ_FLAG_HIDDEN);
	lv_obj_add_flag(ui_MSG_Label1_S5, LV_OBJ_FLAG_HIDDEN);
	lv_obj_add_flag(ui_MSG_Label2_S5, LV_OBJ_FLAG_HIDDEN);
	lv_obj_add_flag(ui_MSG_Label3_S5, LV_OBJ_FLAG_HIDDEN);

	if (MData.lvgl.s5_id == 0)
	{
		lv_label_set_text(ui_MSG_Label1_S5, "启动QC诱骗?");
		lv_label_set_text(ui_MSG_Label2_S5, "5V 9V...");
		lv_label_set_text(ui_MSG_Label3_S5, "3.6V~20V");
		MData.QC.state = QC_RST;
	}
	else if (MData.lvgl.s5_id == 1)
	{
		lv_label_set_text(ui_MSG_Label1_S5, "启动PD诱骗?");
		lv_label_set_text(ui_MSG_Label2_S5, "5V 9V...");
		lv_label_set_text(ui_MSG_Label3_S5, "PPS");
	}
	MData.lvgl.s5_msg_id = 0;
	lv_obj_set_style_outline_color(ui_MSG_Label2_S5, lv_color_hex(0xFFC107), LV_PART_MAIN | LV_STATE_DEFAULT);
	lv_obj_set_style_outline_color(ui_MSG_Label3_S5, lv_color_hex(0x292831), LV_PART_MAIN | LV_STATE_DEFAULT);

	lv_label_set_text_fmt(ui_KEY_Label1_S5_MSG, "●");
	lv_obj_set_style_text_color(ui_KEY_Label1_S5_MSG, lv_color_hex(0x1CFF00), LV_PART_MAIN | LV_STATE_DEFAULT);
	lv_label_set_text_fmt(ui_KEY_Label2_S5_MSG, "○");
	lv_label_set_text_fmt(ui_KEY_Label3_S5_MSG, "○");
	lv_obj_set_style_text_color(ui_KEY_Label3_S5_MSG, lv_color_hex(0xFF0000), LV_PART_MAIN | LV_STATE_DEFAULT);
}

void Metoo_s5_mode_change(lv_event_t *e)
{
	// Your code here
	MData.lvgl.s5_id = MData.lvgl.s5_id + 1;
	if (MData.lvgl.s5_id > 1)
		MData.lvgl.s5_id = 0;
	if (MData.lvgl.s5_id == 0)
	{
		lv_label_set_text_fmt(ui_TAG_Label_S5, "QC诱骗");
	}
	else if (MData.lvgl.s5_id == 1)
	{
		lv_label_set_text_fmt(ui_TAG_Label_S5, "PD诱骗");
	}
}

void Metoo_s5_setUP(lv_event_t *e)
{
	// Your code here
	if (MData.lvgl.s5_id == 0)
	{
		if (MData.QC.state == QC_ON)
		{
			if (MData.QC.mode == QC_M_3_0)
				MData.QC.setV += 0.2;
			else if (MData.QC.mode == QC_M_2_0)
			{
				// MData.QC.mode = QC_M_2_0;
				if (MData.QC.setV == 5)
					MData.QC.setV = 9;
				else if (MData.QC.setV == 9)
					MData.QC.setV = 12;
				else if (MData.QC.setV == 12)
					MData.QC.setV = 20;
			}
		}
	}
	else if (MData.lvgl.s5_id == 1)
	{
		if (MData.PD->state == PD_ON)
		{
			if (MData.PD->setV == 5)
			{
				MData.PD->setV = 9;
				MData.PD->PD_V = PD_POWER_OPTION_MAX_9V;
			}
			else if (MData.PD->setV == 9)
			{
				MData.PD->setV = 12;
				MData.PD->PD_V = PD_POWER_OPTION_MAX_12V;
			}
			else if (MData.PD->setV == 12)
			{
				MData.PD->setV = 15;
				MData.PD->PD_V = PD_POWER_OPTION_MAX_15V;
			}
			else if (MData.PD->setV == 15)
			{
				MData.PD->setV = 20;
				MData.PD->PD_V = PD_POWER_OPTION_MAX_20V;
			}
			// MData.PD->state = PD_SET;
		}
		else if (MData.PD->state == PD_PPS_ON)
		{
			MData.PD->setV += 0.2;
			// MData.PD->state = PD_PPS_SET;
		}
	}
}

void Metoo_S5_MSG_yes(lv_event_t *e)
{
	// Your code here
	if (lv_obj_has_flag(ui_MSG_Label1_WARN_S5, LV_OBJ_FLAG_HIDDEN))
	{
		if (MData.lvgl.s5_id == 0)
		{
			MData.QC.state = QC_START;
			if (MData.lvgl.s5_msg_id == 0)
				MData.QC.mode = QC_M_2_0;
			else if (MData.lvgl.s5_msg_id == 1)
				MData.QC.mode = QC_M_3_0;
		}
		else if (MData.lvgl.s5_id == 1)
		{
			if (MData.PD->state)
				Metoo_PD_rst(MData.PD);
			if (MData.lvgl.s5_msg_id == 0)
				MData.PD->state = PD_START_1;
			else if (MData.lvgl.s5_msg_id == 1)
				MData.PD->state = PD_PPS_START_1;
			MData.PD->PD_info_state = true;
		}

		lv_label_set_text_fmt(ui_MSG_Label1_S5, "");
		lv_label_set_text_fmt(ui_MSG_Label2_S5, "启动中...");
		lv_label_set_text_fmt(ui_MSG_Label3_S5, "");

		lv_label_set_text_fmt(ui_KEY_Label1_S5_MSG, "◌");
		lv_obj_set_style_text_color(ui_KEY_Label1_S5_MSG, lv_color_hex(0xFFFAFF), LV_PART_MAIN | LV_STATE_DEFAULT);
		lv_label_set_text_fmt(ui_KEY_Label2_S5_MSG, "◌");
		lv_label_set_text_fmt(ui_KEY_Label3_S5_MSG, "◌");
		lv_obj_set_style_text_color(ui_KEY_Label3_S5_MSG, lv_color_hex(0xFFFAFF), LV_PART_MAIN | LV_STATE_DEFAULT);

		lv_obj_set_style_outline_color(ui_MSG_Label2_S5, lv_color_hex(0x292831), LV_PART_MAIN | LV_STATE_DEFAULT);
		lv_obj_set_style_outline_color(ui_MSG_Label3_S5, lv_color_hex(0x292831), LV_PART_MAIN | LV_STATE_DEFAULT);
	}
	else
	{
		lv_obj_add_flag(ui_MSG_Label1_WARN_S5, LV_OBJ_FLAG_HIDDEN);
		lv_obj_clear_flag(ui_MSG_Label1_S5, LV_OBJ_FLAG_HIDDEN);
		lv_obj_clear_flag(ui_MSG_Label2_S5, LV_OBJ_FLAG_HIDDEN);
		lv_obj_clear_flag(ui_MSG_Label3_S5, LV_OBJ_FLAG_HIDDEN);
	}
}

void Metoo_S5_MSG_set(lv_event_t *e)
{
	// Your code here
	if (MData.QC.state == QC_OFF)
	{
		MData.lvgl.s5_msg_id = MData.lvgl.s5_msg_id + 1;
		if (MData.lvgl.s5_msg_id > 1)
			MData.lvgl.s5_msg_id = 0;
		// Your code here
		if (MData.lvgl.s5_msg_id == 0)
		{
			// MData.QC.mode = QC_M_2_0;
			lv_obj_set_style_outline_color(ui_MSG_Label2_S5, lv_color_hex(0xFFC107), LV_PART_MAIN | LV_STATE_DEFAULT);
			lv_obj_set_style_outline_color(ui_MSG_Label3_S5, lv_color_hex(0x292831), LV_PART_MAIN | LV_STATE_DEFAULT);
		}
		else if (MData.lvgl.s5_msg_id == 1)
		{
			// MData.QC.mode = QC_M_3_0;
			lv_obj_set_style_outline_color(ui_MSG_Label2_S5, lv_color_hex(0x292831), LV_PART_MAIN | LV_STATE_DEFAULT);
			lv_obj_set_style_outline_color(ui_MSG_Label3_S5, lv_color_hex(0xFFC107), LV_PART_MAIN | LV_STATE_DEFAULT);
		}
	}
}

void Metoo_S5_MSG_no(lv_event_t *e)
{
	// Your code here
	lv_label_set_text_fmt(ui_KEY_Label1_S5, "●");
	lv_label_set_text_fmt(ui_KEY_Label3_S5, "●");
}
