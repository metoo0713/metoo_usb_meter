#include "Metoo_lvgl.h"
#include "lvgl.h"
#include "MetooConfig.h"
#include "log.h"
void Metoo_lvgl_update(lv_timer_t *t);

lv_timer_t *timer_update0;

lv_chart_series_t *ui_Chart_VA_S3_Ser1;
lv_chart_series_t *ui_Chart_VA_S3_Ser2;

void my_log_cb(const char *buf)
{
    log_info("%s", buf);
}

void Metoo_lvgl_init(void)
{
    // lv_log_register_print_cb(my_log_cb);
    timer_update0 = lv_timer_create(NULL, CHART_TICK, NULL);
    lv_timer_set_cb(timer_update0, Metoo_lvgl_update);

    // Label
    // lv_label_set_recolor(ui_Label_VAW_S2, true);  // 打开颜色设置
    // lv_label_set_recolor(ui_Label_mAh_S2, true);  // 打开颜色设置
    // lv_label_set_recolor(ui_Label_MAX_S4, true);  // 打开颜色设置
    // lv_label_set_recolor(ui_Label_VAWT_S3, true); // 打开颜色设置

    lv_chart_set_point_count(ui_Chart_VA_S3, CHART_NUM);                  // 点的数量
    lv_chart_set_update_mode(ui_Chart_VA_S3, LV_CHART_UPDATE_MODE_SHIFT); // 图表更新模式
    lv_chart_set_range(ui_Chart_VA_S3, LV_CHART_AXIS_PRIMARY_Y, 0, 200);  // 左轴范围
    lv_chart_set_range(ui_Chart_VA_S3, LV_CHART_AXIS_SECONDARY_Y, 0, 80); // 右轴范围
    lv_obj_set_style_size(ui_Chart_VA_S3, 0, LV_PART_INDICATOR);          // 不显示点
    lv_chart_set_div_line_count(ui_Chart_VA_S3, 3, CHART_TIME + 1);       // 设置水平垂直背景风格线，水平线为持续时间+1

    ui_Chart_VA_S3_Ser1 = lv_chart_add_series(ui_Chart_VA_S3, lv_color_hex(0xF44336), LV_CHART_AXIS_PRIMARY_Y);
    ui_Chart_VA_S3_Ser2 = lv_chart_add_series(ui_Chart_VA_S3, lv_color_hex(0x2196F3), LV_CHART_AXIS_SECONDARY_Y);
}

uint8_t old_direction = 0xFF;
uint8_t bar_value = 0;
void Metoo_Bar_loop(void)
{

    if (old_direction != MData.direction)
    {
        switch (MData.direction)
        {
        case IN_MALE:

            lv_obj_set_style_base_dir(ui_Bar1, LV_BASE_DIR_LTR, 0); // 将BAR设置为左到右
            lv_obj_clear_flag(ui_Bar1, LV_OBJ_FLAG_HIDDEN);         // 取消隐藏
            lv_obj_set_style_bg_color(ui_Bar1, lv_color_hex(0x292831), LV_PART_INDICATOR | LV_STATE_DEFAULT);
            lv_obj_set_style_bg_grad_color(ui_Bar1, lv_color_hex(0x00FF00), LV_PART_INDICATOR | LV_STATE_DEFAULT);

            lv_obj_set_style_base_dir(ui_Bar2, LV_BASE_DIR_LTR, 0); // 将BAR设置为左到右
            lv_obj_clear_flag(ui_Bar2, LV_OBJ_FLAG_HIDDEN);         // 取消隐藏
            lv_obj_set_style_bg_color(ui_Bar2, lv_color_hex(0x292831), LV_PART_INDICATOR | LV_STATE_DEFAULT);
            lv_obj_set_style_bg_grad_color(ui_Bar2, lv_color_hex(0x00FF00), LV_PART_INDICATOR | LV_STATE_DEFAULT);

            break;
        case IN_FEMALE:
            lv_obj_set_style_base_dir(ui_Bar1, LV_BASE_DIR_RTL, 0); // 将BAR设置为右到左
            lv_obj_clear_flag(ui_Bar1, LV_OBJ_FLAG_HIDDEN);         // 取消隐藏
            lv_obj_set_style_bg_color(ui_Bar1, lv_color_hex(0x00FF00), LV_PART_INDICATOR | LV_STATE_DEFAULT);
            lv_obj_set_style_bg_grad_color(ui_Bar1, lv_color_hex(0x292831), LV_PART_INDICATOR | LV_STATE_DEFAULT);

            lv_obj_set_style_base_dir(ui_Bar2, LV_BASE_DIR_RTL, 0); // 将BAR设置为右到左
            lv_obj_clear_flag(ui_Bar2, LV_OBJ_FLAG_HIDDEN);         // 取消隐藏
            lv_obj_set_style_bg_color(ui_Bar2, lv_color_hex(0x00FF00), LV_PART_INDICATOR | LV_STATE_DEFAULT);
            lv_obj_set_style_bg_grad_color(ui_Bar2, lv_color_hex(0x292831), LV_PART_INDICATOR | LV_STATE_DEFAULT);
            break;
        case IN_NONE:
            lv_obj_add_flag(ui_Bar1, LV_OBJ_FLAG_HIDDEN); // 隐藏
            lv_obj_add_flag(ui_Bar2, LV_OBJ_FLAG_HIDDEN); // 隐藏
            bar_value = 0;
            break;
        default:
            break;
        }
    }
    if (MData.direction != IN_NONE)
    {
        lv_bar_set_value(ui_Bar1, bar_value += 15, LV_ANIM_ON);
        lv_bar_set_value(ui_Bar2, bar_value, LV_ANIM_ON);
    }
}

void Metoo_Chart_loop(void)
{
    if (MData.INA.mode == INA219_R_START)
    {
        lv_chart_set_next_value(ui_Chart_VA_S3, ui_Chart_VA_S3_Ser1, (int16_t)(MData.INA.voltage * 10));
        lv_chart_set_next_value(ui_Chart_VA_S3, ui_Chart_VA_S3_Ser2, (int16_t)(MData.INA.current * 10));
    }
}

void Metoo_S5_MSG(bool state)
{
    if (state)
    {
        lv_label_set_text_fmt(ui_MSG_Label2_S5, "启动成功!");
        lv_label_set_text_fmt(ui_KEY_Label1_S5, "◐");
        lv_label_set_text_fmt(ui_KEY_Label3_S5, "◑");
    }
    else
    {
        lv_label_set_text_fmt(ui_MSG_Label2_S5, "启动失败!");
        lv_label_set_text_fmt(ui_KEY_Label1_S5, "●");
        lv_label_set_text_fmt(ui_KEY_Label3_S5, "●");
        MData.led_r.state = 1;
        MData.led_g.state = 0;
        Metoo_LED_set(&MData.led_r);
        Metoo_LED_set(&MData.led_g);
    }
    if (lv_obj_get_y(ui_Panel_MSG_S5) == 0)
        msgout_Animation(ui_Panel_MSG_S5, 1000);
}

void Metoo_S4_MSG(bool state)
{
    if (state)
    {
        if (lv_obj_get_y(ui_Panel_MSG_S4) == 0)
        {

            // lv_label_set_text_fmt(ui_Label1_S4, "%s", MData.PD->PD_info);
            lv_label_set_text_fmt(ui_MSG_Label1_WARN_S4, "检测成功!");
            msgout_Animation(ui_Panel_MSG_S4, 1000);
        }
    }
}

void Metoo_S4_set(void)
{
    lv_label_ins_text(ui_Label1_S4, LV_LABEL_POS_LAST, MData.PD->PD_info);
}

void Metoo_S4_loop(void)
{
    if (MData.lvgl.s4_test_state > 0)
    {
        MData.lvgl.s4_test_state--;
        if (MData.lvgl.s4_test_state == 1)
        {
            char buf[64];
            Metoo_QC_test(&MData.QC);
            switch (MData.QC.Version)
            {
            case QC_V_3_0:
                sprintf(buf, "QC2.0  QC3.0\n");
                lv_label_ins_text(ui_Label1_S4, LV_LABEL_POS_LAST, buf);
                break;
            case QC_V_2_0:
                sprintf(buf, "QC2.0\n");
                lv_label_ins_text(ui_Label1_S4, LV_LABEL_POS_LAST, buf);
                break;
            default:
                break;
            }
            Metoo_QC_rst(&MData.QC);

            if (MData.PD->state)
                Metoo_PD_rst(MData.PD);
            MData.PD->state = PD_START_1;
            MData.lvgl.s4_test_state = 0;
        }
    }
}

void Metoo_lvgl_update(lv_timer_t *t)
{
    Metoo_Bar_loop();
    Metoo_Chart_loop();

    // lv_label_set_text_fmt(ui_Label_VAW_S2, "#F44336%2.2fV#\n#2196F3%1.3fA#\n#8BC34A%3.1fW#",
    //                       MData.INA.voltage, MData.INA.current, MData.INA.power);

    // ========================运行时间========================

    uint32_t time_S = millis(); // 计时毫秒

    if (MData.INA.mode == INA219_R_START)
        time_S = (MData.INA.time_now - MData.INA.time_start) / 1000;
    else
        time_S = (MData.INA.time_end - MData.INA.time_start) / 1000;

    uint16_t millisecond = time_S % 1000;
    time_S /= 1000;
    uint8_t second = time_S % 60;
    time_S /= 60;
    uint8_t minute = time_S % 60;
    time_S /= 60;
    uint8_t hour = time_S % 60;

    lv_label_set_text_fmt(ui_Label_V_S2, "%05.2fV", MData.INA.voltage);
    lv_label_set_text_fmt(ui_Label_A_S2, "%05.3fA", MData.INA.current);
    lv_label_set_text_fmt(ui_Label_W_S2, "%05.1fW", MData.INA.power);

    char *mode;
    if (MData.QC.Version == QC_V_2_0)
        mode = "QC2.0";
    else if (MData.QC.Version == QC_V_3_0)
        mode = "QC3.0";
    else
        mode = "     ";

    lv_label_set_text_fmt(ui_Label_mAh_S2, "%09.2fmAh\n%09.3f Wh\n%02d:%02d:%02d:%03d\n+%03.1fV  -%03.1fV\n温度     %.0f℃\n模式   %s",
                          MData.INA.mAh, MData.INA.mwh / 1000, hour, minute, second, millisecond, MData.QC.readDP, MData.QC.readDN, MData.temp.value, mode);

    lv_label_set_text_fmt(ui_Label_V_S3, "%04.2fV", MData.INA.voltage);
    lv_label_set_text_fmt(ui_Label_A_S3, "%04.3fA", MData.INA.current);
    lv_label_set_text_fmt(ui_Label_W_S3, "%04.2fW", MData.INA.power);
    lv_label_set_text_fmt(ui_Label_T_S3, "%.0f℃", MData.temp.value);

    lv_label_set_text_fmt(ui_Label_MAX_S2, "最│%06.3f V\n大│%06.4f A\n值│%06.2f W\n", MData.INA.voltage_MAX, MData.INA.current_MAX, MData.INA.power_MAX);

    lv_label_set_text_fmt(ui_Label_AVG_S2, "平│%06.3f V\n均│%06.4f A\n值│%06.2f W", MData.INA.voltage_AVG, MData.INA.current_AVG, MData.INA.power_AVG);

    lv_label_set_text_fmt(ui_Label_DN_S5, "%3.1fV", MData.QC.readDN);
    lv_label_set_text_fmt(ui_Label_DP_S5, "%3.1fV", MData.QC.readDP);

    if (MData.lvgl.s5_id == 0)
        lv_label_set_text_fmt(ui_Label_SET_V, "%4.2fV", MData.QC.setV);
    else if (MData.lvgl.s5_id == 1)
        lv_label_set_text_fmt(ui_Label_SET_V, "%4.2fV", MData.PD->setV);
    lv_label_set_text_fmt(ui_Label_OUT_V, "%4.2fV", MData.INA.voltage);
}
