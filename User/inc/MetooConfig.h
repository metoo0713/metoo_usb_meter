#ifndef METOOCONFIG_H_
#define METOOCONFIG_H_

#include "n32g45x.h"

#include "Metoo_INA219.h"
#include "Metoo_DAC.h"
#include "Metoo_ADC.h"
#include "Metoo_PWM.h"
#include "Metoo_TIM.h"
#include "Metoo_SPI.h"
#include "Metoo_ST7789.h"
#include "Metoo_KEY.h"
#include "Metoo_LED.h"
#include "Metoo_Tick.h"
#include "Metoo_QC.h"
#include "Metoo_PD.h"
#include "Metoo_lvgl.h"

#define RECORD_NUM 14400      // 5小时每10秒一次数据两个uint16_t
#define RECORD_APART 10000000 // 间隔时间10s*1000000，单位为uS
#define IP_TH (0.000)         // 正方向电流阈值
#define IN_TH (-0.03)        // 反方向电流阈值    

typedef enum
{
    MODE_THR,  // 直通
    MODE_SINK, // 诱骗
} Metoo_mode_t;

typedef enum
{
    IN_NONE,
    IN_MALE,   // 公到母
    IN_FEMALE, // 母到公
} Metoo_direction_t;

typedef struct
{

    Metoo_mode_t mode;
    Metoo_direction_t direction;
    uint8_t blk; // 显示屏背光0-100
    MetooKEY_t key[3];
    MetooLED_t led_r;
    MetooLED_t led_g;
    MetooADC_Data_t ADC_DP;
    MetooADC_Data_t ADC_DN;
    MetooADC_Data_t temp;
    MetooDAC_Data_t DAC_DP;
    MetooDAC_Data_t DAC_DN;
    MetooINA219_Data_t INA;
    MetooQC_Data_t QC;
    MetooLVGL_Data_t lvgl;
    MetooPD_Data_t *PD;
} MetooData_t;

typedef struct
{
    float IB; // 电流校准值
    float sys_c; // 系统电流
} MetooConfig_t;

extern MetooData_t MData;
extern MetooConfig_t Mconfig;
#endif /* METOOCONFIG_H_ */