#include "MetooConfig.h"

#include "n32g45x.h"

MetooData_t MData = {
    .key[0] = {
        .GPIO_port = GPIOA,
        .GPIO_pin = GPIO_PIN_15,
        .EXTI_LINE = EXTI_LINE15,
        .Pin_source = GPIO_PIN_SOURCE15,
        .IRQChannel = EXTI15_10_IRQn,
    },
    .key[1] = {
        .GPIO_port = GPIOB,
        .GPIO_pin = GPIO_PIN_4,
        .EXTI_LINE = EXTI_LINE4,
        .Pin_source = GPIO_PIN_SOURCE4,
        .IRQChannel = EXTI4_IRQn,
    },
    .key[2] = {
        .GPIO_port = GPIOB,
        .GPIO_pin = GPIO_PIN_9,
        .EXTI_LINE = EXTI_LINE9,
        .Pin_source = GPIO_PIN_SOURCE9,
        .IRQChannel = EXTI9_5_IRQn,
    },
    .led_r = {
        .GPIO_port = GPIOA,
        .GPIO_pin = GPIO_PIN_1,
        .state = 0,
    },
    .led_g = {
        .GPIO_port = GPIOA,
        .GPIO_pin = GPIO_PIN_2,
        .state = 1,
    },
    .ADC_DP = {
        .ADC = ADC1,
        .ADC_ch = ADC1_Channel_04_PA3,
        .GPIO_port = GPIOA,
        .GPIO_pin = GPIO_PIN_3,
    },
    .ADC_DN = {
        .ADC = ADC1,
        .ADC_ch = ADC1_Channel_03_PA6,
        .GPIO_port = GPIOA,
        .GPIO_pin = GPIO_PIN_6,
    },
    .temp = {
        .ADC = ADC2,
        .ADC_ch = ADC2_Channel_04_PA7,
        .GPIO_port = GPIOA,
        .GPIO_pin = GPIO_PIN_7,
    },

    .DAC_DP = {
        .GPIO_port = GPIOA,
        .GPIO_pin = GPIO_PIN_4,
        .DAC_ch = DAC_CHANNEL_1,
    },
    .DAC_DN = {
        .GPIO_port = GPIOA,
        .GPIO_pin = GPIO_PIN_5,
        .DAC_ch = DAC_CHANNEL_2,
    },

    .INA = {
        .ID = 0,
        .current_MAX = 0,
        .current_AVG = 0,
        .voltage_MAX = 0,
        .voltage_AVG = 0,
        .power_MAX = 0,
        .power_AVG = 0,
        .mAh = 0,
        .mwh = 0,
        .mode = INA219_R_END,
    },
    .PD =  &MetooPD_Data,
    .mode = MODE_THR,
    .blk = 80,
    .lvgl.s5_msg_id = 0,
};

MetooConfig_t Mconfig = {
    .IB = 0,
    .sys_c = 0,
};
// MetooData_t MData = {};