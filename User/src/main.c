#include "main.h"

#include "hw_config.h"
#include "usb_lib.h"
#include "usb_pwr.h"

#include "Metoo_RCC.h"

#include "MetooConfig.h"

///////////////////////////////////////////PD测试///////////////
#include "Metoo_PD.h"

///////////////////////////////////////////////////////////////

// #include "core.h"
// #include "dpm.h"
// #include "HWIO.h"

// #include "Timing.h"
// #include "PlatformI2C.h"
// #include "HostComm.h"

// static DevicePolicyPtr_t dpm;
// static Port_t ports[NUM_PORTS]; /* Array of ports */

// volatile FSC_BOOL haveINTReady = FALSE;
///////////////////////////////////////////////////////////////

#include "lvgl.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"
#include "ui.h"
#include "Metoo_lvgl.h"
__IO uint32_t TimingDelay = 0;

// extern __IO uint8_t Receive_Buffer[64];
// extern __IO uint32_t Receive_length;
// extern __IO uint32_t length;
// uint8_t Send_Buffer[64];
uint32_t packet_sent = 1;
uint32_t packet_receive = 1;

int CDC_len = 0;
char CDC_buf[64];

uint64_t time_last = 0;
uint64_t time_last_rec = 0;
uint64_t time_used = 0;

uint64_t time1, time0;

void Metoo_ina219_loop(void);

int main(void)
{

    log_init();
    Metoo_RCC_init();

    Metoo_PWM_init();
    Metoo_PWM_set(0); // 启动状态关闭背光

    Metoo_Tick_init();

    GPIO_ConfigPinRemap(GPIO_RMP_SW_JTAG_SW_ENABLE, ENABLE); // 禁用JTAG，启用SW

    USB_Interrupts_Config();

    Set_USBClock();

    USB_Init();

    Metoo_ina219_init();
    // // MData.INA.RData = malloc(sizeof(MetooINA219_RData_t) * RECORD_NUM);

    Metoo_DAC_Deinit();
    Metoo_ADC_init(&MData.ADC_DP);
    Metoo_ADC_init(&MData.ADC_DN);
    Metoo_ADC_init(&MData.temp);

    Metoo_LED_init(&MData.led_r);
    Metoo_LED_init(&MData.led_g);
    Metoo_LED_set(&MData.led_r);
    Metoo_LED_set(&MData.led_g);

    KeyInputExtiInit(&MData.key[0]);
    KeyInputExtiInit(&MData.key[1]);
    KeyInputExtiInit(&MData.key[2]);

    log_info("Metoo USB Meter\n");

    Metoo_SPI_init();
    LCD_Init();
    lv_init();
    lv_port_disp_init();
    lv_port_indev_init();
    ui_init();
    Metoo_lvgl_init();

    Metoo_TIM_init();

    uint8_t BLK_delay = 50;

    GPIO_Initial();

    Mconfig.sys_c = ina219_getCurrent_mA() / 1000; // 校准系统自身耗电，归零
    if (Mconfig.sys_c > IP_TH)
        Mconfig.sys_c = IP_TH;
    else if (Mconfig.sys_c < IN_TH)
        Mconfig.sys_c = IN_TH;

    // Mconfig.sys_c = 0;
    Metoo_PD_init();

    while (1)
    {
        lv_timer_handler();
        Metoo_PD_loop(MData.PD);

        Metoo_QC_loop(&MData.QC);
        Metoo_S4_loop();
        Metoo_ADC_get_T(&MData.temp);
        Metoo_ina219_loop();

        if (BLK_delay == 0)
            Metoo_PWM_set(MData.blk);
        BLK_delay--;
        ////////////////////////////////////////////////////

        // MData.led_g.state = !MData.led_g.state;
        // MData.led_r.state = !MData.led_g.state;
        // Metoo_LED_set(&MData.led_g);
        // Metoo_LED_set(&MData.led_r);
        ////////////////////////////////////////////////////
    }
}

void Metoo_ina219_loop(void)
{

    MData.INA.current = ina219_getCurrent_mA() / 1000;
    if (MData.INA.current > (Mconfig.sys_c + 0.01))
    {
        MData.direction = IN_MALE;
        MData.INA.current = MData.INA.current - Mconfig.sys_c;
        if (MData.INA.mode != INA219_R_START)
            MData.INA.mode = INA219_R_GO_START;
    }
    else if (MData.INA.current < (Mconfig.sys_c - 0.01))
    {
        MData.direction = IN_FEMALE;
        MData.INA.current = -(MData.INA.current - Mconfig.sys_c); // 负电流取绝对值
        if (MData.INA.mode != INA219_R_START)
            MData.INA.mode = INA219_R_GO_START;
    }
    else
    {
        MData.INA.current = 0; // 空载电流清零
        MData.direction = IN_NONE;
    }

    // if (MData.INA.current < 0)
    //     MData.INA.current = -MData.INA.current;

    MData.INA.voltage = ina219_getBusVoltage_V();
    MData.INA.power = MData.INA.voltage * MData.INA.current;

    if (MData.INA.mode == INA219_R_GO_START)
    {
        // MetooINA219_Config.mark = MARK_MAX;

        MData.INA.ID = 0;
        MData.INA.current_MAX = 0;
        MData.INA.current_AVG = 0;
        MData.INA.voltage_MAX = 0;
        MData.INA.voltage_AVG = 0;
        MData.INA.power_MAX = 0;
        MData.INA.power_AVG = 0;
        MData.INA.mAh = 0;
        MData.INA.mwh = 0;

        MData.INA.time_start = micros();
        time_last = micros();

        MData.INA.mode = INA219_R_START;
    }

    // ========================计算能量========================

    MData.INA.time_now = micros();
    time_used = MData.INA.time_now - time_last;
    time_last = MData.INA.time_now;

    MData.INA.mAh += MData.INA.current * time_used / 3600000;
    MData.INA.mwh += MData.INA.power * time_used / 3600000;
    // log_info("time_now = %lld timeused = %lld\n\r",MData.INA.time_now,time_used);
    // ========================计算最大值========================

    if (MData.INA.current_MAX < MData.INA.current)
        MData.INA.current_MAX = MData.INA.current;
    if (MData.INA.voltage_MAX < MData.INA.voltage)
        MData.INA.voltage_MAX = MData.INA.voltage;
    if (MData.INA.power_MAX < MData.INA.power)
        MData.INA.power_MAX = MData.INA.power;

    // ========================记录数据========================
    // TODO: 开启记录就报错
    // if ((MData.INA.time_now - time_last_rec) >= RECORD_APART)
    // {

    time_last_rec = MData.INA.time_now;
    // MData.INA.RData[MData.INA.ID].voltage = MData.INA.voltage * 1000;
    // MData.INA.RData[MData.INA.ID].current = MData.INA.current * 1000;
    MData.INA.voltage_AVG = (MData.INA.voltage_AVG * MData.INA.ID + MData.INA.voltage) / (MData.INA.ID + 1);
    MData.INA.current_AVG = (MData.INA.current_AVG * MData.INA.ID + MData.INA.current) / (MData.INA.ID + 1);
    MData.INA.power_AVG = (MData.INA.power_AVG * MData.INA.ID + MData.INA.power) / (MData.INA.ID + 1);
    MData.INA.ID++;
    // MData.led_g.state = !MData.led_g.state;
    // Metoo_LED_set(&MData.led_g);
    if (MData.INA.ID > RECORD_NUM)
    {
        MData.INA.ID = 0;
    }
    // }
    CDC_len = sprintf(CDC_buf, "time_us = %lld, V = %.3f, A = %.3f\n\r", MData.INA.time_now, MData.INA.voltage, MData.INA.current);
    CDC_Send_DATA((unsigned char *)CDC_buf, CDC_len);
}