#ifndef __GPIO_H__
#define __GPIO_H__

#include "main.h"

void GPIO_ToggleBit(GPIO_Module* GPIOx, uint16_t Pin);


void I2C2_GPIO_Config(void);
void GPIO_Initial(void);

#endif /*__GPIO_H__ */
