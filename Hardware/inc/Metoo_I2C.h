#ifndef METOO_I2C_H_
#define METOO_I2C_H_
#include "main.h"


#define I2C1_OwnAddr 0x10
#define I2C2_OwnAddr 0x20
#define I2C3_OwnAddr 0x30
#define I2C4_OwnAddr 0x40


#define I2C1_UseIT ENABLE
#define I2C2_UseIT DISABLE
#define I2C3_UseIT DISABLE
#define I2C4_UseIT DISABLE

#define I2CT_FLAG_TIMEOUT ((uint32_t)0x1000)
#define I2CT_LONG_TIMEOUT ((uint32_t)(10 * I2CT_FLAG_TIMEOUT))



typedef enum {
    MASTER_OK = 0,
    MASTER_BUSY,
    MASTER_MODE,
    MASTER_TXMODE,
    MASTER_RXMODE,
    MASTER_SENDING,
    MASTER_SENDED,
    MASTER_RECVD,
    MASTER_BYTEF,
    MASTER_BUSERR,
    MASTER_UNKNOW,
    SLAVE_OK = 20,
    SLAVE_BUSY,
    SLAVE_MODE,
    SLAVE_BUSERR,
    SLAVE_UNKNOW,
} ErrCode_t;


typedef struct
{
    I2C_Module *I2C_port;
    uint16_t GPIO_pin;
    uint8_t state;
} MetooI2C_t;


int Metoo_I2C_Write(I2C_Module* I2Cx, uint8_t slaveAddr, uint8_t regAddr, uint8_t* datPtr, uint8_t len);
// ErrCode_t I2C_Read(I2C_Module *I2Cx, uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint8_t count);
// int I2C_Read(I2C_Module* I2Cx, uint8_t slaveAddr, uint8_t regAddr, uint8_t* datPtr, uint8_t len);
int Metoo_I2C_Read(I2C_Module* I2Cx, uint8_t slaveAddr, short regAddr, uint8_t* datPtr, uint16_t len);
#endif /* METOO_I2C_H_ */