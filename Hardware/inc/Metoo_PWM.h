#ifndef METOO_PWM_H_
#define METOO_PWM_H_

#include "main.h"

#define PWM_1               TIM1
#define PWM_1_CH            
#define PWM_1_PIN           GPIO_PIN_8
#define PWM_1_PROT          GPIOA
#define PWM_1_PSC           14 
#define PWM_1_ARR           999
void Metoo_PWM_init(void);
void Metoo_PWM_set(float data);  // 0.0-100.0
#endif /* METOO_PWM_H_ */