#ifndef METOO_TICK_H_
#define METOO_TICK_H_

#include "n32g45x.h"
#define METOO_TICK_PER_SECOND 1000000 // ÿ��̶� 1000000 ��Ӧ1us

void Metoo_Tick_init(void);
void Metoo_Tick_loop(void);
uint64_t micros(void);
uint64_t millis(void);
void Metoo_Delay_ms(uint32_t ms);
void Metoo_Delay_us(uint32_t us);

#endif /* METOO_TICK_H_ */