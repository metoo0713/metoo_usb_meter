#ifndef METOO_LED_H_
#define METOO_LED_H_
#include "n32g45x.h"

typedef struct
{
    GPIO_Module *GPIO_port;
    uint16_t GPIO_pin;
    uint8_t state;
} MetooLED_t;

void Metoo_LED_init(MetooLED_t *led);
// void Metoo_LED_On(MetooLED_t *led);
// void Metoo_LED_Off(MetooLED_t *led);
void Metoo_LED_set(MetooLED_t *led);
#endif /* METOO_LED_H_ */