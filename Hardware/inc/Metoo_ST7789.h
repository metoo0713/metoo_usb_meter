#ifndef METOO_ST7789_H_
#define METOO_ST7789_H_

#include "main.h"

#define LCD_SPI                 SPI1

#define LCD_DC_Pin              GPIO_PIN_6
#define LCD_DC_GPIO_Port        GPIOB
#define LCD_RST_Pin             GPIO_PIN_7
#define LCD_RST_GPIO_Port       GPIOB

#define LCD_SCL_Pin             GPIO_PIN_3
#define LCD_SCL_GPIO_Port       GPIOB
#define LCD_SDA_Pin             GPIO_PIN_5
#define LCD_SDA_GPIO_Port       GPIOB



//LCD的宽和高定义
#define USE_HORIZONTAL 2  //设置横屏或者竖屏显示 0或1为竖屏 2或3为横屏


#if USE_HORIZONTAL==0||USE_HORIZONTAL==1
#define LCD_Width 135
#define LCD_Height 240

#else
#define LCD_Width 240
#define LCD_Height 135
#endif



//--------------------------------- 

// #define	LCD_PWR(n)		(n?GPIO_SetBits(LCD_PWR_GPIO_Port,LCD_PWR_Pin):GPIO_ResetBits(LCD_PWR_GPIO_Port,LCD_PWR_Pin))
#define	LCD_RST(n)		(n?GPIO_SetBits(LCD_RST_GPIO_Port,LCD_RST_Pin):GPIO_ResetBits(LCD_RST_GPIO_Port,LCD_RST_Pin))
#define	LCD_DC(n)			(n?GPIO_SetBits(LCD_DC_GPIO_Port,LCD_DC_Pin):GPIO_ResetBits(LCD_DC_GPIO_Port,LCD_DC_Pin))



void LCD_Init(void);																	//初始化
void LCD_DisplayOn(void);																//开显示
void LCD_DisplayOff(void);																//关显示
void LCD_Write_HalfWord(const uint16_t da);													//写半个字节数据到LCD
void LCD_Address_Set(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);									//设置数据显示区域
// void LCD_Clear(uint16_t color);																//清屏
// void LCD_Fill(uint16_t x_start, uint16_t y_start, uint16_t x_end, uint16_t y_end, uint16_t color);				//填充单色
// void LCD_Draw_Point(uint16_t x, uint16_t y);														//画点
// void LCD_Draw_ColorPoint(uint16_t x, uint16_t y,uint16_t color);										//画带颜色点
// void LCD_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);										//画线
// void LCD_DrawRectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);									//画矩形
// void LCD_Draw_Circle(uint16_t x0, uint16_t y0, uint8_t r);												//画圆
// void LCD_ShowChar(uint16_t x, uint16_t y, char chr, uint8_t size);										//显示一个字符
// void LCD_ShowNum(uint16_t x,uint16_t y,uint32_t num,uint8_t len,uint8_t size);									//显示一个数字
// void LCD_ShowxNum(uint16_t x,uint16_t y,uint32_t num,uint8_t len,uint8_t size,uint8_t mode);							//显示数字
// void LCD_ShowString(uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t size,char *p);					//显示字符串
// void LCD_Show_Image(uint16_t x, uint16_t y, uint16_t width, uint16_t height, const uint8_t *p);					//显示图片
// void Display_ALIENTEK_LOGO(uint16_t x,uint16_t y);												//显示ALIENTEK LOGO
// void LCD_Draw_Point1(uint16_t x, uint16_t y,uint8_t t);
void put_px(uint16_t x, uint16_t y,void *color);                                             // lvgl 画点
void Metoo_flush(int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint32_t data, uint32_t size);
void Metoo_DMA_flush(int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint32_t data, uint32_t size);
void fillScreen_test(u16 color);


#endif /* METOO_ST7789_H_ */