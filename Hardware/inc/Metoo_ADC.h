#ifndef METOO_ADC_H_
#define METOO_ADC_H_

#include "main.h"

#define ADC_1 ADC1
#define ADC_1_CH ADC1_Channel_04_PA3
#define ADC_1_PORT GPIOA
#define ADC_1_PIN GPIO_PIN_3

#define ADC_2 ADC1
#define ADC_2_CH ADC1_Channel_03_PA6
#define ADC_2_PORT GPIOA
#define ADC_2_PIN GPIO_PIN_6

typedef struct
{
    float value;
    uint16_t raw;
    ADC_Module *ADC;
    uint8_t ADC_ch;
    GPIO_Module *GPIO_port;
    uint16_t GPIO_pin;
} MetooADC_Data_t;

void Metoo_ADC_init(MetooADC_Data_t *MetooADC);
uint16_t Metoo_ADC_get_RAW(MetooADC_Data_t *MetooADC);
float Metoo_ADC_get_V(MetooADC_Data_t *MetooADC);
float Metoo_ADC_get_T(MetooADC_Data_t *MetooADC);
#endif /* METOO_ADC_H_ */