#ifndef METOO_DAC_H_
#define METOO_DAC_H_


#include <stdint.h>
#include "main.h"


/* Defines ********************************************************************/

#define DAC_1                   DAC_CHANNEL_1
#define DAC_1_PORT              GPIOA
#define DAC_1_PIN               GPIO_PIN_4

#define DAC_2                   DAC_CHANNEL_2
#define DAC_2_PORT              GPIOA
#define DAC_2_PIN               GPIO_PIN_5


typedef struct
{
    float value;
    uint16_t raw;
    uint32_t DAC_ch;
    GPIO_Module *GPIO_port;
    uint16_t GPIO_pin;
} MetooDAC_Data_t;


void Metoo_DAC_init(MetooDAC_Data_t *MetooDAC);
void Metoo_DAC_Deinit(void);
void Metoo_DAC_set_V(MetooDAC_Data_t *MetooDAC, float Voltage);


#endif /* METOO_DAC_H_ */