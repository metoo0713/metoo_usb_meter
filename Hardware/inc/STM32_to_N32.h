/////////////// system
#define systick_delayMs Metoo_Delay_ms


///////////////I2C

#define I2C_Cmd                                         I2C_Enable
#define I2C_InitTypeDef                                 I2C_InitType
#define RCC_APB1PeriphClockCmd                          RCC_EnableAPB1PeriphClk
#define I2C_Mode                                        BusMode
#define I2C_DutyCycle                                   FmDutyCycle
#define I2C_OwnAddress1                                 OwnAddr1
#define I2C_Ack                                         AckEnable
#define I2C_AcknowledgedAddress                         AddrMode
#define I2C_ClockSpeed                                  ClkSpeed
#define I2C_Mode_I2C                                    I2C_BUSMODE_I2C
#define I2C_DutyCycle_2                                 I2C_FMDUTYCYCLE_2
#define I2C_Ack_Enable                                  I2C_ACKEN
#define I2C_AcknowledgedAddress_7bit                    I2C_ADDR_MODE_7BIT
#define I2C_GetFlagStatus                               I2C_GetFlag
#define I2C_AcknowledgeConfig                           I2C_ConfigAck
#define I2C_GenerateSTART                               I2C_GenerateStart
#define I2C_Send7bitAddress                             I2C_SendAddr7bit
#define I2C_GenerateSTOP                                I2C_GenerateStop
#define I2C_Direction_Transmitter                       I2C_DIRECTION_SEND
#define I2C_Direction_Receiver                          I2C_DIRECTION_RECV

#define I2C_EVENT_MASTER_MODE_SELECT                    I2C_EVT_MASTER_MODE_FLAG
#define I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED      I2C_EVT_MASTER_TXMODE_FLAG
#define I2C_EVENT_MASTER_BYTE_TRANSMITTED               I2C_EVT_MASTER_DATA_SENDING
#define I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED         I2C_EVT_MASTER_RXMODE_FLAG
#define I2C_EVENT_MASTER_BYTE_RECEIVED                  I2C_FLAG_RXDATNE
#define I2C_ReceiveData                                 I2C_RecvData



/////////////SPI
#define SPI_InitTypeDef                     SPI_InitType
#define SPI_I2S_GetFlagStatus               SPI_I2S_GetStatus
#define SPI_I2S_SendData                    SPI_I2S_TransmitData
// #define
// #define
// #define
// #define
// #define
// #define
// #define
// #define
// #define
// #define
// #define
// #define
// #define


//////////////GPIO

#define GPIO_InitTypeDef            GPIO_InitType
#define RCC_APB2PeriphClockCmd      RCC_EnableAPB2PeriphClk
#define GPIO_Pin                    Pin
#define GPIO_Init                   GPIO_InitPeripheral