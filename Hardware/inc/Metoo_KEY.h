#ifndef METOO_KEY_H_
#define METOO_KEY_H_
#include "main.h"

#define BUTTON_CLICKED_TIME 20
#define BUTTON_PRESSED_LONG_TIME 1000
typedef enum
{
    BUTTON_RELEASED = 0,
    BUTTON_PRESSED,
    BUTTON_CLICKED,
    BUTTON_PRESSED_LONG,
} Metoo_KEY_state_t;

typedef struct
{
    GPIO_Module *GPIO_port;
    uint16_t GPIO_pin;
    uint32_t EXTI_LINE;
    uint8_t Pin_source;
    IRQn_Type IRQChannel;
    Metoo_KEY_state_t state;
    uint32_t pressed_time;
    uint32_t repeating_time;

} MetooKEY_t;

extern MetooKEY_t key1;
extern MetooKEY_t key2;
extern MetooKEY_t key3;

void KeyInputExtiInit(MetooKEY_t *key);
void key_Handler(MetooKEY_t *key);
#endif /* METOO_KEY_H_ */