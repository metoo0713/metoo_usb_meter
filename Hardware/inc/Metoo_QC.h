#ifndef METOO_QC_H_
#define METOO_QC_H_
#include "main.h"

typedef enum
{
    QC_NONE,
    QC_V_1_0,
    QC_V_2_0,
    QC_V_3_0,
} MetooQC_Version_t;

typedef enum
{
    QC_M_2_0,
    QC_M_3_0,
} MetooQC_Mode_t;

typedef enum
{
    QC_OFF,
    QC_START,
    QC_ON,
    QC_FAIL,
    QC_RST,
    QC_TEST,
} MetooQC_State_t;

typedef struct
{
    MetooQC_State_t state;
    MetooQC_Version_t Version;
    MetooQC_Mode_t mode;
    float setV;
    float setDP;
    float setDN;
    float readDP;
    float readDN;
} MetooQC_Data_t;

bool Metoo_QC_init(MetooQC_Data_t *QC);
void Metoo_QC_loop(MetooQC_Data_t *QC);
void Metoo_QC_read(MetooQC_Data_t *QC);
void Metoo_QC_rst(MetooQC_Data_t *QC);
void Metoo_QC_set(MetooQC_Data_t *QC);
void Metoo_QC_check(MetooQC_Data_t *QC);
void Metoo_QC_test(MetooQC_Data_t *QC);
#endif /* METOO_QC_H_ */