#ifndef METOO_SPI_H_
#define METOO_SPI_H_
#include "main.h"

#define HARD_SPI
// #define SOFT_SPI


#define SPI_MASTER          SPI1
#define SPI_MASTER_CLK      RCC_APB2_PERIPH_SPI1

//#define SPI_MASTER_NSS_GPIO           GPIOC
#define SPI_MASTER_SCK_GPIO         GPIOB
// #define SPI_MASTER_MISO_GPIO            GPIOA
#define SPI_MASTER_MOSI_GPIO        GPIOB

//#define SPI_MASTER_PIN_NSS        GPIO_PIN_2
#define SPI_MASTER_PIN_SCK          GPIO_PIN_3

// #define SPI_MASTER_PIN_MISO       GPIO_PIN_0
#define SPI_MASTER_PIN_MOSI         GPIO_PIN_5

#define SPI_MASTER_DMA            DMA1
#define SPI_MASTER_DMA_CLK        RCC_AHB_PERIPH_DMA1
#define SPI_MASTER_Tx_DMA_Channel DMA1_CH1
#define SPI_MASTER_Tx_DMA_FLAG    DMA1_FLAG_TC1
// #define SPI_MASTER_Rx_DMA_Channel DMA1_CH2
// #define SPI_MASTER_Rx_DMA_FLAG    DMA1_FLAG_TC2

#define SPI_SLAVE_DR_Base  0x4000380C
#define SPI_MASTER_DR_Base 0x4001300C

void Metoo_SPI_init(void);
void Metoo_SPI_SetSpeed(uint8_t SpeedSet);
uint8_t Metoo_SPI_ReadWriteByte(uint8_t TxData);

#endif /*METOO_SPI_H_*/
