#include "Metoo_QC.h"
#include "MetooConfig.h"
#include "log.h"
uint32_t time_mark = 0;

bool Metoo_QC_init(MetooQC_Data_t *QC)
{
    // MetooQC_Mode_t set_mode = QC->mode;
    Metoo_DAC_init(&MData.DAC_DP);
    Metoo_DAC_set_V(&MData.DAC_DP, 0.6);
    Metoo_Delay_ms(100);
    Metoo_QC_read(QC);
    if (QC->readDN > 0.3 && QC->readDN < 0.7)
    {
        Metoo_Delay_ms(1300);
        Metoo_DAC_init(&MData.DAC_DN);
        Metoo_DAC_set_V(&MData.DAC_DP, 0.6);
        Metoo_DAC_set_V(&MData.DAC_DN, 0);
        QC->state = QC_ON;
        Metoo_S5_MSG(true);
        return true;
    }
    else
    {
        QC->Version = QC_V_1_0;
        Metoo_DAC_Deinit();
        QC->state = QC_FAIL;
        Metoo_S5_MSG(false);
        return false;
    }
}

void Metoo_QC_read(MetooQC_Data_t *QC)
{
    QC->readDP = Metoo_ADC_get_V(&MData.ADC_DP);
    QC->readDN = Metoo_ADC_get_V(&MData.ADC_DN);
}

void Metoo_QC_test(MetooQC_Data_t *QC)
{
    bool rst = Metoo_QC_init(QC);
    if (rst)
    { // QC 2.0���
        QC->mode = QC_M_2_0;
        QC->setV = 9;
        Metoo_QC_set(QC);
        Metoo_Delay_ms(500);
        float v = ina219_getBusVoltage_V();
        if (8.8 < v && v < 9.2)
            QC->Version = QC_V_2_0;

        QC->setV = 5;
        Metoo_QC_set(QC);
        Metoo_Delay_ms(50);
        // QC 3.0���
        Metoo_DAC_set_V(&MData.DAC_DP, 0.6);
        Metoo_DAC_set_V(&MData.DAC_DN, 3.3);

        QC->mode = QC_M_3_0;
        QC->setV = 6.0;
        Metoo_QC_set(QC);
        Metoo_Delay_ms(50);
        Metoo_QC_set(QC);
        Metoo_Delay_ms(50);
        Metoo_QC_set(QC);
        Metoo_Delay_ms(50);
        Metoo_QC_set(QC);
        Metoo_Delay_ms(50);
        Metoo_QC_set(QC);
        Metoo_Delay_ms(50);
        Metoo_Delay_ms(500);
        v = ina219_getBusVoltage_V();
        if (5.8 < v && v < 6.2)
            QC->Version = QC_V_3_0;
        Metoo_Delay_ms(50);

        QC->mode = QC_M_2_0;
        QC->setV = 5;
        Metoo_QC_set(QC);
    }
}

void Metoo_QC_check(MetooQC_Data_t *QC)
{
    if ((0.3 < QC->readDP && QC->readDP < 0.9) || (3.3 < QC->readDP && QC->readDP < 3.6))
        QC->Version = QC_V_2_0;
    else
        QC->Version = QC_NONE;

    if ((0.3 < QC->readDP && QC->readDP < 0.9) && (3.0 < QC->readDN && QC->readDN < 3.6))
        QC->Version = QC_V_3_0;
}

void Metoo_QC_set(MetooQC_Data_t *QC)
{
    if (QC->mode == QC_M_3_0)
    {
        float v = ina219_getBusVoltage_V();
        if (v > QC->setV)
        {
            if ((v - QC->setV) >= 0.15)
            {
                Metoo_DAC_set_V(&MData.DAC_DN, 0.6);
                Metoo_Delay_ms(5);
                Metoo_DAC_set_V(&MData.DAC_DN, 3.3);
                Metoo_Delay_ms(5);
            }
        }

        else if (v < QC->setV)
        {
            if ((QC->setV - v) >= 0.15)
            {
                Metoo_DAC_set_V(&MData.DAC_DP, 3.3);
                Metoo_Delay_ms(5);
                Metoo_DAC_set_V(&MData.DAC_DP, 0.6);
                Metoo_Delay_ms(5);
            }
        }
    }
    else if (QC->mode == QC_M_2_0)
    {
        uint8_t V = QC->setV;
        switch (V)
        {
        case 5:
            Metoo_DAC_set_V(&MData.DAC_DP, 0.6);
            Metoo_DAC_set_V(&MData.DAC_DN, 0.0);
            break;
        case 9:
            Metoo_DAC_set_V(&MData.DAC_DP, 3.3);
            Metoo_DAC_set_V(&MData.DAC_DN, 0.6);
            break;
        case 12:
            Metoo_DAC_set_V(&MData.DAC_DP, 0.6);
            Metoo_DAC_set_V(&MData.DAC_DN, 0.6);
            break;
        case 20:
            Metoo_DAC_set_V(&MData.DAC_DP, 3.3);
            Metoo_DAC_set_V(&MData.DAC_DN, 3.3);
            break;
        default:
            break;
        }
    }
}

void Metoo_QC_rst(MetooQC_Data_t *QC)
{
    if (QC->state != QC_OFF)
    {
        Metoo_DAC_init(&MData.DAC_DP);
        Metoo_DAC_init(&MData.DAC_DN);
        Metoo_DAC_set_V(&MData.DAC_DP, 0);
        Metoo_DAC_set_V(&MData.DAC_DN, 0);
        Metoo_Delay_ms(1000);
        Metoo_DAC_Deinit();
        QC->setV = 5;
        QC->state = QC_OFF;
    }
}

void Metoo_QC_loop(MetooQC_Data_t *QC)
{
    switch (QC->state)
    {
    case QC_START:
        Metoo_QC_init(QC);
        if (MData.QC.mode == QC_M_3_0)
        {
            Metoo_DAC_set_V(&MData.DAC_DP, 0.6);
            Metoo_DAC_set_V(&MData.DAC_DN, 3.3);
        }

        break;
    case QC_ON:
        Metoo_QC_set(QC);
        break;

    case QC_RST:
        Metoo_QC_rst(QC);
        break;
        // case QC_TEST:
        //     Metoo_QC_test(QC);
        break;
    default:
        break;
    }

    Metoo_QC_read(QC);
    Metoo_QC_check(QC);
}
