#include "gpio.h"


void GPIO_ToggleBit(GPIO_Module* GPIOx, uint16_t Pin) { GPIOx->POD ^= Pin; }

void I2C2_GPIO_Config(void)
{
    GPIO_InitType GPIO_InitStructure;
    /* Enable the GPIO Clock */
    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO | RCC_APB2_PERIPH_GPIOB, ENABLE);

    // GPIO_InitStruct(&GPIO_InitStructure);
    /*PB10 -- SCL; PB11 -- SDA*/
    GPIO_InitStructure.Pin = GPIO_PIN_10 | GPIO_PIN_11;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
    GPIO_InitPeripheral(GPIOB, &GPIO_InitStructure);
}


void FUSB302_GPIO_init(void)
{
    // Initialize FUSB302
    GPIO_InitType GPIO_InitStructure;
    // GPIO init
    // RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOB | RCC_APB2_PERIPH_AFIO, ENABLE);

    GPIO_InitStruct(&GPIO_InitStructure);

    GPIO_InitStructure.Pin = GPIO_PIN_8;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitPeripheral(GPIOB, &GPIO_InitStructure);

}


void GPIO_Initial(void)
{
    // Disable_JTAG_Config();  // Keep only SWD (PA13 PA14)
    // LOG_GPIO_Config();      // UART1 (PA9 PA10)
    // LED_GPIO_Config();      // R(PA8) B(PB4) G(PB5)
    // I2C1_GPIO_Config();     // I2C1(PB6 PB7)
    // I2C2_GPIO_Config();  // I2C2(PB10 PB11)
    // I2C4_GPIO_Config();  // I2C2(PD14 PD15)
    FUSB302_GPIO_init();
}


