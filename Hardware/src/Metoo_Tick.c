#include "Metoo_Tick.h"
#include "log.h"
static __IO uint64_t systick_count = 0; // ����

void Metoo_Tick_init(void)
{
    /* Setup SysTick Timer for 1 msec interrupts  SystemCoreClock */
    if (SysTick_Config(SystemCoreClock / METOO_TICK_PER_SECOND))
    {
        /* Capture error */
        while (1)
            ;
    }
}

uint64_t micros(void)
{
    return systick_count;
}

uint64_t millis(void)
{
    return systick_count / 1000;
}

void Metoo_Delay_us(uint32_t us)
{
    uint64_t time = us + systick_count;
    while (systick_count < time)
    {
        /* code */
    }
}

void Metoo_Delay_ms(uint32_t ms)
{
    uint64_t time = ms * 1000 + systick_count;
    // log_info("IN time = %lld systick_count = %lld\n\r", time, systick_count);
    while (systick_count < time)
    {
        /* code */
    }
    // log_info("OUT time = %lld systick_count = %lld\n\r", time, systick_count);
}

void Metoo_Tick_loop(void)
{
    systick_count++;
}