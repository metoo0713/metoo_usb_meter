#include "Metoo_LED.h"

void Metoo_LED_init(MetooLED_t *led)
{
    GPIO_InitType GPIO_InitStructure;

    /* Check the parameters */
    assert_param(IS_GPIO_ALL_PERIPH(led->GPIO_port));

    /* Enable the GPIO Clock */
    if (led->GPIO_port == GPIOA)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOA, ENABLE);
    }
    else if (led->GPIO_port == GPIOB)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOB, ENABLE);
    }
    else if (led->GPIO_port == GPIOC)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOC, ENABLE);
    }
    else if (led->GPIO_port == GPIOD)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOD, ENABLE);
    }
    else if (led->GPIO_port == GPIOE)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOE, ENABLE);
    }
    else if (led->GPIO_port == GPIOF)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOF, ENABLE);
    }
    else if (led->GPIO_port == GPIOG)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOG, ENABLE);
    }

    /* Configure the GPIO pin */
    if ((led->GPIO_pin) <= GPIO_PIN_ALL)
    {
        GPIO_InitStructure.Pin = led->GPIO_pin;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitPeripheral(led->GPIO_port, &GPIO_InitStructure);
    }
}

void Metoo_LED_set(MetooLED_t *led)
{
    if (led->state)
        GPIO_SetBits(led->GPIO_port, led->GPIO_pin);
    else
        GPIO_ResetBits(led->GPIO_port, led->GPIO_pin);
}
