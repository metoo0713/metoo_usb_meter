#include "Metoo_PWM.h"

float k = (PWM_1_ARR + 1) / 100; // ת��ϵ��

void Metoo_PWM_init(void)
{
    GPIO_InitType GPIO_InitStructure;
    /* GPIOA Configuration:TIM3 Channel1, 2, 3 and 4 as alternate function push-pull */

    TIM_TimeBaseInitType TIM_TimeBaseStructure;
    OCInitType TIM_OCInitStructure;
    uint16_t TimerPeriod = 0;
    uint16_t Channel1Pulse = 0;

    /* GPIOA clock enable */
    // RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOA | RCC_APB2_PERIPH_TIM1| RCC_APB2_PERIPH_AFIO, ENABLE);
    // GPIO_ConfigPinRemap(GPIO_PART1_RMP_TIM1, ENABLE);
    GPIO_InitStructure.Pin = PWM_1_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

    GPIO_InitPeripheral(PWM_1_PROT, &GPIO_InitStructure);
    // DBG_ConfigPeriph(DBG_TIM1_STOP, ENABLE);
    // GPIO_PinRemapConfig(GPIO_FullRemap_TIM1, ENABLE);

    /* Time Base configuration */
    TIM_TimeBaseStructure.Prescaler = PWM_1_PSC;
    TIM_TimeBaseStructure.Period = PWM_1_ARR;
    TIM_TimeBaseStructure.CntMode = TIM_CNT_MODE_UP;
    TIM_TimeBaseStructure.ClkDiv = 0;
    TIM_TimeBaseStructure.RepetCnt = 0;
    TIM_InitTimeBase(PWM_1, &TIM_TimeBaseStructure);

    /* Channel 1,Configuration in PWM mode */
    TIM_OCInitStructure.OcMode = TIM_OCMODE_PWM1;
    TIM_OCInitStructure.Pulse = 0;
    TIM_OCInitStructure.OutputState = TIM_OUTPUT_STATE_ENABLE;
    TIM_OCInitStructure.OcPolarity = TIM_OC_POLARITY_HIGH;
    TIM_OCInitStructure.OcIdleState = TIM_OC_IDLE_STATE_SET;
    TIM_OCInitStructure.OutputNState = TIM_OUTPUT_NSTATE_DISABLE;
    TIM_OCInitStructure.OcNPolarity = TIM_OCN_POLARITY_LOW;
    TIM_OCInitStructure.OcNIdleState = TIM_OC_IDLE_STATE_RESET;
    TIM_InitOc1(PWM_1, &TIM_OCInitStructure);

    TIM_ConfigArPreload(PWM_1, ENABLE);

    /* TIM1 counter enable */
    TIM_Enable(PWM_1, ENABLE);

    /* TIM1 Main Output Enable */
    TIM_EnableCtrlPwmOutputs(PWM_1, ENABLE);
}

void Metoo_PWM_set(float Duty)
{
    // OCInitType TIM_OCInitStructure;
    // TIM_OCInitStructure.Pulse = data;
    // TIM_InitOc1(PWM_1, &TIM_OCInitStructure);
    uint16_t data = Duty * k;
    TIM_SetCmp1(PWM_1, data);
}