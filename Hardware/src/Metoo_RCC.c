#include "Metoo_RCC.h"

void Metoo_RCC_init(void)
{


    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOA |
                                RCC_APB2_PERIPH_GPIOB |
                                // RCC_APB2_PERIPH_GPIOC |
                                // RCC_APB2_PERIPH_GPIOD |
                                // RCC_APB2_PERIPH_GPIOE |
                                // RCC_APB2_PERIPH_GPIOF |
                                // RCC_APB2_PERIPH_GPIOG,
                                RCC_APB2_PERIPH_TIM1 |
                                RCC_APB2_PERIPH_AFIO,
                            ENABLE);

    RCC_EnableAHBPeriphClk(RCC_AHB_PERIPH_ADC1 | RCC_AHB_PERIPH_ADC2, ENABLE);
    RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_DAC | RCC_APB1_PERIPH_TIM6 | RCC_APB1_PERIPH_I2C2, ENABLE);
    RCC_ConfigPclk2(RCC_HCLK_DIV2);
}
