#include "Metoo_ADC.h"

void adc_gpio_init(void);
void adc_init(void);

#define NTC_3450_START -30
#define NTC_3450_END 100
#define NTC_3450_SIZE 130
uint16_t NTC_3450[130] = {3783, 3767, 3751, 3734, 3716, 3697, 3678, 3659, 3638, 3617, // NTC3450 �¶ȷ�Χ -30 100
                          3595, 3573, 3550, 3526, 3501, 3476, 3450, 3424, 3396, 3368, //
                          3339, 3310, 3280, 3249, 3218, 3186, 3153, 3120, 3086, 3052, //
                          3017, 2982, 2946, 2909, 2872, 2835, 2798, 2760, 2721, 2682, //
                          2644, 2604, 2565, 2525, 2486, 2446, 2406, 2366, 2326, 2286, //
                          2246, 2206, 2166, 2127, 2087, 2048, 2009, 1970, 1932, 1893, //
                          1855, 1818, 1780, 1744, 1707, 1671, 1635, 1600, 1566, 1531, //
                          1497, 1464, 1431, 1399, 1367, 1336, 1305, 1275, 1246, 1217, //
                          1188, 1160, 1133, 1106, 1079, 1053, 1028, 1003, 979, 955,   //
                          932, 909, 887, 865, 844, 823, 803, 783, 764, 745,           //
                          727, 709, 691, 674, 658, 641, 625, 610, 595, 580,           //
                          566, 552, 538, 525, 512, 499, 487, 475, 463, 452,           //
                          441, 430, 420, 410, 400, 390, 380, 371, 362, 354};          //

void Metoo_ADC_init(MetooADC_Data_t *MetooADC)
{
    GPIO_InitType GPIO_InitStructure;

    GPIO_InitStructure.Pin = MetooADC->GPIO_pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_InitPeripheral(MetooADC->GPIO_port, &GPIO_InitStructure); // ADC_1 ADC_2 ͬһ��PORT

    /* RCC_ADCHCLK_DIV16*/
    ADC_ConfigClk(ADC_CTRL3_CKMOD_AHB, RCC_ADCHCLK_DIV16);

    ADC_InitType ADC_InitStructure;
    /* ADC configuration ------------------------------------------------------*/
    ADC_InitStructure.WorkMode = ADC_WORKMODE_INDEPENDENT;
    ADC_InitStructure.MultiChEn = DISABLE;
    ADC_InitStructure.ContinueConvEn = DISABLE;
    ADC_InitStructure.ExtTrigSelect = ADC_EXT_TRIGCONV_NONE;
    ADC_InitStructure.DatAlign = ADC_DAT_ALIGN_R;
    ADC_InitStructure.ChsNumber = 1;
    ADC_Init(MetooADC->ADC, &ADC_InitStructure);

    /* Enable ADC */
    ADC_Enable(MetooADC->ADC, ENABLE);
    /*Check ADC Ready*/
    while (ADC_GetFlagStatusNew(MetooADC->ADC, ADC_FLAG_RDY) == RESET)
        ;
    /* Start ADC calibration */
    ADC_StartCalibration(MetooADC->ADC);
    /* Check the end of ADC calibration */
    while (ADC_GetCalibrationStatus(MetooADC->ADC))
        ;
}

uint16_t Metoo_ADC_get_RAW(MetooADC_Data_t *MetooADC)
{

    ADC_ConfigRegularChannel(MetooADC->ADC, MetooADC->ADC_ch, 1, ADC_SAMP_TIME_239CYCLES5);
    /* Start ADC Software Conversion */
    ADC_EnableSoftwareStartConv(MetooADC->ADC, ENABLE);
    while (ADC_GetFlagStatus(MetooADC->ADC, ADC_FLAG_ENDC) == 0)
    {
    }
    ADC_ClearFlag(MetooADC->ADC, ADC_FLAG_ENDC);
    ADC_ClearFlag(MetooADC->ADC, ADC_FLAG_STR);
    MetooADC->raw = ADC_GetDat(MetooADC->ADC);
    return MetooADC->raw;
}

float Metoo_ADC_get_V(MetooADC_Data_t *MetooADC)
{
    MetooADC->value = Metoo_ADC_get_RAW(MetooADC) * 0.000805664;
    return MetooADC->value;
}

float Metoo_ADC_get_T(MetooADC_Data_t *MetooADC)
{
    MetooADC->raw = Metoo_ADC_get_RAW(MetooADC);
    MetooADC->value = 0;
    for (int i = 0; i < NTC_3450_SIZE; i++)
    {
        if (MetooADC->raw < NTC_3450[i] && MetooADC->raw >= NTC_3450[i + 1])
        {
            MetooADC->value = (i + NTC_3450_START) * 1.0;
            return MetooADC->value;
        }
    }

    return MetooADC->value;
}