#include "Metoo_SPI.h"

#ifdef HARD_SPI
void Metoo_SPI_init(void) // 硬件SPI
{
    GPIO_InitType GPIO_InitStructure;
    SPI_InitType SPI_InitStructure;
    // DMA_InitType DMA_InitStructure;

    SPI_I2S_DeInit(SPI_MASTER);
    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOB | RCC_APB2_PERIPH_AFIO, ENABLE); //使能GPIO时钟
    RCC_EnableAPB2PeriphClk(SPI_MASTER_CLK, ENABLE);                               //使能SPI时钟
    GPIO_ConfigPinRemap(GPIO_RMP1_SPI1, ENABLE);

    // GPIO_InitStruct(&GPIO_InitStructure);
    // GPIO_InitStructure.Pin = SPI_MASTER_PIN_SCK | SPI_MASTER_PIN_MOSI;
    // GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    // GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;			   //复用推挽输出
    // GPIO_InitPeripheral(SPI_MASTER_GPIO, &GPIO_InitStructure); //初始化GPIO

    GPIO_InitStructure.Pin = SPI_MASTER_PIN_SCK | SPI_MASTER_PIN_MOSI;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitPeripheral(SPI_MASTER_SCK_GPIO, &GPIO_InitStructure);

    //     /*!< Configure sFLASH_SPI pins: MISO */
    //     GPIO_InitStructure.Pin       = SPI_MASTER_PIN_MISO;
    // //  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //     GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    //     GPIO_InitPeripheral(SPI_MASTER_MISO_GPIO, &GPIO_InitStructure);

    // /* SPI_MASTER_Tx_DMA_Channel configuration ---------------------------------------------*/
    // DMA_DeInit(SPI_MASTER_Tx_DMA_Channel);
    // DMA_InitStructure.PeriphAddr = (uint32_t)SPI_MASTER_DR_Base;
    // DMA_InitStructure.MemAddr    = (uint32_t)SPI_MASTER_Buffer_Tx;
    // DMA_InitStructure.Direction  = DMA_DIR_PERIPH_DST;
    // DMA_Init(SPI_MASTER_Tx_DMA_Channel, &DMA_InitStructure);

    SPI_InitStructure.DataDirection = SPI_DIR_SINGLELINE_TX; // 只发送模式
    SPI_InitStructure.SpiMode = SPI_MODE_MASTER;             // 设置SPI工作模式：主机模式
    SPI_InitStructure.DataLen = SPI_DATA_SIZE_8BITS;         // 设置SPI数据大小：8位帧结构
    SPI_InitStructure.CLKPOL = SPI_CLKPOL_HIGH;              // 串行同步时钟空闲时SCLK位高电平
    SPI_InitStructure.CLKPHA = SPI_CLKPHA_SECOND_EDGE;       // 串行同步时钟空第二个时钟沿捕获
    SPI_InitStructure.NSS = SPI_NSS_SOFT;                    // NSS信号由软件管理
    SPI_InitStructure.BaudRatePres = SPI_BR_PRESCALER_2;     // 波特率预分频值：波特率预分频值为2
    SPI_InitStructure.FirstBit = SPI_FB_MSB;                 // 数据传输高位先行
    SPI_InitStructure.CRCPoly = 7;                           // CRC值计算的多项式
    SPI_Init(SPI_MASTER, &SPI_InitStructure);                // 初始化SPI
    SPI_Enable(SPI_MASTER, ENABLE);                          // 使能SPI
    GPIO_SetBits(SPI_MASTER_SCK_GPIO, SPI_MASTER_PIN_SCK);
    GPIO_SetBits(SPI_MASTER_MOSI_GPIO, SPI_MASTER_PIN_MOSI);
}

/**
 * @brief  set spi speed
 * @param  SPI_BaudRatePrescaler
 * @return None
 */

void Metoo_SPI_SetSpeed(uint8_t SpeedSet)
{
    // assert_param(IS_SPI_BAUDRATE_PRESCALER(SPI_BaudRatePrescaler));
    SPI_MASTER->CTRL1 &= 0XFFC7;
    SPI_MASTER->CTRL1 |= SpeedSet;
    SPI_Enable(SPI_MASTER, ENABLE);
}

/**
 * @brief  Sends a byte through the SPI interface and return the byte received
 *         from the SPI bus.
 * @param byte byte to send.
 * @return The value of the received byte.
 */
uint8_t Metoo_SPI_ReadWriteByte(uint8_t TxData)
{
    uint8_t retry = 0;
    /*!< Loop while DAT register in not emplty */
    while (SPI_I2S_GetStatus(SPI_MASTER, SPI_I2S_TE_FLAG) == RESET)
    {
        retry++;
        if (retry > 200)
            return 0;
    }
    /*!< Send byte through the SPI1 peripheral */
    SPI_I2S_TransmitData(SPI_MASTER, TxData);
    retry = 0;
    return 1;
    ///////////下面内容需要使用MISO
    // /*!< Wait to receive a byte */
    // while (SPI_I2S_GetStatus(SPI_MASTER, SPI_I2S_RNE_FLAG) == RESET)
    // {
    //     retry++;
    //     if(retry>200)
    //         return 0;
    // }
    // /*!< Return the byte read from the SPI bus */
    // return SPI_I2S_ReceiveData(SPI_MASTER);
}
/*******************************************************************************
 * Function Name	:	Metoo_DMA_Config
 * Description		:	SPI DMA
 * Input			:   DMA_CHx:DMA通道CHx,cpar:SPI外设地址,cmar:发送数据地址,cndtr:数据传输量
 * Output			:	None
 * Return			:	None
 * Attention		:	None
 *
 *******************************************************************************/
DMA_InitType DMA_InitStructure;
u16 DMA1_MEM_LEN; //保存DMA每次数据传送的长度          单次最大65535

void Metoo_DMA_Config(DMA_ChannelType *DMA_CHx, u32 cpar, u32 cmar, u16 cndtr)

{

    RCC_EnableAHBPeriphClk(SPI_MASTER_DMA_CLK, ENABLE);               //使能DMA传输时钟
    DMA_DeInit(DMA_CHx);                                              //将DMA的通道1寄存器重设为缺省值
    DMA1_MEM_LEN = cndtr;                                             // 数据传输量
    DMA_InitStructure.PeriphAddr = cpar;                              // DMA外设ADC基地址
    DMA_InitStructure.MemAddr = cmar;                                 // DMA内存基地址
    DMA_InitStructure.Direction = DMA_DIR_PERIPH_DST;                 // 数据传输方向，从内存读取发送到外设
    DMA_InitStructure.BufSize = cndtr;                                // DMA通道的DMA缓存的大小
    DMA_InitStructure.PeriphInc = DMA_PERIPH_INC_DISABLE;             // 外设地址寄存器不变
    DMA_InitStructure.DMA_MemoryInc = DMA_MEM_INC_ENABLE;             // 内存地址寄存器递增
    DMA_InitStructure.PeriphDataSize = DMA_PERIPH_DATA_SIZE_HALFWORD; // 数据宽度为8位
    DMA_InitStructure.MemDataSize = DMA_MemoryDataSize_HalfWord;      // 数据宽度为8位
    DMA_InitStructure.CircularMode = DMA_MODE_NORMAL;                 // 工作在正常缓存模式
    DMA_InitStructure.Priority = DMA_PRIORITY_MEDIUM;                 // DMA通道 x拥有中优先级
    DMA_InitStructure.Mem2Mem = DMA_M2M_DISABLE;                      // DMA通道x没有设置为内存到内存传输
    DMA_Init(DMA_CHx, &DMA_InitStructure);                            //根据DMA_InitStruct中指定的参数初始化DMA的通道USART1_Tx_DMA_Channel所标识的寄存器
}

/*******************************************************************************
 * Function Name	:	Metoo_DMA_Enable
 * Description		:	SPI DMA
 * Input			:   DMA_CHx:DMA通道CHx
 * Output			:	None
 * Return			:	None
 * Attention		:	None
 *
 *******************************************************************************/

void Metoo_DMA_Enable(DMA_ChannelType *DMA_CHx)

{

    DMA_EnableChannel(DMA_CHx, DISABLE);
    /*从新设置发送数量*/
    DMA_SetCurrDataCounter(DMA_CHx, DMA1_MEM_LEN);
    /* Enable DMA ChannelX */
    DMA_RequestRemap(DMA1_REMAP_SPI1_TX, DMA1, DMA_CHx, ENABLE);
    DMA_EnableChannel(DMA_CHx, ENABLE);
}

#endif

#ifdef SOFT_SPI
void Metoo_SPI_init(void) // 软件SPI
{
    GPIO_InitType GPIO_InitStructure;
    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOB | RCC_APB2_PERIPH_AFIO, ENABLE); //使能B端口时钟
    GPIO_InitStructure.Pin = SPI_MASTER_PIN_SCK | SPI_MASTER_PIN_MOSI;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //速度50MHz
    GPIO_InitPeripheral(GPIOB, &GPIO_InitStructure);  //初始化GPIOA
    GPIO_SetBits(SPI_MASTER_SCK_GPIO, SPI_MASTER_PIN_SCK);
    // GPIO_SetBits(SPI_MASTER_MOSI_GPIO, SPI_MASTER_PIN_MOSI);
}

uint8_t Metoo_SPI_ReadWriteByte(uint8_t TxData)
{
    u8 i;
    for (i = 0; i < 8; i++)
    {
        // LCD_SCLK_Clr();
        GPIO_ResetBits(SPI_MASTER_SCK_GPIO, SPI_MASTER_PIN_SCK);
        // systick_delay_us(1);
        if (TxData & 0x80)
        {
            GPIO_SetBits(SPI_MASTER_MOSI_GPIO, SPI_MASTER_PIN_MOSI);
            // systick_delay_us(1);
        }
        else
        {
            GPIO_ResetBits(SPI_MASTER_MOSI_GPIO, SPI_MASTER_PIN_MOSI);
            // systick_delay_us(1);
        }
        GPIO_SetBits(SPI_MASTER_SCK_GPIO, SPI_MASTER_PIN_SCK);
        // systick_delay_us(1);
        TxData <<= 1;
    }
    return 1;
}
#endif