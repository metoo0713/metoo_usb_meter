#include "Metoo_ST7789.h"
#include "Metoo_SPI.h"
#include "Metoo_Tick.h"


/**
 * @brief	LCD控制接口初始化
 *
 * @param   void
 *
 * @return  void
 */
void LCD_GPIO_Init(void)
{
	GPIO_InitType GPIO_InitStructure;
	RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOB | RCC_APB2_PERIPH_AFIO, ENABLE); //使能B端口时钟
	GPIO_InitStructure.Pin = LCD_DC_Pin | LCD_RST_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //速度50MHz
	GPIO_InitPeripheral(GPIOB, &GPIO_InitStructure);  //初始化GPIOA
													  // GPIO_SetBits(GPIOB, LCD_DC_Pin | LCD_RST_Pin);
}

/**
 * @brief	LCD底层SPI发送数据函数
 *
 * @param   data	数据的起始地址
 * @param   size	发送数据大小
 *
 * @return  void
 */
static void LCD_SPI_Send(uint8_t *data, uint16_t size)
{
	// SPI1_WriteData(data, size);
	while (size--)
	{
		Metoo_SPI_ReadWriteByte(*data);
		data++;
	}
}

/**
 * @brief	写命令到LCD
 *
 * @param   cmd		需要发送的命令
 *
 * @return  void
 */
static void LCD_Write_Cmd(uint8_t cmd)
{
#ifdef HARD_SPI
	while (SPI_I2S_GetStatus(SPI_MASTER, SPI_I2S_BUSY_FLAG) == SET)
	{
	}
#endif
	LCD_DC(0);
	LCD_SPI_Send(&cmd, 1);
}

/**
 * @brief	写数据到LCD
 *
 * @param   cmd		需要发送的数据
 *
 * @return  void
 */
static void LCD_Write_Data(uint8_t data)
{
#ifdef HARD_SPI
	while (SPI_I2S_GetStatus(SPI_MASTER, SPI_I2S_BUSY_FLAG) == SET)
	{
	}
#endif
	LCD_DC(1);
	LCD_SPI_Send(&data, 1);
}

/**
 * @brief	写半个字的数据到LCD
 *
 * @param   cmd		需要发送的数据
 *
 * @return  void
 */
void LCD_Write_HalfWord(const uint16_t da)
{
	uint8_t data[2] = {0};

	data[0] = da >> 8;
	data[1] = da;

	LCD_DC(1);
	LCD_SPI_Send(data, 2);
}

/**
 * 设置数据写入LCD缓存区域
 *
 * @param   x1,y1	起点坐标
 * @param   x2,y2	终点坐标
 *
 * @return  void
 */
void LCD_Address_Set(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	if (USE_HORIZONTAL == 0)
	{
		LCD_Write_Cmd(0x2a); //列地址设置
		LCD_Write_HalfWord(x1 + 52);
		LCD_Write_HalfWord(x2 + 52);
		LCD_Write_Cmd(0x2b); //行地址设置
		LCD_Write_HalfWord(y1 + 40);
		LCD_Write_HalfWord(y2 + 40);
		LCD_Write_Cmd(0x2c); //储存器写
	}
	else if (USE_HORIZONTAL == 1)
	{
		LCD_Write_Cmd(0x2a); //列地址设置
		LCD_Write_HalfWord(x1 + 53);
		LCD_Write_HalfWord(x2 + 53);
		LCD_Write_Cmd(0x2b); //行地址设置
		LCD_Write_HalfWord(y1 + 40);
		LCD_Write_HalfWord(y2 + 40);
		LCD_Write_Cmd(0x2c); //储存器写
	}
	else if (USE_HORIZONTAL == 2)
	{
		LCD_Write_Cmd(0x2a); //列地址设置
		LCD_Write_HalfWord(x1 + 40);
		LCD_Write_HalfWord(x2 + 40);
		LCD_Write_Cmd(0x2b); //行地址设置
		LCD_Write_HalfWord(y1 + 53);
		LCD_Write_HalfWord(y2 + 53);
		LCD_Write_Cmd(0x2c); //储存器写
	}
	else
	{
		LCD_Write_Cmd(0x2a); //列地址设置
		LCD_Write_HalfWord(x1 + 40);
		LCD_Write_HalfWord(x2 + 40);
		LCD_Write_Cmd(0x2b); //行地址设置
		LCD_Write_HalfWord(y1 + 52);
		LCD_Write_HalfWord(y2 + 52);
		LCD_Write_Cmd(0x2c); //储存器写
	}
}


/**
 * @brief	LCD初始化
 *
 * @param   void
 *
 * @return  void
 */
void LCD_Init(void)
{
	LCD_GPIO_Init();
	// LCD_Write_Data(0x01);
	// LCD_PWR(0);
	Metoo_Delay_ms(120);
	LCD_RST(0);
	Metoo_Delay_ms(120);
	LCD_RST(1);
	// LCD_Write_Cmd(0x11);
	Metoo_Delay_ms(120);
	/* Sleep Out */
	LCD_Write_Cmd(0x11);
	/* wait for power stability */
	Metoo_Delay_ms(120);
	/* Memory Data Access Control */
	LCD_Write_Cmd(0x36);
	if (USE_HORIZONTAL == 0)
		LCD_Write_Data(0x00);
	else if (USE_HORIZONTAL == 1)
		LCD_Write_Data(0xC0);
	else if (USE_HORIZONTAL == 2)
		LCD_Write_Data(0x70);
	else
		LCD_Write_Data(0xA0);

	/* RGB 5-6-5-bit  */
	LCD_Write_Cmd(0x3A);
	LCD_Write_Data(0x05);

	/* Porch Setting */
	LCD_Write_Cmd(0xB2);
	LCD_Write_Data(0x0C);
	LCD_Write_Data(0x0C);
	LCD_Write_Data(0x00);
	LCD_Write_Data(0x33);
	LCD_Write_Data(0x33);

	/*  Gate Control */
	LCD_Write_Cmd(0xB7);
	LCD_Write_Data(0x35);

	/* VCOM Setting */
	LCD_Write_Cmd(0xBB);
	LCD_Write_Data(0x19); // Vcom=1.625V

	/* LCM Control */
	LCD_Write_Cmd(0xC0);
	LCD_Write_Data(0x2C);

	/* VDV and VRH Command Enable */
	LCD_Write_Cmd(0xC2);
	LCD_Write_Data(0x01);

	/* VRH Set */
	LCD_Write_Cmd(0xC3);
	LCD_Write_Data(0x12);

	/* VDV Set */
	LCD_Write_Cmd(0xC4);
	LCD_Write_Data(0x20);

	/* Frame Rate Control in Normal Mode */
	LCD_Write_Cmd(0xC6);
	LCD_Write_Data(0x0F); // 60MHZ

	/* Power Control 1 */
	LCD_Write_Cmd(0xD0);
	LCD_Write_Data(0xA4);
	LCD_Write_Data(0xA1);

	/* Positive Voltage Gamma Control */
	LCD_Write_Cmd(0xE0);
	LCD_Write_Data(0xD0);
	LCD_Write_Data(0x04);
	LCD_Write_Data(0x0D);
	LCD_Write_Data(0x11);
	LCD_Write_Data(0x13);
	LCD_Write_Data(0x2B);
	LCD_Write_Data(0x3F);
	LCD_Write_Data(0x54);
	LCD_Write_Data(0x4C);
	LCD_Write_Data(0x18);
	LCD_Write_Data(0x0D);
	LCD_Write_Data(0x0B);
	LCD_Write_Data(0x1F);
	LCD_Write_Data(0x23);

	/* Negative Voltage Gamma Control */
	LCD_Write_Cmd(0xE1);
	LCD_Write_Data(0xD0);
	LCD_Write_Data(0x04);
	LCD_Write_Data(0x0C);
	LCD_Write_Data(0x11);
	LCD_Write_Data(0x13);
	LCD_Write_Data(0x2C);
	LCD_Write_Data(0x3F);
	LCD_Write_Data(0x44);
	LCD_Write_Data(0x51);
	LCD_Write_Data(0x2F);
	LCD_Write_Data(0x1F);
	LCD_Write_Data(0x1F);
	LCD_Write_Data(0x20);
	LCD_Write_Data(0x23);

	/* Display Inversion On */
	LCD_Write_Cmd(0x21);

	LCD_Write_Cmd(0x29);

	LCD_Address_Set(0, 0, LCD_Width - 1, LCD_Height - 1);

	/*打开显示*/
	// LCD_PWR(1);
}

void put_px(uint16_t x, uint16_t y, void *color)
{

	// GPIO_ResetBits(GPIOA, GPIO_PIN_1);
	LCD_Address_Set(x, y, x, y);
	LCD_Write_HalfWord(color);

	// GPIO_SetBits(GPIOA, GPIO_PIN_1);
	// /*!< Loop while DAT register in not emplty */
	// while (SPI_I2S_GetStatus(SPI_MASTER, SPI_I2S_TE_FLAG) == RESET)
	// {
	// }
	// /*!< Send byte through the SPI1 peripheral */
	// SPI_I2S_TransmitData(SPI_MASTER, color);
}

void Metoo_flush(int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint32_t data, uint32_t size) // 未测试，在lv_port_disp
{
	uint16_t *p = data;

	// GPIO_ResetBits(GPIOA, GPIO_PIN_1);
	LCD_Address_Set(x1, y1, x2, y2);

	LCD_DC(1);
	SPI_Enable(SPI_MASTER, DISABLE); //使能SPI
	SPI_ConfigDataLen(SPI_MASTER, SPI_DATA_SIZE_16BITS);
	SPI_Enable(SPI_MASTER, ENABLE); //使能SPI
	while (size--)
	{
		while (SPI_I2S_GetStatus(SPI_MASTER, SPI_I2S_TE_FLAG) == RESET)
		{
		}
		/*!< Send byte through the SPI1 peripheral */
		SPI_I2S_TransmitData(SPI_MASTER, &p);

		p++;
	}
	SPI_Enable(SPI_MASTER, DISABLE); //使能SPI
	SPI_ConfigDataLen(SPI_MASTER, SPI_DATA_SIZE_8BITS);
	SPI_Enable(SPI_MASTER, ENABLE); //使能SPI

	// GPIO_SetBits(GPIOA, GPIO_PIN_1);
}

void Metoo_DMA_flush(int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint32_t data, uint32_t size)
{
	uint16_t t = 1;
	uint32_t num, num1;
	// color1[0] = data;
	num = size;

	// GPIO_ResetBits(GPIOA, GPIO_PIN_1);

	LCD_Address_Set(x1, y1, x2, y2);

	LCD_DC(1);
	SPI_Enable(SPI_MASTER, DISABLE); //使能SPI
	SPI_ConfigDataLen(SPI_MASTER, SPI_DATA_SIZE_16BITS);
	SPI_Enable(SPI_MASTER, ENABLE); //使能SPI

	while (t)
	{
		if (num > 65534)
		{
			num -= 65534;
			num1 = 65534;
		}
		else
		{
			t = 0;
			num1 = num;
		}
		Metoo_DMA_Config(SPI_MASTER_Tx_DMA_Channel, (u32)&SPI_MASTER->DAT, data, num1);
		// MYDMA_Config1(DMA_CH3,(u32)SPI_MASTER_DR_Base,(u32)color1,num1);
		SPI_I2S_EnableDma(SPI_MASTER, SPI_I2S_DMA_TX, ENABLE);
		// SPI_Enable(SPI1, ENABLE);
		Metoo_DMA_Enable(SPI_MASTER_Tx_DMA_Channel);
		while (1)
		{
			if (DMA_GetFlagStatus(SPI_MASTER_Tx_DMA_FLAG, SPI_MASTER_DMA) != RESET) //等待传输完成
			{
				DMA_ClearFlag(SPI_MASTER_Tx_DMA_FLAG, SPI_MASTER_DMA); //清除传输完成标志
				break;
			}
		}
	}
	SPI_Enable(SPI_MASTER, DISABLE); //使能SPI
	SPI_ConfigDataLen(SPI_MASTER, SPI_DATA_SIZE_8BITS);
	SPI_Enable(SPI_MASTER, ENABLE); //使能SPI

	// GPIO_SetBits(GPIOA, GPIO_PIN_1);
}
