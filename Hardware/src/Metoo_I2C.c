#include "Metoo_I2C.h"
#include "log.h"
// void Metoo_I2C_init(void)
// {

// }

//////////////////////////////////////////////////////////////////////////////////////

// void I2C_Initial(void)
// {
//     I2Cx_GPIO_Config();
//     I2C_Config(I2Cx);
//     I2C_NVIC_Config(I2Cx); // #define I2Cx_UseIT
// }

void I2Cx_GPIO_Config(void)
{
    GPIO_InitType GPIO_InitStructure;
    /* Enable the GPIO Clock */
    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO | RCC_APB2_PERIPH_GPIOB, ENABLE);
    GPIO_InitStruct(&GPIO_InitStructure);
    /* SCL--PB6/PB8  SDA--PB7/PB9 */
    GPIO_InitStructure.Pin = GPIO_PIN_6 | GPIO_PIN_7; // 使用默认分组0管脚
    // GPIO_InitStructure.Pin = GPIO_PIN_8 | GPIO_PIN_9;  // 使用分组1 （I2Cx_RMP[1:0] = 01）
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
    GPIO_InitPeripheral(GPIOB, &GPIO_InitStructure);
    // GPIO_ConfigPinRemap(GPIO_RMP_I2Cx, ENABLE);    // 打开重映射功能，使用分组1（PB8 PB9）
}

void I2C_Config(I2C_Module *I2Cx)
{
    I2C_InitType I2C_InitStructure;
    if (I2Cx == I2C1)
    {
        RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_I2C1, ENABLE);
        I2C_InitStructure.OwnAddr1 = I2C1_OwnAddr;
    }
    else if (I2Cx == I2C2)
    {
        RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_I2C2, ENABLE);
        I2C_InitStructure.OwnAddr1 = I2C2_OwnAddr;
    }
    else if (I2Cx == I2C3)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_I2C3, ENABLE);
        I2C_InitStructure.OwnAddr1 = I2C3_OwnAddr;
    }
    else if (I2Cx == I2C4)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_I2C4, ENABLE);
        I2C_InitStructure.OwnAddr1 = I2C4_OwnAddr;
    }
    I2C_DeInit(I2Cx);
    I2C_InitStructure.BusMode = I2C_BUSMODE_I2C;
    I2C_InitStructure.FmDutyCycle = I2C_FMDUTYCYCLE_2;
    I2C_InitStructure.AckEnable = I2C_ACKEN;
    I2C_InitStructure.AddrMode = I2C_ADDR_MODE_7BIT;
    I2C_InitStructure.ClkSpeed = 100000; // 100K
    I2C_Init(I2Cx, &I2C_InitStructure);  // Initial and Enable I2Cx
}

void I2C_NVIC_Config(I2C_Module *I2Cx)
{
    NVIC_InitType NVIC_InitStructure;

    if (I2Cx == I2C1)
    {
        NVIC_InitStructure.NVIC_IRQChannel = I2C1_EV_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelCmd = I2C1_UseIT;
    }
    else if (I2Cx == I2C2)
    {
        NVIC_InitStructure.NVIC_IRQChannel = I2C2_EV_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelCmd = I2C2_UseIT;
    }
    else if (I2Cx == I2C3)
    {
        NVIC_InitStructure.NVIC_IRQChannel = I2C3_EV_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelCmd = I2C3_UseIT;
    }
    else if (I2Cx == I2C4)
    {
        NVIC_InitStructure.NVIC_IRQChannel = I2C4_EV_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelCmd = I2C4_UseIT;
    }
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
    NVIC_Init(&NVIC_InitStructure);
    NVIC_InitStructure.NVIC_IRQChannel++; // 配置I2Cx_ER_IRQn
    NVIC_Init(&NVIC_InitStructure);
    I2C_ConfigInt(I2Cx, I2C_INT_EVENT | I2C_INT_BUF | I2C_INT_ERR, NVIC_InitStructure.NVIC_IRQChannelCmd); //打开I2C三个中断使能
}

void I2C_ResetInit(I2C_Module *I2Cx)
{
    I2Cx->CTRL1 |= 0x8000; // Reset Busy
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    I2Cx->CTRL1 &= ~0x8000;
    I2C_Config(I2Cx);
    log_info("***** IIC module self reset! *****\r\n");
    // I2C_NVIC_Config(I2Cx); // #define I2Cx_UseIT
}

/*
参数说明：
uint8_t  slaveAddr：Bit[7:1]为从机地址，不关心最低位。
short regAddr：发送从器件寄存器地址(8bit)，如果不需要发送，填-1 。【寄存器写】uint8_t* datPtr：发送数据Buf的地址。
返回值：MASTER_OK表示通信正常，其他表示错误。
*/

int Metoo_I2C_Write(I2C_Module *I2Cx, uint8_t slaveAddr, uint8_t regAddr, uint8_t *datPtr, uint8_t len)
{
    // log_info("I2C_Write\n\r");
    uint32_t I2CTimeout;
    if (regAddr < 0 && len == 0)
        return 0; /* 至少发送一个数据 */

    /* 1.保证I2C不在使用中 */
    I2CTimeout = I2CT_FLAG_TIMEOUT;
    while (I2C_GetFlag(I2Cx, I2C_FLAG_BUSY))
    {
        if ((I2CTimeout--) == 0)
        {
            I2C_ResetInit(I2Cx);
            return MASTER_BUSY;
        }
    }
    /* 2.发送START信号 */
    I2C_GenerateStart(I2Cx, ENABLE);
    I2CTimeout = I2CT_FLAG_TIMEOUT;
    while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_MODE_FLAG))
    { // EV5
        if ((I2CTimeout--) == 0)
        {
            I2C_ResetInit(I2Cx);
            return MASTER_MODE;
        }
    }
    /* 3.写从机地址 */
    I2C_SendAddr7bit(I2Cx, slaveAddr, I2C_DIRECTION_SEND);
    I2CTimeout = I2CT_FLAG_TIMEOUT;
    while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_TXMODE_FLAG))
    { // EV6
        if ((I2CTimeout--) == 0)
        {
            I2C_ResetInit(I2Cx);
            return MASTER_TXMODE;
        }
    }
    /* 4.写寄存器地址 */
    if (regAddr >= 0)
    { /* 寄存器写 */
        I2C_SendData(I2Cx, (uint8_t)regAddr);
        I2CTimeout = I2CT_FLAG_TIMEOUT;
        while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_DATA_SENDING))
        { // EV8
            if ((I2CTimeout--) == 0)
            {
                I2C_ResetInit(I2Cx);
                return MASTER_SENDING;
            }
        }
    }
    /* 4.发送数据 */
    while (len--)
    {
        I2C_SendData(I2Cx, *datPtr++);
        I2CTimeout = I2CT_FLAG_TIMEOUT;
        while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_DATA_SENDING))
        { // EV8
            if ((I2CTimeout--) == 0)
            {
                I2C_ResetInit(I2Cx);
                return MASTER_SENDING;
            }
        }
    }
    /* 5.等待最后一个字节发送完成 */
    I2CTimeout = I2CT_FLAG_TIMEOUT;
    while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_DATA_SENDED))
    { // EV8_2
        if ((I2CTimeout--) == 0)
        {
            I2C_ResetInit(I2Cx);
            return MASTER_SENDED;
        }
    }
    /* 6.发送STOP信号 */
    I2C_GenerateStop(I2Cx, ENABLE);
    I2CTimeout = I2CT_FLAG_TIMEOUT;
    while (I2C_GetFlag(I2Cx, I2C_FLAG_BUSY))
    {
        if ((I2CTimeout--) == 0)
        {
            I2C_ResetInit(I2Cx);
            return MASTER_BUSY;
        }
    }
    /* 7.发送完成 */
    return MASTER_OK;
}

// ErrCode_t I2C_Read(I2C_Module *I2Cx, uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint8_t count)
// {
//     uint32_t I2CTimeout;
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (I2C_GetFlag(I2Cx, I2C_FLAG_BUSY))
//     {
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_BUSY;
//         }
//     }
//     /* Enable Acknowledge */
//     // I2C_ConfigAck(I2Cx, ENABLE);
//     /** Send START condition */
//     I2C_GenerateStart(I2Cx, ENABLE);
//     /** Test on EV5 and clear it */
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_MODE_FLAG))
//     {
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_MODE;
//         }
//     }
//     /** Send EEPROM address for write */
//     I2C_SendAddr7bit(I2Cx, dev_addr, I2C_DIRECTION_SEND);
//     /** Test on EV6 and clear it */
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_TXMODE_FLAG))
//     {
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_TXMODE;
//         }
//     }
//     /** Clear EV6 by setting again the PE bit */
//     I2C_Enable(I2Cx, ENABLE);
//     /** Send the EEPROM's internal address to write to */
//     I2C_SendData(I2Cx, reg_addr);
//     /** Test on EV8 and clear it */
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_DATA_SENDED))
//     {
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_SENDING;
//         }
//     }
//     /** Send STRAT condition a second time */
//     I2C_GenerateStart(I2Cx, ENABLE);
//     /** Test on EV5 and clear it */
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_MODE_FLAG))
//     {
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_MODE;
//         }
//     }
//     /** Send EEPROM address for read */
//     I2C_SendAddr7bit(I2Cx, dev_addr, I2C_DIRECTION_RECV);
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (!I2C_GetFlag(I2Cx, I2C_FLAG_ADDRF))
//     {
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_TXMODE;
//         }
//     }
//     /** While there is data to be read */
//     if (count == 1)
//     {
//         /** Disable Acknowledgement */
//         I2C_ConfigAck(I2Cx, DISABLE);
//         (void)(I2Cx->STS1); /// clear ADDR
//         (void)(I2Cx->STS2);
//         I2C_GenerateStop(I2Cx, ENABLE);
//     }
//     else if (count == 2)
//     {
//         I2Cx->CTRL1 |= 0x0800; /// set POSEN
//         (void)(I2Cx->STS1);
//         (void)(I2Cx->STS2);
//         I2C_ConfigAck(I2Cx, DISABLE);
//     }
//     else
//     {
//         I2C_ConfigAck(I2Cx, ENABLE);
//         (void)(I2Cx->STS1);
//         (void)(I2Cx->STS2);
//     }
//     while (count)
//     {
//         if (count <= 3)
//         {
//             /** One byte */
//             if (count == 1)
//             {
//                 /** Wait until RXNE flag is set */
//                 I2CTimeout = I2CT_FLAG_TIMEOUT;
//                 while (!I2C_GetFlag(I2Cx, I2C_FLAG_RXDATNE))
//                 {
//                     if ((I2CTimeout--) == 0)
//                     {
//                         I2C_ResetInit(I2Cx);
//                         return MASTER_RECVD;
//                     }
//                 }
//                 /** Read data from DAT */
//                 /** Read a byte from the EEPROM */
//                 *data = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 data++;
//                 /** Decrement the read bytes counter */
//                 count--;
//             }
//             /** Two bytes */
//             else if (count == 2)
//             {
//                 /** Wait until BTF flag is set */
//                 I2CTimeout = I2CT_FLAG_TIMEOUT;
//                 while (!I2C_GetFlag(I2Cx, I2C_FLAG_BYTEF))
//                 {
//                     if ((I2CTimeout--) == 0)
//                     {
//                         I2C_ResetInit(I2Cx);
//                         return MASTER_RECVD;
//                     }
//                 }
//                 /** Send STOP Condition */
//                 I2C_GenerateStop(I2Cx, ENABLE);

//                 /** Read data from DAT */
//                 *data = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 data++;
//                 /** Decrement the read bytes counter */
//                 count--;
//                 /** Read data from DAT */
//                 *data = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 data++;
//                 /** Decrement the read bytes counter */
//                 count--;
//             }
//             /** 3 Last bytes */
//             else
//             {
//                 I2CTimeout = I2CT_FLAG_TIMEOUT;
//                 while (!I2C_GetFlag(I2Cx, I2C_FLAG_BYTEF))
//                 {
//                     if ((I2CTimeout--) == 0)
//                     {
//                         I2C_ResetInit(I2Cx);
//                         return MASTER_RECVD;
//                     }
//                 }
//                 /** Disable Acknowledgement */
//                 I2C_ConfigAck(I2Cx, DISABLE);
//                 /** Read data from DAT */
//                 *data = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 data++;
//                 /** Decrement the read bytes counter */
//                 count--;

//                 /** Wait until BTF flag is set */
//                 I2CTimeout = I2CT_FLAG_TIMEOUT;
//                 while (!I2C_GetFlag(I2Cx, I2C_FLAG_BYTEF))
//                 {
//                     if ((I2CTimeout--) == 0)
//                     {
//                         I2C_ResetInit(I2Cx);
//                         return MASTER_RECVD;
//                     }
//                 }
//                 /** Send STOP Condition */
//                 I2C_GenerateStop(I2Cx, ENABLE);

//                 /** Read data from DAT */
//                 *data = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 data++;
//                 /** Decrement the read bytes counter */
//                 count--;

//                 /** Read data from DAT */
//                 *data = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 data++;
//                 /** Decrement the read bytes counter */
//                 count--;
//             }
//         }
//         else
//         {
//             /** Test on EV7 and clear it */
//             I2CTimeout = I2CT_FLAG_TIMEOUT;
//             while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_DATA_RECVD_FLAG))
//             {
//                 if ((I2CTimeout--) == 0)
//                 {
//                     I2C_ResetInit(I2Cx);
//                     return MASTER_RECVD;
//                 }
//             }
//             /** Read a byte from the EEPROM */
//             *data = I2C_RecvData(I2Cx);
//             /** Point to the next location where the byte read will be saved */
//             data++;
//             /** Decrement the read bytes counter */
//             count--;
//             if (I2C_GetFlag(I2Cx, I2C_FLAG_BYTEF))
//             {
//                 /** Read a byte from the EEPROM */
//                 *data = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 data++;
//                 /** Decrement the read bytes counter */
//                 count--;
//             }
//         }
//     }
//     return MASTER_OK;
// }

// int I2C_Read(I2C_Module *I2Cx, uint8_t slaveAddr, uint8_t regAddr, uint8_t *datPtr, uint8_t len)
// {
//     log_info("I2C_Read\n\r");
//     uint32_t I2CTimeout;

//     /* 1.保证I2C不在使用中 */
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (I2C_GetFlag(I2Cx, I2C_FLAG_BUSY))
//     {
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_BUSY;
//         }
//     }
//     /* Enable Acknowledge */
//     // I2C_ConfigAck(I2Cx, ENABLE);

//     /* 2.发送START信号 */
//     I2C_GenerateStart(I2Cx, ENABLE);
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_MODE_FLAG))
//     { // EV5
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_MODE;
//         }
//     }
//     /* 3.写从机地址 */
//     I2C_SendAddr7bit(I2Cx, slaveAddr, I2C_DIRECTION_SEND);
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_TXMODE_FLAG))
//     { // EV6
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_TXMODE;
//         }
//     }
//     /** Clear EV6 by setting again the PE bit */
//     I2C_Enable(I2Cx, ENABLE);
//     /** 发送寄存器地址 */
//     I2C_SendData(I2Cx, regAddr);
//     /** Test on EV8 and clear it */
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_DATA_SENDED))
//     {
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_SENDING;
//         }
//     }
//     /* 2.发送START信号 */
//     I2C_GenerateStart(I2Cx, ENABLE);
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_MODE_FLAG))
//     { // EV5
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_MODE;
//         }
//     }
//     /* 3.写从机地址 */
//     I2C_SendAddr7bit(I2Cx, slaveAddr, I2C_DIRECTION_RECV);
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (!I2C_GetFlag(I2Cx, I2C_FLAG_ADDRF))
//     { // EV6
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_TXMODE;
//         }
//     }
//     /* 4.接收数据 */
//     if (len == 1)
//     {
//         /** Disable Acknowledgement */
//         I2C_ConfigAck(I2Cx, DISABLE);
//         (void)(I2Cx->STS1); /// clear ADDR
//         (void)(I2Cx->STS2);
//         I2C_GenerateStop(I2Cx, ENABLE);
//     }
//     else if (len == 2)
//     {
//         I2Cx->CTRL1 |= 0x0800; /// set POSEN
//         (void)(I2Cx->STS1);
//         (void)(I2Cx->STS2);
//         I2C_ConfigAck(I2Cx, DISABLE);
//     }
//     else
//     {
//         I2C_ConfigAck(I2Cx, ENABLE);
//         (void)(I2Cx->STS1);
//         (void)(I2Cx->STS2);
//     }
//     while (len)
//     {
//         if (len <= 3)
//         {
//             /** One byte */
//             if (len == 1)
//             {
//                 /** Wait until RXNE flag is set */
//                 I2CTimeout = I2CT_FLAG_TIMEOUT;
//                 while (!I2C_GetFlag(I2Cx, I2C_FLAG_RXDATNE))
//                 {
//                     if ((I2CTimeout--) == 0)
//                     {
//                         I2C_ResetInit(I2Cx);
//                         return MASTER_RECVD;
//                     }
//                 }
//                 /** Read data from DAT */
//                 /** Read a byte from the EEPROM */
//                 *datPtr = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 datPtr++;
//                 /** Decrement the read bytes lener */
//                 len--;
//             }
//             /** Two bytes */
//             else if (len == 2)
//             {
//                 /** Wait until BTF flag is set */
//                 I2CTimeout = I2CT_FLAG_TIMEOUT;
//                 while (!I2C_GetFlag(I2Cx, I2C_FLAG_BYTEF))
//                 {
//                     if ((I2CTimeout--) == 0)
//                     {
//                         I2C_ResetInit(I2Cx);
//                         return MASTER_RECVD;
//                     }
//                 }
//                 /** Send STOP Condition */
//                 I2C_GenerateStop(I2Cx, ENABLE);

//                 /** Read data from DAT */
//                 *datPtr = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 datPtr++;
//                 /** Decrement the read bytes lener */
//                 len--;
//                 /** Read data from DAT */
//                 *datPtr = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 datPtr++;
//                 /** Decrement the read bytes lener */
//                 len--;
//             }
//             /** 3 Last bytes */
//             else
//             {
//                 I2CTimeout = I2CT_FLAG_TIMEOUT;
//                 while (!I2C_GetFlag(I2Cx, I2C_FLAG_BYTEF))
//                 {
//                     if ((I2CTimeout--) == 0)
//                     {
//                         I2C_ResetInit(I2Cx);
//                         return MASTER_RECVD;
//                     }
//                 }
//                 /** Disable Acknowledgement */
//                 I2C_ConfigAck(I2Cx, DISABLE);
//                 /** Read data from DAT */
//                 *datPtr = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 datPtr++;
//                 /** Decrement the read bytes lener */
//                 len--;

//                 /** Wait until BTF flag is set */
//                 I2CTimeout = I2CT_FLAG_TIMEOUT;
//                 while (!I2C_GetFlag(I2Cx, I2C_FLAG_BYTEF))
//                 {
//                     if ((I2CTimeout--) == 0)
//                     {
//                         I2C_ResetInit(I2Cx);
//                         return MASTER_RECVD;
//                     }
//                 }
//                 /** Send STOP Condition */
//                 I2C_GenerateStop(I2Cx, ENABLE);

//                 /** Read data from DAT */
//                 *datPtr = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 datPtr++;
//                 /** Decrement the read bytes lener */
//                 len--;

//                 /** Read data from DAT */
//                 *datPtr = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 datPtr++;
//                 /** Decrement the read bytes lener */
//                 len--;
//             }
//         }
//         else
//         {
//             /** Test on EV7 and clear it */
//             I2CTimeout = I2CT_FLAG_TIMEOUT;
//             while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_DATA_RECVD_FLAG))
//             {
//                 if ((I2CTimeout--) == 0)
//                 {
//                     I2C_ResetInit(I2Cx);
//                     return MASTER_RECVD;
//                 }
//             }
//             /** Read a byte from the EEPROM */
//             *datPtr = I2C_RecvData(I2Cx);
//             /** Point to the next location where the byte read will be saved */
//             datPtr++;
//             /** Decrement the read bytes lener */
//             len--;
//             if (I2C_GetFlag(I2Cx, I2C_FLAG_BYTEF))
//             {
//                 /** Read a byte from the EEPROM */
//                 *datPtr = I2C_RecvData(I2Cx);
//                 /** Point to the next location where the byte read will be saved */
//                 datPtr++;
//                 /** Decrement the read bytes lener */
//                 len--;
//             }
//         }
//     }

//     /* 5.等待BUSY */
//     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     while (I2C_GetFlag(I2Cx, I2C_FLAG_BUSY))
//     {
//         if ((I2CTimeout--) == 0)
//         {
//             I2C_ResetInit(I2Cx);
//             return MASTER_BUSY;
//         }
//     }
//     /* 6.接收完成 */
//     return MASTER_OK;

//     // I2C_ConfigAck(I2Cx, ENABLE); /* 开启应答 */
//     // while (len--)
//     // {
//     //     if (len == 0)
//     //     { /* 接收最后一个数据前关闭应答，准备STOP */ // EV7_1
//     //         I2C_ConfigAck(I2Cx, DISABLE);
//     //         I2C_GenerateStop(I2Cx, ENABLE);
//     //     }
//     //     I2CTimeout = I2CT_FLAG_TIMEOUT;
//     //     while (!I2C_GetFlag(I2Cx, I2C_FLAG_RXDATNE))
//     //     { // EV7
//     //         if ((I2CTimeout--) == 0)
//     //         {
//     //             I2C_ResetInit(I2Cx);
//     //             return MASTER_BYTEF;
//     //         }
//     //     }
//     //     *datPtr++ = I2C_RecvData(I2Cx);
//     // }
//     // /* 5.等待BUSY */
//     // I2CTimeout = I2CT_FLAG_TIMEOUT;
//     // while (I2C_GetFlag(I2Cx, I2C_FLAG_BUSY))
//     // {
//     //     if ((I2CTimeout--) == 0)
//     //     {
//     //         I2C_ResetInit(I2Cx);
//     //         return MASTER_BUSY;
//     //     }
//     // }
//     // /* 6.接收完成 */
//     // return MASTER_OK;
// }

int Metoo_I2C_Read(I2C_Module* I2Cx, uint8_t slaveAddr, short regAddr, uint8_t* datPtr, uint16_t len)
{

    uint32_t I2CTimeout;

    int ret;

    if (len == 0)
        return 0; /* 至少读取一个数据 */

    if (regAddr >= 0)
    { /* 寄存器读 */

        ret = Metoo_I2C_Write(I2Cx, slaveAddr, regAddr, datPtr, 0);

        if (ret != MASTER_OK)
            return ret;
    }

    /* 1.保证I2C不在使用中 */

    I2CTimeout = I2CT_LONG_TIMEOUT;

    while (I2C_GetFlag(I2Cx, I2C_FLAG_BUSY))
    {

        if ((I2CTimeout--) == 0)
        {

            I2C_ResetInit(I2Cx);

            return MASTER_BUSY;
        }
    }

    /* 2.发送START信号 */

    I2C_GenerateStart(I2Cx, ENABLE);

    I2CTimeout = I2CT_LONG_TIMEOUT;

    while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_MODE_FLAG))
    { // EV5

        if ((I2CTimeout--) == 0)
        {

            I2C_ResetInit(I2Cx);

            return MASTER_MODE;
        }
    }

    /* 3.写从机地址 */

    I2C_SendAddr7bit(I2Cx, slaveAddr, I2C_DIRECTION_RECV);

    I2CTimeout = I2CT_LONG_TIMEOUT;

    while (!I2C_CheckEvent(I2Cx, I2C_EVT_MASTER_RXMODE_FLAG))
    { // EV6

        if ((I2CTimeout--) == 0)
        {

            I2C_ResetInit(I2Cx);

            return MASTER_TXMODE;
        }
    }

    /* 4.接收数据 */

    I2C_ConfigAck(I2Cx, ENABLE); /* 开启应答 */

    while (len--)
    {

        if (len == 0)
        { /* 接收最后一个数据前关闭应答，准备STOP */ // EV7_1

            I2C_ConfigAck(I2Cx, DISABLE);

            I2C_GenerateStop(I2Cx, ENABLE);
        }

        I2CTimeout = I2CT_LONG_TIMEOUT;

        while (!I2C_GetFlag(I2Cx, I2C_FLAG_RXDATNE))
        { // EV7

            if ((I2CTimeout--) == 0)
            {

                I2C_ResetInit(I2Cx);

                return MASTER_BYTEF;
            }
        }

        *datPtr++ = I2C_RecvData(I2Cx);
    }

    /* 5.等待BUSY */

    I2CTimeout = I2CT_LONG_TIMEOUT;

    while (I2C_GetFlag(I2Cx, I2C_FLAG_BUSY))
    {

        if ((I2CTimeout--) == 0)
        {

            I2C_ResetInit(I2Cx);

            return MASTER_BUSY;
        }
    }

    /* 6.接收完成 */

    return MASTER_OK;
}