#include "Metoo_KEY.h"
#include "n32g45x_it.h"
/**
 * @brief  Configures key port.
 * @param key->GPIO_port x can be A to G to select the GPIO port.
 * @param Pin This parameter can be GPIO_PIN_0~GPIO_PIN_15.
 */
void KeyInputExtiInit(MetooKEY_t *key)
{
    GPIO_InitType GPIO_InitStructure;
    EXTI_InitType EXTI_InitStructure;
    NVIC_InitType NVIC_InitStructure;

    uint8_t GPIO_Port_Sources;

    /* Check the parameters */
    assert_param(IS_GPIO_ALL_PERIPH(key->GPIO_port));

    /* Enable the GPIO Clock */
    if (key->GPIO_port == GPIOA)
    {
        GPIO_Port_Sources = GPIOA_PORT_SOURCE;
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOA, ENABLE);
    }
    else if (key->GPIO_port == GPIOB)
    {
        GPIO_Port_Sources = GPIOB_PORT_SOURCE;
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOB, ENABLE);
    }
    else if (key->GPIO_port == GPIOC)
    {
        GPIO_Port_Sources = GPIOC_PORT_SOURCE;
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOC, ENABLE);
    }
    else if (key->GPIO_port == GPIOD)
    {
        GPIO_Port_Sources = GPIOD_PORT_SOURCE;
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOD, ENABLE);
    }
    else if (key->GPIO_port == GPIOE)
    {
        GPIO_Port_Sources = GPIOE_PORT_SOURCE;
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOE, ENABLE);
    }
    else if (key->GPIO_port == GPIOF)
    {
        GPIO_Port_Sources = GPIOF_PORT_SOURCE;
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOF, ENABLE);
    }
    else if (key->GPIO_port == GPIOG)
    {
        GPIO_Port_Sources = GPIOG_PORT_SOURCE;
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOG, ENABLE);
    }

    // RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO, ENABLE);
    /*Configure the GPIO pin as input floating*/
    if ((key->GPIO_pin) <= GPIO_PIN_ALL)
    {
        GPIO_InitStructure.Pin = key->GPIO_pin;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
        GPIO_InitPeripheral(key->GPIO_port, &GPIO_InitStructure);
    }
    /*Configure key EXTI Line to key input Pin*/
    GPIO_ConfigEXTILine(GPIO_Port_Sources, key->Pin_source);

    /*Configure key EXTI line*/
    EXTI_InitStructure.EXTI_Line = key->EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_InitPeripheral(&EXTI_InitStructure);

    /*Set key input interrupt priority*/
    NVIC_InitStructure.NVIC_IRQChannel = key->IRQChannel;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x05;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

// void key_Handler(MetooKEY_t *key) // 消抖，及长按，长按不会自动复位，需要手动复位
// {

//     if (GPIO_ReadInputDataBit(key->GPIO_port, key->GPIO_pin) == (uint8_t)Bit_RESET)
//     {
//         key->state = BUTTON_PRESSED;
//         key->pressed_time = millis();
//     }
//     else
//     {
//         key->repeating_time = millis();
//         uint32_t time_use = key->repeating_time - key->pressed_time;
//         log_info("repeating_time = %d , pressed_time = %d , time_use = %d,", key->repeating_time, key->pressed_time, time_use);

//         // key->repeating_time = 0;
//         // key->pressed_time = 0;

//         if (time_use > BUTTON_PRESSED_LONG_TIME)
//             key->state = BUTTON_PRESSED_LONG;
//         else if (time_use > BUTTON_CLICKED_TIME)
//             key->state = BUTTON_CLICKED;
//         else
//             key->state = BUTTON_RELEASED;
//     }

//     log_info("key=%d\n\r", key->state);
// }

void key_Handler(MetooKEY_t *key)  // 没有消抖
{

    if (GPIO_ReadInputDataBit(key->GPIO_port, key->GPIO_pin) == (uint8_t)Bit_RESET)
    {
        key->state = BUTTON_PRESSED;
    }
    else
    {
        key->state = BUTTON_RELEASED;
    }

}