#include "Metoo_DAC.h"

void dac_gpio_init(void);
void dac_init(void);
void dac_gpio_ID_init(void);

void Metoo_DAC_init(MetooDAC_Data_t *MetooDAC)
{

    GPIO_InitType GPIO_InitStructure;

    /* GPIOA Periph clock enable */
    // RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOA, ENABLE);
    /* Once the DAC channel is enabled, the corresponding GPIO pin is automatically
       connected to the DAC converter. In order to avoid parasitic consumption,
       the GPIO pin should be configured in analog */
    GPIO_InitStructure.Pin = MetooDAC->GPIO_pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_InitPeripheral(MetooDAC->GPIO_port, &GPIO_InitStructure);

    DAC_InitType DAC_InitStructure;

    /* DAC Periph clock enable */
    // RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_DAC, ENABLE);
    /* DAC channel1 Configuration */
    DAC_InitStructure.Trigger = DAC_TRG_SOFTWARE;
    DAC_InitStructure.WaveGen = DAC_WAVEGEN_NONE;
    DAC_InitStructure.LfsrUnMaskTriAmp = DAC_UNMASK_LFSRBIT0;
    DAC_InitStructure.BufferOutput = DAC_BUFFOUTPUT_ENABLE;
    DAC_Init(MetooDAC->DAC_ch, &DAC_InitStructure);

    /* Enable DAC Channel1: Once the DAC channel1 is enabled, PA.04 is
       automatically connected to the DAC converter. */
    DAC_Enable(MetooDAC->DAC_ch, ENABLE);

    /* Set DAC Channel1 DHR12L register */
    Metoo_DAC_set_V(MetooDAC, 0); // 启动设置为0
    // dac_gpio_init();
    // dac_init();
}

void Metoo_DAC_Deinit(void)
{
    DAC_DeInit();
    dac_gpio_ID_init();
}

void Metoo_DAC_set_V(MetooDAC_Data_t *MetooDAC, float Voltage)
{
    MetooDAC->value = Voltage;
    MetooDAC->raw = Voltage * 1241.212121; // 1/3.3*4096
    if (MetooDAC->DAC_ch == DAC_CHANNEL_1)
        DAC_SetCh1Data(DAC_ALIGN_R_12BIT, MetooDAC->raw);
    if (MetooDAC->DAC_ch == DAC_CHANNEL_2)
        DAC_SetCh2Data(DAC_ALIGN_R_12BIT, MetooDAC->raw);

    DAC_SoftTrgEnable(MetooDAC->DAC_ch, ENABLE);
}

/**
 * @brief  Configures the different GPIO ports.
 */
void dac_gpio_init(void)
{
    GPIO_InitType GPIO_InitStructure;

    /* GPIOA Periph clock enable */
    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOA, ENABLE);
    /* Once the DAC channel is enabled, the corresponding GPIO pin is automatically
       connected to the DAC converter. In order to avoid parasitic consumption,
       the GPIO pin should be configured in analog */
    GPIO_InitStructure.Pin = DAC_1_PIN | DAC_2_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_InitPeripheral(DAC_1_PORT, &GPIO_InitStructure);
}

/**
 * @brief  将gpio设置为输入下拉，以免影响ADC检测
 */
void dac_gpio_ID_init(void)
{
    GPIO_InitType GPIO_InitStructure;

    /* GPIOA Periph clock enable */
    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOA, ENABLE);
    /* Once the DAC channel is enabled, the corresponding GPIO pin is automatically
       connected to the DAC converter. In order to avoid parasitic consumption,
       the GPIO pin should be configured in analog */
    GPIO_InitStructure.Pin = DAC_1_PIN | DAC_2_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
    GPIO_InitPeripheral(DAC_1_PORT, &GPIO_InitStructure);
}

/**
 * @brief  Configures the different GPIO ports.
 */
void dac_init(void)
{
    DAC_InitType DAC_InitStructure;

    /* DAC Periph clock enable */
    RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_DAC, ENABLE);
    /* DAC channel1 Configuration */
    DAC_InitStructure.Trigger = DAC_TRG_SOFTWARE;
    DAC_InitStructure.WaveGen = DAC_WAVEGEN_NONE;
    DAC_InitStructure.LfsrUnMaskTriAmp = DAC_UNMASK_LFSRBIT0;
    DAC_InitStructure.BufferOutput = DAC_BUFFOUTPUT_ENABLE;
    DAC_Init(DAC_CHANNEL_1, &DAC_InitStructure);
    DAC_Init(DAC_CHANNEL_2, &DAC_InitStructure);

    /* Enable DAC Channel1: Once the DAC channel1 is enabled, PA.04 is
       automatically connected to the DAC converter. */
    DAC_Enable(DAC_CHANNEL_1, ENABLE);
    DAC_Enable(DAC_CHANNEL_2, ENABLE);
    /* Set DAC Channel1 DHR12L register */
    DAC_SetCh1Data(DAC_ALIGN_R_12BIT, 0);
    DAC_SetCh2Data(DAC_ALIGN_R_12BIT, 0);
}
